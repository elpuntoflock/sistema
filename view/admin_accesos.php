
<?php
include_once 'controller/seguridad.php';
require_once 'db/conexion.php';

$usuario = mysqli_query($conn, "SELECT ID_USUARIO, CONCAT_WS(' ', NOMBRES,NOMBRE2,APELLIDO1,APELLIDO2)NOMBRE
								FROM tb_usuario");

$usuarioII = mysqli_query($conn, "SELECT ID_USUARIO, CONCAT_WS(' ',NOMBRES,NOMBRE2,APELLIDO1,APELLIDO2)NOMBRE
                                FROM tb_usuario");
                                
$usuarioIII = mysqli_query($conn, "SELECT ID_USUARIO, CONCAT_WS(' ',NOMBRES,NOMBRE2,APELLIDO1,APELLIDO2)NOMBRE
                                FROM tb_usuario");
                                
$sql_user = mysqli_query($conn, "SELECT ID_USUARIO, CONCAT_WS(' ',NOMBRES,NOMBRE2,APELLIDO1,APELLIDO2)NOMBRE, EMAIL,
                                        NOMBRES, NOMBRE2, APELLIDO1, APELLIDO2, CELULAR, TIPO_USUARIO
                                FROM tb_usuario");

?>

<style>
.centrar {
    text-align: center !important;
}
.upper {
    text-transform: uppercase !important;
}
.color {
    color: #fff;
}
</style>

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>

<div class="">
    <div class="row">
        <div class="col-md-12">
            <div class="wrapper-logo-secondary">
                <img src="img/logo/Law.jpg" alt="Logotipo Firma Law">
            </div>
        </div>
    </div>
</div>

<div class="wrapper-title">
    <div class="container">
        <div class="row">
            <div class="col-md-4" data-line="mobil">
                <div class="line"></div>
            </div>
            <div class="col-md-4 section-title">
                <h1>PANEL DE CONTROL</h1>
            </div>
            <div class="col-md-4">
                <div class="line"></div>
            </div>
        </div>
    </div>
</div>

<!--**********************************************************-->

<!--**********************************************************-->
<div class="wrapper-dashboard" style='margin-top: 70px;'>
        <div class="row m-t-25" style="margin-top: -55px;">

            <div class="col-md-4">
                <div class="overview-item overview-item--c1">
                    <div class="overview__inner-1">
                        <div class="overview-box clearfix">
                            <div class="icon">
                                <i class="zmdi zmdi-account-o"></i>
                            </div>
                            <div class="text">
                                <span class="span-dasboard">Accesos a Casos Usuario</span>
                                <span class="span-dasboard"><a href="" data-toggle="modal" data-target="#exampleModal"><h2><i class="fas fa-key"></i> </h2> </a></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 

            <div class="col-md-4">
                <div class="overview-item overview-item--c2">
                    <div class="overview__inner-1">
                        <div class="overview-box clearfix">
                            <div class="icon">
                                <i class="zmdi zmdi-shopping-cart"></i>
                            </div>
                            <div class="text">
                                <span class="span-dasboard">Accesos Men&uacute; a Usuario</span>
                                <span class="span-dasboard"><a href=""  data-toggle="modal" data-target="#exampleModalItem"><h2><i class="fas fa-sitemap"></i></h2> </a></span>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="overview-item overview-item--c1">
                    <div class="overview__inner-1">
                        <div class="overview-box clearfix">
                            <div class="icon">
                                <i class="zmdi zmdi-account-o"></i>
                            </div>
                            <div class="text">
                                <span class="span-dasboard">Creaci&oacute;n Usuarios</span>
                                <span class="span-dasboard"><a href="" data-toggle="modal" data-target="#ModalUser"><h2><i class="fas fa-user-plus"></i> </h2> </a></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>            

        </div>

</div>
<!--*****************************************************************************************-->
<div class="wrapper-dashboard" >
        <div class="row m-t-25" style="margin-top: -55px;">
            <div class="col-md-2" style='margin-top: 45px;'></div>
            <div class="col-md-4 table-space">
                <div class="overview-item overview-item--c1">
                    <div class="overview__inner-1">
                        <div class="overview-box clearfix">
                            <div class="icon">
                                <i class="zmdi zmdi-account-o"></i>
                            </div>
                            <div class="text">
                                <span class="span-dasboard">Eliminar Item por Usuario</span>
                                <span class="span-dasboard"><a href="" data-toggle="modal" data-target="#TableUserItem"><h2><i class="fas fa-users-cog"></i> </h2> </a></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 table-space">
                <div class="overview-item overview-item--c1">
                    <div class="overview__inner-1">
                        <div class="overview-box clearfix">
                            <div class="icon">
                                <i class="zmdi zmdi-account-o"></i>
                            </div>
                            <div class="text">
                                <span class="span-dasboard">Eliminar Casos por Usuario</span>
                                <span class="span-dasboard"><a href="" data-toggle="modal" data-target="#myModalD"><h2><i class="fa fa-trash"></i> </h2> </a></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container">

            <!-- Modal -->
            <div class="modal fade" id="myModalD" role="dialog">
                <div class="modal-dialog modal-lg">
                
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                    <h4 class="modal-title">ELIMINAR PERMISOS DE CASOS</h4>
                    </div>
                    <div class="modal-body">
                    <form action="menu.php?id=48" method="post">

            <div class="wrapper-space">
                <table  class='table table-striped table-bordered table-hover' id='example4'>
                    <thead>
                        <tr>
                        <th class='center'>ID CASO</th>
                        <th class='center'>USUARIO</th>	
                        <th class='center'>DESCRIPCI&Oacute;N CAUSA</th>
                        <th class='center'>SELECCIONAR</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php

                            $items_casos = mysqli_query($conn, "SELECT A.ID_CASO, A.ID_USUARIO, b.CAUSA
                                                                    FROM tb_acceso a,
                                                                        tb_caso b
                                                                    WHERE a.ID_CASO = b.ID_CASO");

                        while($ftc1 = mysqli_fetch_array($items_casos)){
                            $caso_item = $ftc1['0'];
                            echo "<tr>";
                                echo "<td class='center'>";
                                    echo "<input type='text' name='usuario' class='ocultar' value='$ftc1[1]'>";
                                    echo $ftc1[0];
                                echo "</td>";
                                echo "<td>";
                                    echo $ftc1[1];
                                echo "</td>";
                                echo "<td>";
                                    echo $ftc1[2];
                                echo "</td>";
                                echo "<td class='wrapper-centrar'>";
                                    echo "<input type='checkbox' name='id_causa[]' value='$caso_item'>";
                                echo "</td>";
                            echo "</tr>";
                        }

                        ?>
                    </tbody>
                </table>            
            </div>
            <div class="wrapper-space wrapper-centrar">
                <button type="submit" class="btn btn-success">GRABAR</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">CERRAR</button>
            </div>  

                    </form>

                    </div>
                </div>
                
                </div>
            </div>
            
            </div>

        </div>

</div>
<!--/***************************************************************************************/-->

<div class="wrapper-dashboard table-space" >
    <div class="row m-t-25" style="margin-top: -55px;">

<div class="wrapper-title">
    <div class="container">
        <div class="row">
            <div class="col-md-4" data-line="mobil">
                <div class="line"></div>
            </div>
            <div class="col-md-4 section-title">
                <h1>CONTROL DE USUARIO</h1>
            </div>
            <div class="col-md-4">
                <div class="line"></div>
            </div>
        </div>
    </div>
</div>

			<div class="col-md-12 table-responsive bajar">
				<table id="example" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%;">
					<thead>
						<tr>
							<th class="centrar">USUARIO</th>
							<th class="centrar">NOMBRES</th>
							<th class="centrar">EMAIL</th>
                            <th class="centrar">ELIMINAR</th>
                            <th class="centrar">EDITAR</th>
						</tr>
					</thead>
					<tbody>
						<?php
        				while ($fond = mysqli_fetch_array($sql_user)){
                                $d_usuario  = $fond[0];
                                $p_nombre   = $fond[3];
                                $s_nombre   = $fond[4];
                                $p_apellido = $fond[5];
                                $s_apellido = $fond[6];
                                $email      = $fond[2];
                                $celular    = $fond[7];
                                $tipo_user  = $fond[8];
								echo "<tr>";
									echo "<td style='font-weight: bold;'>$fond[0]</td>";
									echo "<td style='text-align: left;'>$fond[1]</td>";
                                    echo "<td style='text-align: left;'>$fond[2]</td>";
                                    echo "<td class='center'><a href='menu.php?id=45&tmp=$fond[0]' ><i class='fa fa-user-times' aria-hidden='true'></i> Eliminar Usuario</td>";
                                    echo "<td class='center'><a href='#' data-toggle='modal' data-target='#myModal'
                                    data-usuario    = '$d_usuario'
                                    data-nombre1    = '$p_nombre'
                                    data-nombre2    = '$s_nombre'
                                    data-apellido1  = '$p_apellido'
                                    data-apellido2  = '$s_apellido'
                                    data-email      = '$email'
                                    data-celular    = '$celular'
                                    data-tipo       = '$tipo_user'
                                    ><i class='fa fa-user-circle' aria-hidden='true'></i> Editar Usuario</a></td>";
								echo "</tr>";
							} 
						?>           
					</tbody>
				</table>

		<div>

    </div>
</div>

<!--**************************** MODAL ACCESO A LOS CASOS **************************-->

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-key" aria-hidden="true"></i> ACCESO POR CASO</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
 
	    <form action="menu.php?id=42" method="post">
            <div class="wrapper-space">
                <label>SELECCIONE USUARIO</label>
                <select name="usuario" id="valor_usuario" class="form-control" required="" onchange="ConsultaUsuario()">
                    <option value="">SELECCIONAR USUARIO</option>
				<?php
                    while ($rowX = mysqli_fetch_array($usuario))
                    {
                    echo '<option value="' . $rowX['ID_USUARIO']. '">' . $rowX['NOMBRE'] . '</option>' . "\n";
                    }
                    ?> 						
			    </select>
            </div>

            <div id="dato"></div>

        </form>
      </div>
      <div class="">

    <div class="alert alert-warning alert-dismissible fade show center" role="alert">
        <strong>Mensaje...!!!  </strong> Debe de Seleccionar la opci&oacute;n (Caso / Causa) al cual el Usuario tendra acceso...!!!
    </div>

    </div>
    </div>
  </div>
</div>

<!--****************************MODAL ACCESO A MENU**************************-->

<div class="modal fade" id="exampleModalItem" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-bars" aria-hidden="true"></i> ACCESO A MEN&Uacute;</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
 
	    <form action="menu.php?id=46" method="post">
            <div class="wrapper-space wrapper-centrar">
                <label>SELECCIONE USUARIO</label>
                <select name="usuario" id="val_user" class="form-control" required="" onchange="return FuncionConsulta();">
                    <option value="">SELECCIONAR USUARIO</option>
                    <?php
                    while ($rowy = mysqli_fetch_array($usuarioII))
                    {
                    echo '<option value="' . $rowy['ID_USUARIO']. '">' . $rowy['NOMBRE'] . '</option>' . "\n";
                    }
                    ?> 						
                </select>            
            </div>

           <div id="result"></div>
            
        </form>

      </div>
      <div class="">

        <div class="alert alert-warning alert-dismissible fade show center" role="alert">
        <strong>Mensaje...!!!  </strong> Debe de Seleccionar la opci&oacute;n (Pantalla) a la cual el Usuario tendra acceso...!!!
        </div>

       </div>
    </div>
  </div>
</div>

<!--****************************MODAL CREACION DE USUARIOS **************************-->

<div class="modal fade" id="ModalUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-user-plus" aria-hidden="true"></i> CREACI&Oacute;N DE USUARIOS</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
 
	    <form action="menu.php?id=43" method="post" onsubmit="return validar();">
            <div class="wrapper-space wrapper-centrar">
                <label>DEFINIR USUARIO</label>
                <input type="text" name="id_usuario" class="form-control centrar upper" placeholder="INGRESAR EL USUARIO" id="usuario" autofocus="">
            </div>

            <div class="wrapper-space wrapper-centrar">
                <label>PRIMER NOMBRE</label>
                <input type="text" name="p_nombre" class="form-control upper" placeholder="Primer Nombre" id="pnombre">
            </div>
            <div class="wrapper-space wrapper-centrar">
                <label>SEGUNDO NOMBRE</label>
                <input type="text" name="s_nombre" class="form-control upper" placeholder="Segundo Nombre">
            </div>	
            <div class="wrapper-space wrapper-centrar">
                <label>PRIMER APELLIDO</label>
                <input type="text" name="p_apellido" class="form-control upper" placeholder="Primer Apellido" id="papellido">
            </div> 
            <div class="wrapper-space wrapper-centrar">
                <label>SEGUNDO APELLIDO</label>
                <input type="text" name="s_apellido" class="form-control upper" placeholder="Segundo Apellido">
            </div>    
            <div class="wrapper-space wrapper-centrar">
                <label>SEXO</label>
                <select name="sexo" class="form-control">
                    <option value="M">MASCULINO</option>
                    <option value="F">FEMENINO</option>
                </select>
            </div> 

            <div class="wrapper-space wrapper-centrar">
                <label>CELULAR</label>
                <input type="text" name="celular" class="form-control centrar" placeholder="Celular" id="celular">
            </div> 
            <div class="wrapper-space wrapper-centrar">
                <label>CORREO ELECTRONICO</label>
                <input type="text" name="email" class="form-control centrar" placeholder="Correo Electronico" id="email">
            </div>

            <div class="wrapper-space wrapper-centrar">
                <label>TIPO DE USUARIO</label>
                <select name="tipo" class="form-control">
                    <option value="1">ADMINISTRADOR</option>
                    <option value="2">OPERATIVO</option>
                </select>
            </div> 

            <div class="wrapper-space wrapper-centrar">
                <label>CONTRASE&Ntilde;A</label>
                <input type="password" name="password" class="form-control centrar" placeholder="Contrase&ntilde;a Generica" id="password" require>
            </div>                    	

            <div style="margin-top: 40px;">
                <div class="boton-formulario">
                    <button type="submit" class="btn btn-success">GRABAR</button>
                </div>
            </div>

        </form>

      </div>
    </div>
  </div>
</div>

<script>
    function validar(){
        var usuario     = document.getElementById('usuario').value;
        var nombre      = document.getElementById('pnombre').value;
        var apellido    = document.getElementById('papellido').value;
        var celular     = document.getElementById('celular').value;
        var email       = document.getElementById('email').value;
        var contra      = document.getElementById('password').value;

    if(usuario == false){
        alert('El USUARIO es obligatorio FAVOR DE INGRESARLO....');
        return false;
    }         

    if(nombre == false){
        alert('El campo Primer Nombre es obligatorio FAVOR DE INGRESARLO....');
        return false;
    }  

    if(apellido == false){
        alert('El campo Primer Apellido es obligatorio FAVOR DE INGRESARLO....');
        return false;
    } 

    if(celular == false){
        alert('El campo Celular es obligatorio FAVOR DE INGRESARLO....');
        return false;
    }  

    if(email == false){
        alert('El campo Correo Electronico es obligatorio FAVOR DE INGRESARLO....');
        return false;
    }                   
        
    if(contra == false){
        alert('El campo contraseña es obligatorio FAVOR DE INGRESARLO....');
        return false;
    }

    }
</script>


<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"><i class="fa fa-user-circle" aria-hidden="true"></i> MODIFICACI&Oacute;N USUARIO</h4>
        </div>
		
		<div class="modal-body">

		<form action="menu.php?id=44" method="post">
			<div class="wrapper-space wrapper-centrar">
				<label>USUARIO</label>
				<input type="text" name="d_usuario" id='d_usuario' class="form-control centrar" readonly=''>
			</div>

			<div class="wrapper-space wrapper-centrar">
				<label>PRIMER NOMBRE</label>
				<input type="text" name="p_nombre" id='p_nombre' class="form-control upper centrar" placeholder="PRIMER NOMBRE">
			</div>

			<div class="wrapper-space wrapper-centrar">
				<label>SEGUNDO NOMBRE</label>
				<input type="text" name="s_nombre" id='s_nombre' class="form-control upper centrar" placeholder="SEGUNDO NOMBRE">
			</div>

			<div class="wrapper-space wrapper-centrar">
	  			<label>PRIMER APELLIDO</label>
	  			<input type="text" name="p_apellido" id="p_apellido" class="form-control upper centrar" placeholder="PRIMER APELLIDO">
		  	</div>			

			<div class="wrapper-space wrapper-centrar">
	  			<label>SEGUNDO APELLIDO</label>
	  			<input type="text" name="s_apellido" id="s_apellido" class="form-control upper centrar" placeholder="SEGUNDO APELLIDO">
		  	</div>

			<div class="wrapper-space wrapper-centrar">
                <label>EMAIL</label>
                <input type="text" name="correo" id="correo" class="form-control centrar" placeholder="EMAIL">
			</div>

			<div class="wrapper-space wrapper-centrar">
                <label>CELULAR</label>
                <input type="text" name="cel" id="cel" class="form-control upper centrar" placeholder="CELULAR">
            </div>
            <div class="wrapper-space wrapper-centrar">
                <label>TIPO DE USUARIO</label>
                <select name="tipo_user" class="form-control">
                    <option value="1">ADMINISTRADOR</option>
                    <option value="2">OPERATIVO</option>
                </select>
            </div>             

       </div>
		
			<div class="modal-footer">
				<button type="submit" class="btn btn-success">GRABAR</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">CERRAR</button>
			</div>	
		</form>
		</div>	
		
   
    </div>
</div>

    <script>
	$('#myModal').on('show.bs.modal', function(e)
	{
        var d_usuario 	= $(e.relatedTarget).data('usuario');
        var p_nombre 	= $(e.relatedTarget).data('nombre1');
        var s_nombre 	= $(e.relatedTarget).data('nombre2');
        var apellido1 	= $(e.relatedTarget).data('apellido1');
        var apellido2 	= $(e.relatedTarget).data('apellido2');
        var correo  	= $(e.relatedTarget).data('email');
        var celular  	= $(e.relatedTarget).data('celular');
        var tipo      	= $(e.relatedTarget).data('tipo');

        $(e.currentTarget).find('input[name="d_usuario"]').val(d_usuario);
        $(e.currentTarget).find('input[name="p_nombre"]').val(p_nombre);
        $(e.currentTarget).find('input[name="s_nombre"]').val(s_nombre);
        $(e.currentTarget).find('input[name="p_apellido"]').val(apellido1);
        $(e.currentTarget).find('input[name="s_apellido"]').val(apellido2);        
        $(e.currentTarget).find('input[name="correo"]').val(correo);        
        $(e.currentTarget).find('input[name="cel"]').val(celular);
        $(e.currentTarget).find('select[name="tipo_user"]').val(tipo);


	});
  </script>

  <!--******************************* TABLA DE USUARIOS POR ITEM *********************************-->
  <style>
  #example1_filter {
      text-align: center !important;
      
  }

  #example1_filter > label {
    font-weight: bold !important;
  }

  .wrapper-text {
      font-weight: bold;
  }
  .dataTables_length {
        font-size: 16px;
        text-align: center;
  }

  .dataTables_length > label {
    font-weight: bold !important; 
  }

  .color {
      color: #fff;
  }

  </style>

<div class="modal fade" id="TableUserItem" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">CONTROL DE ITEM POR USUARIO</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
		
		<div class="modal-body">

		<form action="menu.php?id=47" method="post">

            <!--div class="wrapper-space wrapper-centrar">
                <label>SELECCIONE USUARIO</label>
                <select name="usuario" class="form-control" required="">
                    <?php
                    while ($rowy = mysqli_fetch_array($usuarioIII))
                    {
                    echo '<option value="' . $rowy['ID_USUARIO']. '">' . $rowy['NOMBRE'] . '</option>' . "\n";
                    }
                    ?> 						
                </select>            
            </div-->
            <div class="wrapper-space">
                <table  class='table table-striped table-bordered table-hover' id='example3'>
                    <thead>
                        <tr>
                        <th>USUARIO</th>	
                        <th style='text-align: center;'>DESCRIPCI&Oacute;N ITEM</th>
                        <th style='text-align: center;'>SELECCIONAR</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php

                            $items_acceso = mysqli_query($conn, "SELECT a.ID_USUARIO, a.ITEM, b.DESCRIPCION
                                                            FROM tb_acceso_item a,
                                                                tb_items_menu b
                                                            WHERE a.ITEM = b.ID_ITEM");

                        while($ftc = mysqli_fetch_array($items_acceso)){
                            $itm_menu = $ftc['1'];
                            echo "<tr>";
                                echo "<td>";
                                    echo "<input type='text' name='usuario' class='ocultar' value='$ftc[0]'>";
                                    echo $ftc[0];
                                echo "</td>";
                                echo "<td>";
                                    echo $ftc[2];
                                echo "</td>";
                                echo "<td class='wrapper-centrar'>";
                                    echo "<input type='checkbox' name='id_items[]' value='$itm_menu'>";
                                echo "</td>";
                            echo "</tr>";
                        }

                        ?>
                    </tbody>
                </table>            
            </div>

            <div class="wrapper-space wrapper-centrar">
                <button type="submit" class="btn btn-success">GRABAR</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">CERRAR</button>
            </div>            
			

		</form>
		</div>	
		
   
    </div>
</div>


  <!--******************************* FINALIZA TABLA DE USUARIOS POR ITEM *********************************-->
  <script>
    $(document).ready(function() {
        
    $('#example1').DataTable({
        "order": [[ 0, "asc" ]],
        "language": {
            "url": "assets/js/Spanish.json"
        }
    });

    $('#example2').DataTable({
        "lengthMenu": [[20, 25, 50, -1], [10, 25, 50, "All"]] ,
        "language": {
            "url": "assets/js/Spanish.json"
        }
    }); 

    $('#example3').DataTable({
        "lengthMenu": [[20, 25, 50, -1], [10, 25, 50, "All"]] ,
        "language": {
            "url": "assets/js/Spanish.json"
        }
    });

    $('#example4').DataTable({
        "lengthMenu": [[20, 25, 50, -1], [10, 25, 50, "All"]] ,
        "language": {
            "url": "assets/js/Spanish.json"
        }
    }); 

} );
</script>

<script>

    function FuncionConsulta() {
        var formData = new FormData();
        formData.append('user',  document.getElementById("val_user").value);

        //alert();
        
        $.ajax({
        url:  "controller/data_user.php",
        type: "POST",
        dataType: "text",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,

            })
        .done(function(res){
        $("#result").html( res);
        });

    }

    function ConsultaUsuario() {
        var formData = new FormData();
        formData.append('user',  document.getElementById("valor_usuario").value);

        //alert();
        
        $.ajax({
        url:  "controller/data_casos.php",
        type: "POST",
        dataType: "text",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,

            })
        .done(function(res){
        $("#dato").html( res);
        });

    }    

</script>