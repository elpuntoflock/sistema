<?php
	//Incluimos librería y archivo de conexión
	require_once '../Classes/PHPExcel.php';
	require_once '../db/conexion.php';
	session_start();
	$nombre		= strtoupper($_SESSION['usuario']);
	
	//Consulta
    $sql = mysqli_query($conn, "SELECT CASE 
								WHEN A.CAUSA = '' THEN 'SIN CAUSA'
								ELSE A.CAUSA
								END AS DETALLE_CAUSA, A.ID_CONTACTO,  TRIM(CONCAT_WS(' ', B.NOMBRES,' ',B.APELLIDOS,' ',B.NOMBRE_EMPRESA))NOMBRE, A.JUZGADO, 
								A.ID_CASO, A.DESCRIPCION, DATE_FORMAT(A.FECHA_INI,'%d/%m/%Y')FECHA_INICIO, DATE_FORMAT(A.FECHA_FIN,'%d/%m/%Y'), A.OBSERVACIONES, A.NUMERO_MP,
								A.FISCALIA, A.JUZGADO, A.JUEZ, A.OFICIAL, A.DIRECCION_JUZGADO, A.TELEFONO_JUZGADO, A.CASO_ORIGEN, A.ESTATUS,
								A.ABOGA_RESPONSABLE
								FROM	tb_caso A,
										tb_contacto B,
										tb_acceso C
								WHERE A.ID_CONTACTO = B.ID_CONTACTO
								AND A.ID_CASO 		= C.ID_CASO
								AND C.ID_USUARIO  	= '".$nombre."'");
	$fila = 7; //Establecemos en que fila inciara a imprimir los datos
	
    //$gdImage = imagecreatefrompng('img/logo/law_menu.png');//Logotipo
    //$gdImage = imagecreatefrompng('img/logo/google.png');//Logotipo
	
	//Objeto de PHPExcel
	$objPHPExcel  = new PHPExcel();
	
	//Propiedades de Documento
	$objPHPExcel->getProperties()->setCreator("Marko robles")->setDescription("Reporte de Contactos");
	
	//Establecemos la pestaña activa y nombre a la pestaña
	$objPHPExcel->setActiveSheetIndex(0);
	$objPHPExcel->getActiveSheet()->setTitle("Casos");
	/*
	$objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
	$objDrawing->setName('Logotipo');
	$objDrawing->setDescription('Logotipo');
	$objDrawing->setImageResource($gdImage);
	$objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_PNG);
	$objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
	$objDrawing->setHeight(80);
	$objDrawing->setCoordinates('A1');
	$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());*/
	
	$estiloTituloReporte = array(
    'font'      => array(
	'name'      => 'Arial',
	'bold'      => false,
	'italic'    => false,
	'strike'    => false,
    'size'      => 15
    ),
    'fill'  => array(
    'type'  => PHPExcel_Style_Fill::FILL_SOLID

	),
    'borders'       => array(
	'allborders'    => array(
	'style'         => PHPExcel_Style_Border::BORDER_NONE
	)
    ),
    'alignment' => array(
	'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
    )
	);
	
	$estiloTituloColumnas = array(
    'font' => array(
	'name'  => 'Arial',
	'bold'  => 	true,
	'size'  =>	10,
	'color' => array(
	'rgb' => 'FFFFFF'
	)
    ),
    'fill' => array(
	'type' => PHPExcel_Style_Fill::FILL_SOLID,
	'color' => array('rgb' => '8d939a')
    ),
    'borders' => array(
	'allborders' => array(
	'style' => PHPExcel_Style_Border::BORDER_THIN
	)
    ),
    'alignment' =>  array(
	'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER
    )
	);
	
	$estiloInformacion = new PHPExcel_Style();
	$estiloInformacion->applyFromArray( array(
    'font'  => array(
	'name'  => 'Arial',
	'color' => array(
	'rgb'   => '000000'
	)
    ),
    'fill' => array(
	'type'  => PHPExcel_Style_Fill::FILL_SOLID
	),
    'borders' => array(
	'allborders' => array(
	'style' => PHPExcel_Style_Border::BORDER_THIN
	)
    ),
	'alignment' =>  array(
	'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY,
	'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER
    )
	));
	
	$objPHPExcel->getActiveSheet()->getStyle('A1:M4')->applyFromArray($estiloTituloReporte);
	$objPHPExcel->getActiveSheet()->getStyle('A6:M6')->applyFromArray($estiloTituloColumnas);
	
	$objPHPExcel->getActiveSheet()->setCellValue('A3', 'REPORTE GENERAL DE CASOS');
	$objPHPExcel->getActiveSheet()->mergeCells('A3:M3');
	
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
	$objPHPExcel->getActiveSheet()->setCellValue('A6', 'CAUSA');
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(35);
	$objPHPExcel->getActiveSheet()->setCellValue('B6', 'NOMBRE');
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(50);
	$objPHPExcel->getActiveSheet()->setCellValue('C6', 'JUZGADO');
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(45);
	$objPHPExcel->getActiveSheet()->setCellValue('D6', 'DESCRIPCION');
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
	$objPHPExcel->getActiveSheet()->setCellValue('E6', 'FECHA INICIO');
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(60);
	$objPHPExcel->getActiveSheet()->setCellValue('F6', 'OBSERVACIONES');
	$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
	$objPHPExcel->getActiveSheet()->setCellValue('G6', 'NUMERO MP');
	$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
	$objPHPExcel->getActiveSheet()->setCellValue('H6', 'FISCALIA');
	$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
	$objPHPExcel->getActiveSheet()->setCellValue('I6', 'JUZGADO');
	$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
	$objPHPExcel->getActiveSheet()->setCellValue('J6', 'JUEZ');
	$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
	$objPHPExcel->getActiveSheet()->setCellValue('K6', 'OFICIAL');
	$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(30);
	$objPHPExcel->getActiveSheet()->setCellValue('L6', 'DIRECCION JUZGADO');
	$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(10);
	$objPHPExcel->getActiveSheet()->setCellValue('M6', 'ESTATUS');
	
	//Recorremos los resultados de la consulta y los imprimimos
	while($rows = $sql->fetch_assoc()){
		
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$fila, $rows['DETALLE_CAUSA']);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, $rows['NOMBRE']);
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, $rows['JUZGADO']);
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, $rows['DESCRIPCION']);
		$objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, strtolower($rows['FECHA_INICIO']));
		$objPHPExcel->getActiveSheet()->setCellValue('F'.$fila, $rows['OBSERVACIONES']);
		$objPHPExcel->getActiveSheet()->setCellValue('G'.$fila, $rows['NUMERO_MP']);
		$objPHPExcel->getActiveSheet()->setCellValue('H'.$fila, $rows['FISCALIA']);
		$objPHPExcel->getActiveSheet()->setCellValue('I'.$fila, $rows['JUZGADO']);
		$objPHPExcel->getActiveSheet()->setCellValue('J'.$fila, $rows['JUEZ']);
		$objPHPExcel->getActiveSheet()->setCellValue('K'.$fila, $rows['OFICIAL']);
		$objPHPExcel->getActiveSheet()->setCellValue('L'.$fila, $rows['DIRECCION_JUZGADO']);
		$objPHPExcel->getActiveSheet()->setCellValue('M'.$fila, $rows['ESTATUS']);
		

		
		$fila++; //Sumamos 1 para pasar a la siguiente fila
	}
	
	$fila = $fila-1;
	
	$objPHPExcel->getActiveSheet()->setSharedStyle($estiloInformacion, "A7:M".$fila);

	$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

	
	header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	header('Content-Disposition: attachment;filename="Reporte Casos.xlsx"');
	header('Cache-Control: max-age=0');
	
	$writer->save('php://output');
?>