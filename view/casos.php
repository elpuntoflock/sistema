<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="lib_alert/css/sweetalert.css">
<script type="text/javascript" src="lib_alert/js/sweetalert-dev.js"></script>


<script>
	function ErrorAcceso()
		{
		swal({title:"Su usuario no tiene privilegios para esta pantalla..!", type:"error", showConfirmButton:false, text:"COMUNIQUESE CON EL ADMINISTRADOR", timer:'1500'}, 
		function () 
		{
		location.href = "menu.php?id=33"; 
		});
		}
</script>
<?php
require_once 'controller/seguridad.php';
require_once 'controller/controller.php';
require_once 'db/conexion.php';

$sql_permiso = mysqli_query($conn, "SELECT COUNT(*)CUENTA
										FROM tb_acceso_item
										WHERE id_usuario = '".$nombre."'
										  AND ITEM = ".$_REQUEST['id']."");
								
	while($valida = $sql_permiso->fetch_array(MYSQLI_ASSOC)){

		$resultado = $valida['CUENTA'];
	}

	if($resultado == 1){
		
	}else{
		echo "<script>ErrorAcceso();</script>";
    }
?>
<div class="row">
    <div class="col-md-12">
        <div class="wrapper-logo-secondary">
            <img src="img/logo/Law.jpg" alt="Logotipo Firma Law">
        </div>
    </div>
</div>

<div class="wrapper-return">
<a href="menu.php?id=1"><button type="button" class="btn btn-info"><i class="fa fa-arrow-left" aria-hidden="true"></i> Regresar</button></a>
</div>

<div class="wrapper-title">
    <div class="container">
        <div class="row">
            <div class="col-md-4" data-line="mobil">
                <div class="line"></div>
            </div>
            <div class="col-md-4 section-title">
                <h1><i class="fa fa-address-card" aria-hidden="true"></i> CASOS</h1>
            </div>
            <div class="col-md-4">
                <div class="line"></div>
            </div>
        </div>
    </div>
</div>

<div class="card wrapper-space">
    <div class="wrapper-centrar card-header">
        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#ModalCrea"><i class="fa fa-address-card" aria-hidden="true"></i> Crear Casos Nuevos</button>
        <a href="view/repo_casos.php" target="_blanck"><button type="button" class="btn btn-warning"><i class="fa fa-file" aria-hidden="true"></i> Reporte PDF</button></a>
        <a href="view/repo_casos_excel.php"><button type="button" class="btn btn-primary"><i class="fa fa-table" aria-hidden="true"></i> Reporte Excel</button></a>
    </div>
</div>

<div class="container wrapper-space" style="margin-top: 75px;">
    <div class="row">
        <div class="col-md-12">
            <table id="example" class="table table-striped table-bordered dt-responsive nowrap">
                <thead>
                    <tr>
                        <th class="centrar">CAUSA</th>
						<th class="centrar">EDITAR</th>                        
						<th class="centrar">CLIENTE</th>
						<th class="centrar">JUZGADO</th>
						<th class="centrar">DESCRIPCI&Oacute;N</th>
						<!--th class="centrar">DOCUMENTOS</th-->
						<th class="centrar">ABOGADO RESPONSABLE</th>
						<th>ESTATUS</th>
                    </tr>
                </thead>   
                <tbody>
                    <?php
                   while ($res = mysqli_fetch_array($busqueda)){

                        echo "<tr>";
                        $id_caso 		= $res[4];
                        $id_cliente		= $res[1];
                        $descripcion	= utf8_encode(strtoupper($res[5]));
                        $fec_inicio		= $res[6];
                        $fec_final		= $res[7];
                        $observaciones	= utf8_encode(strtoupper($res[8]));
                        $causa 			= utf8_encode($res[0]);
                        $num_mp 		= $res[9];
                        $fiscalia 		= $res[10];
                        $juzgado 		= $res[11];
                        $juez			= $res[12];
                        $oficial 		= $res[13];
                        $direc_juzgado	= $res[14];
                        $zona_juzgado 	= $res[15];
                        $tel_juzgado 	= $res[16];
						$origen 		= $res[17];
						$estatus		= $res[18];
						$aboga			= $res[19];
                        $des_juzgado = utf8_encode(strtoupper($res[3]));

                            echo "<td>";
                                echo $causa;
                            echo "</td>";

                            echo "<td class='wrapper-centrar'>";
							echo "<a href='#' data-toggle='modal' data-target='#Modal' 
											  data-id_caso 		= '$id_caso'
											  data-contacto 	= '$id_cliente'
											  data-origen   	= '$origen'
											  data-descripcion 	= '$descripcion'
											  data-fecha_inicio = '$fec_inicio'
											  data-fecha_final	= '$fec_final'
											  data-observaciones = '$observaciones'
											  data-causa		= '$causa'
											  data-num_mp		= '$num_mp'
											  data-fiscalia		= '$fiscalia'
											  data-juzgado		= '$juzgado'
											  data-juez			= '$juez'
											  data-oficial		= '$oficial'
											  data-direccion_juzgado	= '$direc_juzgado'
											  data-telefono_juzgado 	= '$tel_juzgado'
											  data-estatus 		= '$estatus'><i class='fa fa-edit' aria-hidden='true'></i></a>";
                            echo "</td>";
                            
                            echo "<td>";
                                echo $id_cliente." - ".$res[2];
                            echo "</td>";

							echo "<td style='text-align: left;'>";
								echo $des_juzgado;
							echo "</td>";
							
							echo "<td style='text-align: left;'>";
								echo $descripcion;
							echo "</td>";
							
							//echo "</td>";
							/*echo "<td>";
								echo "<a href=''><i class='fa fa-search' aria-hidden='true'></i> Documentos</a>";
							echo "</td>";*/

                            echo "<td>";
								echo $aboga;
							echo "</td>";
							echo "<td>";
								if($estatus == 'I'){
									$estatus = 'INACTIVO';
								}elseif($estatus == 'A'){
									$estatus = 'ACTIVO';
								}elseif($estatus == 'C'){
									$estatus = 'FINALIZADO';
								}
                                echo $estatus;
                            echo "</td>";							
                        echo "</tr>";                          
                   
                    }
                    ?>                  
                </tbody>             
            </table>
        </div>
    </div>
</div>



<div class="">
	<div class="modal fade" id="ModalCrea" role="dialog">
		<div class="modal-dialog">
		
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">CREACI&Oacute;N DE CASOS</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body cuerpo">

			<form action="menu.php?id=11" method="post" enctype="multipart/form-data" onsubmit="return validar();">
			
			

			<div class="tabbable">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#tab1" data-toggle="tab"><i class="fa fa-plus-circle" aria-hidden="true"></i> GENERALES CASO</a></li>
					<li><a href="#tab2" data-toggle="tab"><i class="fa fa-plus-circle" aria-hidden="true"></i> DATOS JUZGADOS</a></li>
				</ul>
        <div class="tab-content">
			<div class="tab-pane active" id="tab1">
					<div class="wrapper-space wrapper-centrar">
						<label>CLIENTE</label>
						<select name="id_contacto" class="form-control" autofocus="">
							<option value="">SELECCIONAR CLIENTE</option>
                        <?php
                        $contacto = mysqli_query($conn,"SELECT ID_CONTACTO, TRIM(CONCAT_WS(' ', NOMBRES,' ',APELLIDOS,' ',NOMBRE_EMPRESA))NOMBRES
                        FROM tb_contacto");                        

						while ($row = mysqli_fetch_array($contacto))
						{
							echo '<option value="' . $row['ID_CONTACTO']. '">' . $row['ID_CONTACTO'].'-'. $row['NOMBRES'] . '</option>' . "\n";
						}
						?>
						</select>
					</div>
					<div class="wrapper-space wrapper-centrar">
						<label for="">RUTA DEL CASO</label>
						<select name="ruta" id="ruta" class="form-control">
							<option value="">SELECCIONAR RUTA</option>
							<?php
							
							$ruta = mysqli_query($conn, "SELECT id_area, descripcion
															FROM tb_archivos
															WHERE tb__casos is null");

								while($ftp = mysqli_fetch_array($ruta)){
									echo "<option value='$ftp[0]'>$ftp[1]</option>";
								}														
							
							?>
						</select>
					</div>

					<div class="wrapper-space wrapper-centrar">
						<label>CASO ORIGEN</label>
						<select name="origen" class="form-control">
							<option value="0">SIN CASO ORIGEN</option>
							<?php
								$sdl = mysqli_query($conn, "SELECT a.ID_CASO, a.CAUSA
															FROM tb_caso a,
																 tb_acceso b
															WHERE a.id_caso = b.id_caso
															  AND b.id_usuario = '".$nombre."'");
								while($res = mysqli_fetch_array($sdl)){
									echo '<option value="' . $res['ID_CASO']. '">' . $res['CAUSA'] . '</option>' . "\n";
								}							
							?>
						</select>
					</div>					
					
					<div class="wrapper-space wrapper-centrar">
						<label>DESCRIPCI&Oacute;N</label>
						<input type="text" name="descripcion" class="form-control upper" placeholder="DESCRIPCI&Oacute;N DEL CASO" required="">
					</div>
					<div class="wrapper-space wrapper-centrar">
						<label>FECHA INICIO</label>
						<input type="text" name="fec_inicio" class="form-control centrar" id="fecha_inicio" placeholder="FECHA INICIO" required="">
					</div>
					<div class="wrapper-space wrapper-centrar">
						<label>FECHA FINAL</label>
						<input type="text" name="fec_final" class="form-control centrar" id="fecha_final" placeholder="FECHA FINAL">
					</div>
					<div class="wrapper-space wrapper-centrar">
						<label>OBSERVACIONES</label>
						<input type="text" name="text" class="form-control upper" placeholder="OBSERVACIONES">
					</div>

					<div style="margin-bottom: 15px;" class="wrapper-space wrapper-centrar">
						<label for="">ADJUNTAR ARCHIVO</label>
						<input type="file" name="file" class="form-control">
					</div>

					<div class="wrapper-space wrapper-centrar">
						<label>ABOGADO RESPONSABLE</label>
						<input type="text" name="abogado" class="form-control upper" placeholder="Abogado Responsable">
					</div>					

			</div>
			<div class="tab-pane" id="tab2">
				<div class="wrapper-space wrapper-centrar">
					<label>N&Uacute;MERO EXPEDIENTE</label>
					<input type="text" name="causa" class="form-control upper" placeholder="N&uacute;mero Expediente" id="causa">
				</div>
				<div class="wrapper-space wrapper-centrar">
					<label>N&Uacute;MERO MP</label>
					<input type="text" name="mun_mp" class="form-control upper" placeholder="N&uacute;mero MP">
				</div>
				<div class="wrapper-space wrapper-centrar">
					<label>FISCALIA</label>
					<input type="text" name="fiscalia" class="form-control upper" placeholder="Fiscalia">
				</div>
				<div class="wrapper-space wrapper-centrar">
					<label>JUZGADO</label>
					<input type="text" name="juzgado" class="form-control upper" placeholder="Juzgado">
				</div>
				<div class="wrapper-space wrapper-centrar">
					<label>JUEZ</label>
					<input type="text" name="juez" class="form-control upper" placeholder="Juez">
				</div>
				<div class="wrapper-space wrapper-centrar">
					<label>OFICIAL</label>
					<input type="text" name="oficial" class="form-control upper" placeholder="oficial">
				</div>	
				<div class="wrapper-space wrapper-centrar">
					<label>DIRECCI&Oacute;N JUZGADO</label>
					<input type="text" name="direccion_juzgado" class="form-control upper" placeholder="direcci&oacute;n juzgado">
				</div>	

				<div class="wrapper-space wrapper-centrar">
					<label>TELEFONO JUZGADO</label>
					<input type="text" name="tel_juzgado" class="form-control upper" placeholder="telefono juzgado">
				</div>
				<div class="boton-formulario bajar wrapper-space wrapper-centrar">
			<button type="submit" class="btn btn-success"><i class="fa fa-hdd" aria-hidden="true"></i> GRABAR</button>
			<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> CERRAR</button>
		</div>															
			</div>
        </div>

		</form>
        </div>
			</div>
		</div>
		
		</div>
	</div>
</div>


<!-- Modal -->
<div class="modal fade" id="Modal" tabindex="-1" role="dialog" aria-labelledby="Modal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="Modal"><i class="fa fa-user-plus" aria-hidden="true"></i>  Modificaci&oacute;n de Casos</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body cuerpo">
        
          <div role="tabpanel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#uploadTab1" aria-controls="uploadTab" role="tab" data-toggle="tab">
                        <i class="fa fa-plus-circle" aria-hidden="true"></i> Datos Generales</a>

                        </li>
                        <li role="presentation"><a href="#browseTab1" aria-controls="browseTab" role="tab" data-toggle="tab">
                            <i class="fa fa-plus-circle" aria-hidden="true"></i> Datos Complemento</a>

                        </li>
                    </ul>
                    <!-- Tab panes -->
                    <form action="menu.php?id=20" method="post">
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="uploadTab1">
								<div class="wrapper-space wrapper-centrar">
									<label for="">Id Caso</label>
									<input type="text" name="id_caso" class="form-control wrapper-centrar" readonly="">
								</div>
								<div class="wrapper-space wrapper-centrar">
									<label>CLIENTE</label>
									<select name="id_contacto" class="form-control" autofocus="">
										<option value="">SELECCIONAR CONTACTO</option>
									<?php
									$contacto = mysqli_query($conn,"SELECT ID_CONTACTO, TRIM(CONCAT_WS(' ', NOMBRES,' ',APELLIDOS,' ',NOMBRE_EMPRESA))NOMBRES
									FROM tb_contacto");                        

									while ($row = mysqli_fetch_array($contacto))
									{
										echo '<option value="' . $row['ID_CONTACTO']. '">' . $row['ID_CONTACTO'].'-'. $row['NOMBRES'] . '</option>' . "\n";
									}
									?>
									</select>
								</div>
								<div class="wrapper-space wrapper-centrar">
									<label>CASO ORIGEN</label>
									<select name="origen" class="form-control">
										<option value="0">SIN CASO ORIGEN</option>
										<?php
											$sdl = mysqli_query($conn, "SELECT a.ID_CASO, a.CAUSA
																		FROM tb_caso a,
																			tb_acceso b
																		WHERE a.id_caso = b.id_caso
																		AND b.id_usuario = '".$nombre."'");
											while($res = mysqli_fetch_array($sdl)){
												echo '<option value="' . $res['ID_CASO']. '">' . $res['CAUSA'] . '</option>' . "\n";
											}							
										?>
									</select>
								</div>	
								<div class="wrapper-space wrapper-centrar">
									<label>DESCRIPCI&Oacute;N</label>
									<input type="text" name="descripcion" class="form-control upper" placeholder="DESCRIPCI&Oacute;N DEL CASO" required="">
								</div>
								<div class="wrapper-space wrapper-centrar">
									<label>FECHA INICIO</label>
									<input type="text" name="fec_inicio" class="form-control centrar" id="fec_inicio" placeholder="FECHA INICIO" required="">
								</div>
								<div class="wrapper-space wrapper-centrar">
									<label>FECHA FINAL</label>
									<input type="text" name="fec_final" class="form-control centrar" id="fec_final" placeholder="FECHA FINAL">
								</div>
								<div class="wrapper-space wrapper-centrar">
									<label>OBSERVACIONES</label>
									<input type="text" name="text" class="form-control upper" placeholder="OBSERVACIONES">
								</div>

                            </div>

                            <div role="tabpanel" class="tab-pane" id="browseTab1">
								<div class="wrapper-space wrapper-centrar">
									<label>CAUSA</label>
									<input type="text" name="causa" class="form-control upper" placeholder="Causa de Caso" id="causa">
								</div>
								<div class="wrapper-space wrapper-centrar">
									<label>N&Uacute;MERO MP</label>
									<input type="text" name="num_mp" class="form-control upper" placeholder="N&uacute;mero MP">
								</div>
								<div class="wrapper-space wrapper-centrar">
									<label>FISCALIA</label>
									<input type="text" name="fiscalia" class="form-control upper" placeholder="Fiscalia">
								</div>
								<div class="wrapper-space wrapper-centrar">
									<label>JUZGADO</label>
									<input type="text" name="juzgado" class="form-control upper" placeholder="Juzgado">
								</div>
								<div class="wrapper-space wrapper-centrar">
									<label>JUEZ</label>
									<input type="text" name="juez" class="form-control upper" placeholder="Juez">
								</div>
								<div class="wrapper-space wrapper-centrar">
									<label>OFICIAL</label>
									<input type="text" name="oficial" class="form-control upper" placeholder="oficial">
								</div>	
								<div class="wrapper-space wrapper-centrar">
									<label>DIRECCI&Oacute;N JUZGADO</label>
									<input type="text" name="direccion_juzgado" class="form-control upper" placeholder="direcci&oacute;n juzgado">
								</div>	

								<div class="wrapper-space wrapper-centrar">
									<label>TELEFONO JUZGADO</label>
									<input type="text" name="tel_juzgado" class="form-control upper" placeholder="telefono juzgado">
								</div>
								<div class="wrapper-space wrapper-centrar">
									<label for="">ESTATUS</label>
									<select name="estatus" class="form-control">
										<option value="A">ACTIVO</option>
										<option value="I">INACTIVO</option>
										<option value="C">FINALIZADO</option>
									</select>
								</div>								                          
                                <div class="wrapper-centrar wrapper-space">
                                    <button type="submit" class="btn btn-success"><i class="fa fa-plus-square" aria-hidden="true"></i> MODIFICAR</button>
                                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-ban" aria-hidden="true"></i> CERRAR</button>
                                </div>

                            </div>
                        </div>
                    </form>
                </div>
      </div>


    </div>
  </div>
</div>

<script src="assets/js/bootstrap.min.js"></script>

  <script>
	$('#Modal').on('show.bs.modal', function(e)
	{
        var id 				= $(e.relatedTarget).data('id_caso');
		var contacto 		= $(e.relatedTarget).data('contacto');
		var origen	 		= $(e.relatedTarget).data('origen');
		var descripcion		= $(e.relatedTarget).data('descripcion');
		var fecha_inicio	= $(e.relatedTarget).data('fecha_inicio');
		var fecha_final		= $(e.relatedTarget).data('fecha_final');
		var observaciones	= $(e.relatedTarget).data('observaciones');
		var causa			= $(e.relatedTarget).data('causa');
		var num_mp			= $(e.relatedTarget).data('num_mp');
		var fiscalia		= $(e.relatedTarget).data('fiscalia');
		var juzgado			= $(e.relatedTarget).data('juzgado');
		var juez			= $(e.relatedTarget).data('juez');
		var oficial			= $(e.relatedTarget).data('oficial');
		var direccion_juzgado	= $(e.relatedTarget).data('direccion_juzgado');
		var telefono_juzgado	= $(e.relatedTarget).data('telefono_juzgado');
		var estatus			= $(e.relatedTarget).data('estatus');


        $(e.currentTarget).find('input[name="id_caso"]').val(id);
        $(e.currentTarget).find('select[name="id_contacto"]').val(contacto);
		$(e.currentTarget).find('select[name="origen"]').val(origen);
		$(e.currentTarget).find('input[name="descripcion"]').val(descripcion);
		$(e.currentTarget).find('input[name="fec_inicio"]').val(fecha_inicio);
		$(e.currentTarget).find('input[name="fec_final"]').val(fecha_final);
		$(e.currentTarget).find('input[name="text"]').val(observaciones);
		$(e.currentTarget).find('input[name="causa"]').val(causa);
		$(e.currentTarget).find('input[name="num_mp"]').val(num_mp);
		$(e.currentTarget).find('input[name="fiscalia"]').val(fiscalia);
		$(e.currentTarget).find('input[name="juzgado"]').val(juzgado);
		$(e.currentTarget).find('input[name="juez"]').val(juez);
		$(e.currentTarget).find('input[name="oficial"]').val(oficial);
		$(e.currentTarget).find('input[name="direccion_juzgado"]').val(direccion_juzgado);
		$(e.currentTarget).find('input[name="tel_juzgado"]').val(telefono_juzgado);
		$(e.currentTarget).find('select[name="estatus"]').val(estatus);


	});
  </script>

  <script>
    $('#fecha_inicio').datetimepicker({ footer: true, modal: true, format: 'dd/mm/yyyy' });
    $('#fecha_final').datetimepicker({ footer: true, modal: true, format: 'dd/mm/yyyy' });
    $('#fec_inicio').datetimepicker({ footer: true, modal: true, format: 'dd/mm/yyyy' });
    $('#fec_final').datetimepicker({ footer: true, modal: true, format: 'dd/mm/yyyy' });	
</script>