<!--/ ==================================================================================================================
    Section footer title
=================================================================================================================== /-->
<a href="#" class="scroll-to-top"><i class="fas fa-chevron-circle-up"></i><span class="sr-only">ir arriba</span></a>
<div class="wrapper-title">
    <div class="container">
        <div class="row">
            <div class="col-md-4" data-line="mobil">
                <div class="line"></div>
            </div>
            <div class="col-md-4 section-title">
                <h1>BUSCANOS MUNIGUATE</h1>
            </div>
            <div class="col-md-4">
                <div class="line"></div>
            </div>
        </div>
    </div>
</div>
<!--/ ==================================================================================================================
    Footer
=================================================================================================================== /-->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-7" data-info="social-box" >

                <h3 data-title="title">Redes Sociales</h3>
                <div>
                    <a href="https://www.facebook.com/muniguate/"><i class="icon-Facebook"></i></a>
                    <p>Facebook</p>
                </div>
                <div>
                    <a href="https://twitter.com/muniguate?lang=es"><i class="icon-Twitter"></i></a>
                    <p>Twitter</p>
                </div>
                <div>
                    <a href="https://www.youtube.com/user/portalmuniguate"><i class="icon-You_tube"></i></a>
                    <p>You Tube</p>
                </div>
                <div>
                    <a href="https://www.instagram.com/muniguate/"><i class="icon-Instagram"></i></a>
                    <p>Instagram</p>
                </div>
            </div>

            <div class="col-md-4 col-sm-5" data-info="contact-box">
                <h3 data-title="title">Contactanos</h3>
                <div>
                    <a href="tel:1551"><i class="icon-Call_Center"></i></a>
                    <p>1551 Call Center</p>
                    <p>Call Center</p>
                </div>
                <div>
                    <a href="tel:123"><i class="icon-Bomberos_Municipales"></i></a>
                    <p>123 Bomberos</p>
                    <p>Bomberos</p>
                </div>
                <div>
                    <a href="#"><i class="icon-address-book"></i></a>
                    <p>Directorio</p>
                    <p>Directorio</p>
                </div>
            </div>
        </div>

    </div>
    <div class="container">
        <div class="row top-generico-footer">
            <div class="col-md-12">
                <div class="line line-footer" data-last="sub-footer">
                    <div class="wrapper-directions-footer">
                        <span>21 Calle 6-77 Zona 1, Centro C&iacute;vico, Palacio Municipal. Ciudad de Guatemala, Guatemala, Centroam&eacute;rica.</span>
                        <span class="wrapper-brand-it">powered by I<sup>2</sup>+Desarrollo, Municipalidad de Guatemala</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
</body>

<!--Sources javascript-->
<script src="assets/js/jquery.js"></script>
<script src="assets/js/simplePagination.js"></script>
<script src="assets/js/main.js"></script>
<script src="assets/js/popper.js"></script>
<script src="assets/bootstrap/js/bootstrap.js"></script>

</html>