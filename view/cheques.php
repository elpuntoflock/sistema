<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="lib_alert/css/sweetalert.css">
<script type="text/javascript" src="lib_alert/js/sweetalert-dev.js"></script>


<script>
	function ErrorAcceso()
		{
		swal({title:"Su usuario no tiene privilegios para esta pantalla..!", type:"error", showConfirmButton:false, text:"COMUNIQUESE CON EL ADMINISTRADOR", timer:'1500'}, 
		function () 
		{
		location.href = "menu.php?id=33"; 
		});
		}
</script>
<?php
require_once 'controller/controller.php';
require_once 'controller/seguridad.php';


$sql_permiso = mysqli_query($conn, "SELECT COUNT(*)CUENTA
										FROM tb_acceso_item
										WHERE id_usuario = '".$nombre."'
										  AND ITEM = ".$_REQUEST['id']."");
								
	while($valida = $sql_permiso->fetch_array(MYSQLI_ASSOC)){

		$resultado = $valida['CUENTA'];
	}

	if($resultado == 1){
		
	}else{
		echo "<script>ErrorAcceso();</script>";
    }
?>
<div class="row">
    <div class="col-md-12">
        <div class="wrapper-logo-secondary">
            <img src="img/logo/Law.jpg" alt="Logotipo Firma Law">
        </div>
    </div>
</div>


<div class="wrapper-title">
    <div class="container">
        <div class="row">
            <div class="col-md-4" data-line="mobil">
                <div class="line"></div>
            </div>
            <div class="col-md-4 section-title">
                <h1><i class="fa fa-credit-card" aria-hidden="true"></i>  EMISIÓN DE CHEQUES</h1>
            </div>
            <div class="col-md-4">
                <div class="line"></div>
            </div>
        </div>
    </div>
</div>


<form action="menu.php?id=41" method="post">
    <div class="col-md-12">
        <div class="col-md-6 center">
            <label for="">SELECCIONAR CUENTA</label>
            <select name="cuenta" id="" class="form-control">
                <?php
                while($row = mysqli_fetch_array($cuenta)){
                    echo '<option value="' . $row['IDCUENTA']. '">'. $row['CUENTA'] . ' - '.$row['NUM_CUENTA'].' - '.$row['MONEDA'].'</option>' . "\n";
                }
                ?>
            </select>
        </div>
        <div class="col-md-6 center">
            <label for="">SELECCIONAR FACTURA / RECIBO</label>
            <select name="factura" id="" class="form-control">
                <?php
                while($res = mysqli_fetch_array($fac_provee)){
                    echo '<option value="' . $res['ID_FACTURA']. '">' . $res['NOMBRE'] . ' ** - ** '.$res['DETALLE'].'</option>' . "\n";
                }
                ?>
            </select>
        </div>
    </div>
    <div class="col-md-12 wrapper-space">
        <div class="col-md-4 center">
            <label for="">NUMERO DE CHEQUE</label>
            <input type="text" name="numero_cheque" class="form-control upper center" placeholder="N&uacute;mero Cheque" required="">
        </div>
        <div class="col-md-4 center">
            <label for="">MONTO DE CHEQUE</label>
            <input type="text" name="monto" class="form-control upper center" placeholder="Monto Cheque" required="">
        </div>
        <div class="col-md-4 center">
            <label for="">FECHA DE PAGO</label>
            <input type="text" name="fecha_pago" id="datepicker" class="form-control upper center" placeholder="Fecha Pago" required="">
        </div>
    </div>
    <div class="col-md-12 wrapper-space">
        <div class="col-md-5 center">
            <label for="">INGRESAR REFERENCIA</label>
            <input type="text" name="referencia" class="form-control wrapper-upper" placeholder="referencia">
        </div>
        <div class="col-md-5 center">
            <label for="">INGRESAR MOTIVO</label>
            <input type="text" name="motivo" class="form-control  wrapper-upper" placeholder="motivo">
        </div>
        <div class="col-md-2 center">
            <label for="">NEGOCIABLE</label>
            <select name="negociable" class="form-control">
                <option value="S">SI</option>
                <option value="N">NO</option>
            </select>
        </div>
    </div>

    <div class="col-md-12 wrapper-space">
        <div class="col-md-5"></div>
        <div class="col-md-2">
            <button type="submit" id="boton" class="btn btn-success"><i class="fa fa-hdd" aria-hidden="true"></i> GRABAR</button>
        </div>
            <?php
            if(isset($_REQUEST['ftc']))
                {
                $var = $_REQUEST['ftc'];  
                echo "<div class='col-md-1'>";
                    echo "<a href='reporte_cheque.php?ftc=$var' target='_blanck'><button type='button' class='btn btn-warning'>IMPRESI&Oacute;N</button></a>";
                echo "</div>";
                }
                else{
                    $var = '0';    
                }

            ?>
    </div>    
</form>
<script>
        $('#datepicker').datepicker({ format: 'yyyy/mm/dd' });
    </script>