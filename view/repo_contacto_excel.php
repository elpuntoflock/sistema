<?php
	//Incluimos librería y archivo de conexión
	require_once '../Classes/PHPExcel.php';
	require_once '../db/conexion.php';
	
	//Consulta
    $sql = mysqli_query($conn, "SELECT ID_CONTACTO, TRIM(CONCAT_WS(' ', NOMBRES,' ',APELLIDOS,' ',NOMBRE_EMPRESA))NOMBRE, CUI, TELEFONO,
                                       NIT, DIRECCION, EMAIL
                                  FROM tb_contacto");
	$fila = 7; //Establecemos en que fila inciara a imprimir los datos
	
    //$gdImage = imagecreatefrompng('img/logo/law_menu.png');//Logotipo
    //$gdImage = imagecreatefrompng('img/logo/google.png');//Logotipo
	
	//Objeto de PHPExcel
	$objPHPExcel  = new PHPExcel();
	
	//Propiedades de Documento
	$objPHPExcel->getProperties()->setCreator("Marko robles")->setDescription("Reporte de Contactos");
	
	//Establecemos la pestaña activa y nombre a la pestaña
	$objPHPExcel->setActiveSheetIndex(0);
	$objPHPExcel->getActiveSheet()->setTitle("Contactos");
	/*
	$objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
	$objDrawing->setName('Logotipo');
	$objDrawing->setDescription('Logotipo');
	$objDrawing->setImageResource($gdImage);
	$objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_PNG);
	$objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
	$objDrawing->setHeight(80);
	$objDrawing->setCoordinates('A1');
	$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());*/
	
	$estiloTituloReporte = array(
    'font'      => array(
	'name'      => 'Arial',
	'bold'      => false,
	'italic'    => false,
	'strike'    => false,
    'size'      =>15
    ),
    'fill'  => array(
    'type'  => PHPExcel_Style_Fill::FILL_SOLID

	),
    'borders'       => array(
	'allborders'    => array(
	'style'         => PHPExcel_Style_Border::BORDER_NONE
	)
    ),
    'alignment' => array(
	'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
    )
	);
	
	$estiloTituloColumnas = array(
    'font' => array(
	'name'  => 'Arial',
	'bold'  => true,
	'size' =>10,
	'color' => array(
	'rgb' => 'FFFFFF'
	)
    ),
    'fill' => array(
	'type' => PHPExcel_Style_Fill::FILL_SOLID,
	'color' => array('rgb' => '8d939a')
    ),
    'borders' => array(
	'allborders' => array(
	'style' => PHPExcel_Style_Border::BORDER_THIN
	)
    ),
    'alignment' =>  array(
	'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER
    )
	);
	
	$estiloInformacion = new PHPExcel_Style();
	$estiloInformacion->applyFromArray( array(
    'font' => array(
	'name'  => 'Arial',
	'color' => array(
	'rgb' => '000000'
	)
    ),
    'fill' => array(
	'type'  => PHPExcel_Style_Fill::FILL_SOLID
	),
    'borders' => array(
	'allborders' => array(
	'style' => PHPExcel_Style_Border::BORDER_THIN
	)
    ),
	'alignment' =>  array(
	'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	'vertical'  => PHPExcel_Style_Alignment::VERTICAL_CENTER
    )
	));
	
	$objPHPExcel->getActiveSheet()->getStyle('A1:E4')->applyFromArray($estiloTituloReporte);
	$objPHPExcel->getActiveSheet()->getStyle('A6:E6')->applyFromArray($estiloTituloColumnas);
	
	$objPHPExcel->getActiveSheet()->setCellValue('A3', 'REPORTE GENERAL DE CONTACTOS');
	$objPHPExcel->getActiveSheet()->mergeCells('A3:E3');
	
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
	$objPHPExcel->getActiveSheet()->setCellValue('A6', 'Id Contacto');
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(80);
	$objPHPExcel->getActiveSheet()->setCellValue('B6', 'Nombre');
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
	$objPHPExcel->getActiveSheet()->setCellValue('C6', 'Cui');
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(13);
	$objPHPExcel->getActiveSheet()->setCellValue('D6', 'Telefono');
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(50);
	$objPHPExcel->getActiveSheet()->setCellValue('E6', 'Correo Electronico');
	
	//Recorremos los resultados de la consulta y los imprimimos
	while($rows = $sql->fetch_assoc()){
		
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$fila, $rows['ID_CONTACTO']);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$fila, $rows['NOMBRE']);
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$fila, $rows['CUI']);
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$fila, $rows['TELEFONO']);
		$objPHPExcel->getActiveSheet()->setCellValue('E'.$fila, strtolower($rows['EMAIL']));
		
		$fila++; //Sumamos 1 para pasar a la siguiente fila
	}
	
	$fila = $fila-1;
	
	$objPHPExcel->getActiveSheet()->setSharedStyle($estiloInformacion, "A7:E".$fila);

	$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

	
	header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	header('Content-Disposition: attachment;filename="Reporte Contactos.xlsx"');
	header('Cache-Control: max-age=0');
	
	$writer->save('php://output');
?>