<?php
require_once 'db/conexion.php';

$caso = $_REQUEST['fft'];

$detalle = mysqli_query($conn, "SELECT DATE_FORMAT(FECHA,'%d/%m/%Y')FEC, DESCRIPCION, MONTO, TIPO, MONEDA
                                  FROM tb_cargo_abono
                                  WHERE TIPO <> 6
                                    AND ID_CASO = '".$caso."'
                                  ORDER BY FECHA ASC");

$saldo = mysqli_query($conn, "SELECT SUM(A.SALDO) DETALLE, A.MONEDA
                                FROM tb_corriente A,
                                     tb_caso B,
                                     tb_contacto C
                                WHERE   A.ID_CASO 	    = B.ID_CASO
                                    AND B.ID_CONTACTO   = C.ID_CONTACTO
                                    AND B.ID_CASO       = '".$caso."'");

?>
<div class="row">
    <div class="col-md-12">
        <div class="wrapper-logo-secondary">
            <img src="img/logo/Law.jpg" alt="Logotipo Firma Law">
        </div>
    </div>
</div>

<div class="wrapper-return">
<a href="menu.php?id=16"><button type="button" class="btn btn-info"><i class="fa fa-arrow-left" aria-hidden="true"></i> Regresar</button></a>
</div>

<div class="wrapper-title">
    <div class="container">
        <div class="row">
            <div class="col-md-4" data-line="mobil">
                <div class="line"></div>
            </div>
            <div class="col-md-4 section-title">
                <h1><i class="fa fa-credit-card" aria-hidden="true"></i> DETALLE GENERAL </h1>
            </div>
            <div class="col-md-4">
                <div class="line"></div>
            </div>
        </div>
    </div>
</div>

<div class="container wrapper-space" style="margin-top: 75px;">
    <div class="row">
        <div class="col-md-12">
            <table id="example" class="table table-striped table-bordered dt-responsive nowrap">
                <thead>
                    <tr>
                        <th class="center">FECHA</th>
						<th class="center">DETALLE</th>                        
						<th class="center">MONTO</th>
                    </tr>
                </thead>   
                <tbody>
                    <?php
                   while ($res = mysqli_fetch_array($detalle)){

                        echo "<tr>";

                            echo "<td class='wrapper-centrar'>";
                                echo $res[0];
                            echo "</td>";

                            echo "<td class='wrapper-centrar'>";
                                echo $res[1];
                            echo "</td>";
                            
                            echo "<td class='wrapper-centrar'>";
                                echo $res[4].' '.$res[2];
                            echo "</td>";

                        echo "</tr>";                          
                   
                    }
                    ?>
				<tr style='font-weight: bold; background-color: #ABB4BA;;'>
					<td style='color: #000;' class='center'>SALDO A LA FECHA:</td>
					<td style='color: #000;' class='center'><?php echo date('d/m/Y'); ?></td>
					<td style='color: #000;' class='center'>
						<?php
						while($res = mysqli_fetch_array($saldo)){
							echo $res[1].'&nbsp;&nbsp;'.number_format($res[0],2,'.',',');
						}
						?>
					</td>
				</tr>                    
                </tbody>             
            </table>
        </div>
    </div>
</div>