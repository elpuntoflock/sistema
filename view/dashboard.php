<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="lib_alert/css/sweetalert.css">
<script type="text/javascript" src="lib_alert/js/sweetalert-dev.js"></script>


<script>
	function ErrorAcceso()
		{
		swal({title:"Su usuario no tiene privilegios para esta pantalla..!", type:"error", showConfirmButton:false, text:"COMUNIQUESE CON EL ADMINISTRADOR", timer:'1500'}, 
		function () 
		{
		location.href = "menu.php?id=33"; 
		});
		}
</script>
<?php
include_once 'controller/seguridad.php';
require_once 'controller/controller.php';

$sql_permiso = mysqli_query($conn, "SELECT COUNT(*)CUENTA
										FROM tb_acceso_item
										WHERE id_usuario = '".$nombre."'
										  AND ITEM = ".$_REQUEST['id']."");
								
	while($valida = $sql_permiso->fetch_array(MYSQLI_ASSOC)){

		$resultado = $valida['CUENTA'];
	}

	if($resultado == 1){
		
	}else{
		echo "<script>ErrorAcceso();</script>";
    }

?>    
    <div class="wrapper-dashboard">
        <div class="row m-t-25">
            <div class="col-md-4">
            <div class="wrapper-logo-secondary">
            <img src="img/logo/Law.jpg" alt="Logotipo Firma Law">
        </div>
    </div>

    <div class="col-md-4">
        <div class="overview-item overview-item--c1">
            <div class="overview__inner-1">
                <div class="overview-box clearfix">
                    <div class="icon">
                        <i class="zmdi zmdi-account-o"></i>
                    </div>
                    <div class="text">
                        <span class="span-dasboard">CONTACTOS</span>
                        <span class="span-dasboard"><a href="menu.php?id=3"><h2><i class="fa fa-users"></i> <?php echo $num_contacto; ?></h2> </a></span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="overview-item overview-item--c2">
            <div class="overview__inner-1">
                <div class="overview-box clearfix">
                    <div class="icon">
                        <i class="zmdi zmdi-shopping-cart"></i>
                    </div>
                    <div class="text">
                        <span class="span-dasboard">CASOS</span>
                        <span class="span-dasboard"><a href="menu.php?id=8"><h2><i class="fas fa-th-large"></i> <?php echo $num_casos; ?></h2> </a></span>
                    </div>
                </div>

            </div>
        </div>
    </div>

        </div>

        <div class="row m-t-25 bajar">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <div class="overview-item overview-item--c1">
                    <div class="overview__inner-1">
                        <div class="overview-box clearfix">
                            <div class="icon">
                                <i class="zmdi zmdi-shopping-cart"></i>
                            </div>
                            <div class="text">
                                <span class="span-dasboard">CALENDARIO</span>
                                <span class="span-dasboard"><a href="menu.php?id=6"><h2><i class="fa fa-calendar"></i> </h2> </a></span>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

                        <div class="col-md-4">
                <div class="overview-item overview-item--c2">
                    <div class="overview__inner-1">
                        <div class="overview-box clearfix">
                            <div class="icon">
                                <i class="zmdi zmdi-shopping-cart"></i>
                            </div>
                            <div class="text">
                                <span class="span-dasboard">CUENTA CORRIENTE</span>
                                <span class="span-dasboard"><a href="menu.php?id=14"><h2><i class="fas fa-credit-card"></i> </h2> </a></span>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>