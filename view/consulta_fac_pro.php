<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="lib_alert/css/sweetalert.css">
<script type="text/javascript" src="lib_alert/js/sweetalert-dev.js"></script>


<script>
	function ErrorAcceso()
		{
		swal({title:"Su usuario no tiene privilegios para esta pantalla..!", type:"error", showConfirmButton:false, text:"COMUNIQUESE CON EL ADMINISTRADOR", timer:'1500'}, 
		function () 
		{
		location.href = "menu.php?id=33"; 
		});
		}
</script>
<?php
require_once 'controller/controller.php';

$sql_permiso = mysqli_query($conn, "SELECT COUNT(*)CUENTA
										FROM tb_acceso_item
										WHERE id_usuario = '".$nombre."'
										  AND ITEM = ".$_REQUEST['id']."");
								
	while($valida = $sql_permiso->fetch_array(MYSQLI_ASSOC)){

		$resultado = $valida['CUENTA'];
	}

	if($resultado == 1){
		
	}else{
		echo "<script>ErrorAcceso();</script>";
    }

?>
<div class="row">
    <div class="col-md-12">
        <div class="wrapper-logo-secondary">
            <img src="img/logo/Law.jpg" alt="Logotipo Firma Law">
        </div>
    </div>
</div>


<div class="wrapper-title">
    <div class="container">
        <div class="row">
            <div class="col-md-4" data-line="mobil">
                <div class="line"></div>
            </div>
            <div class="col-md-4 section-title">
                <h1><i class="fa fa-search-plus" aria-hidden="true"></i> CONSULTA FACTURAS</h1>
            </div>
            <div class="col-md-4">
                <div class="line"></div>
            </div>
        </div>
    </div>
</div>

<div class="container wrapper-space" style="margin-top: 75px;">
    <div class="row">
        <div class="col-md-12">
            <table id="example" class="table table-striped table-bordered dt-responsive nowrap">
                <thead>
                    <tr>
                        <th class="centrar">NOMBRE</th>
						<th class="centrar">SERIE</th>                        
						<th class="centrar"># FACTURA</th>
						<th class="centrar">FECHA DE EMISI&Oacute;N</th>
						<th class="centrar">OBSERVACIONES</th>
						<th class="centrar">MONTO</th>
						<th>ESTATUS</th>
                    </tr>
                </thead>   
                <tbody>
                    <?php
                   while ($row = mysqli_fetch_array($rep_facturas)){

                        echo "<tr>";
                            echo "<td>";
                                echo $row[0];
                            echo "</td>";
                            echo "<td class='center'>";
                                echo $row[1];
                            echo "</td>";
                            echo "<td class='center'>";
                                echo $row[2];
                            echo "</td>";
                            echo "<td class='center'>";
                                echo $row[3];
                            echo "</td>";
                            echo "<td>";
                                echo $row[4];
                            echo "</td>";
                            echo "<td class='center'>";
                                echo $row[5];
                            echo "</td>";
                            echo "<td>";
                            $estado = $row[6]; 
                             if($estado == 'C'){
                                $estado = 'CREDITO';
                             }elseif($estado == 'A'){
                                $estado = 'ANULADO';
                             }elseif($estado == 'P'){
                                $estado = 'PAGADO';
                             }
                                echo $estado;
                            echo "</td>";                            
                        echo "</tr>";                          
                   
                    }
                    ?>                  
                </tbody>             
            </table>
        </div>
    </div>
</div>