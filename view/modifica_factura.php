<?php
require_once 'controller/controller.php';
?>
<div class="row">
    <div class="col-md-12">
        <div class="wrapper-logo-secondary">
            <img src="img/logo/Law.jpg" alt="Logotipo Firma Law">
        </div>
    </div>
</div>

<div class="wrapper-title">
    <div class="container">
        <div class="row">
            <div class="col-md-4" data-line="mobil">
                <div class="line"></div>
            </div>
            <div class="col-md-4 section-title">
                <h1><i class="fa fa-window-restore" aria-hidden="true"></i> MODIFICA FACTURACI&Oacute;N</h1>
            </div>
            <div class="col-md-4">
                <div class="line"></div>
            </div>
        </div>
    </div>
</div>

<div class="col-md-12 table-responsive bajar">
	    <table id="example" class="display nowrap table table-striped table-bordered" style="width:170%;">
	        <thead>
	            <tr>
                    <th class="center ocultar">CASO</th>
	                <th class="center">FACTURA</th>
	                <th class="center">MONTO</th>
	                <th class="center">FECHA EMISI&Oacute;N</th>
					<th class="center">ESTADO</th>
                    <th class="center">FECHA ANULACI&Oacute;N</th>
                    <th class="center">TC</th>
                    <th class="center">TE</th>
                    <th class="center">TTC</th>
                    <th class="center">OBSERVACIONES</th>
                    <th class="center">MODIFICAR</th>
	            </tr>
	        </thead>
	        <tbody>
	       	<?php
               $var = 0;
			while ($row = mysqli_fetch_array($facturas)){
                $var ++;

        echo "<script type='text/javascript'>

                function selectItemByValue(elmnt, value){

                for(var i=0; i < elmnt.options.length; i++)
                {
                    if(elmnt.options[i].value == value)
                    elmnt.selectedIndex = i;
                }
                }

                </script>";
                $caso       = $row[9];
                $fecha      = $row[2];
                $newDate    = date("d-m-Y", strtotime($fecha));
                $monto      = number_format($row[1],2,'.',',');
                $estatus    = $row[3];
                $tc         = $row[4];
                $ttc        = $row[5];
                $te         = $row[6];
                $fecha_anula = $row[8];
                if ($fecha_anula <> NULL) {
                    $anulacion   = date("d-m-Y", strtotime($fecha_anula));
                }
                else {
                    $anulacion = NULL;
                }
                $observaciones = $row[9];
                
                echo "<tr class='table-size'>";
                    echo "<form action='menu.php?id=27' method='post' enctype='multipart/form-data' onsubmit='return validacion()' id='form$var'>";

                        echo "<td class='ocultar'>$caso <input type='text' class='ocultar' name='caso' value='$caso' form='form$var'> </td>";
                        echo "<td >$row[0]  <input type='text' class='ocultar' name='id_factura' value='$row[7]' form='form$var'> </td>";
                        echo "<td >$monto   <input type='text' class='ocultar' id='monto' name='monto' value='$row[1]' form='form$var'> </td>";
                        echo "<td >$newDate <input type='text' class='ocultar' name='fecha_emision' value='$newDate' form='form$var'> </td>";
                        echo "<td style='width: 150px;' form='form$var'>
                                <select name='estatus' id='estatus_$var' class='form-control'>
                                    <option value='C'>Credito</option>
                                    <option value='P'>Pagado</option>  
                                    <option value='A'>Anulado</option>
                                  </select>
                                </td>";

                        echo "<script language='javascript'>
                                var numberMI$var = document.getElementById('estatus_$var');
                                selectItemByValue(numberMI$var,'".$estatus."');
                            </script>";
                        echo "<td style='width: 350px;'><input type='text' class='form-control center fecha' name='fecha_nula' id='date_$var' placeholder='Anulaci&oacute;n' value='$anulacion' form='form$var'></td>";
                        echo "<td style='width: 250px;'><input type='text' class='form-control center' id='tc$var' name='tc' placeholder='TC' readonly='' value='$tc' form='form$var'></td>";
                        echo "<td style='width: 250px;'><input type='text' class='form-control center' id='te$var' name='te' placeholder='TE' readonly='' value='$te' form='form$var'></td>";
                        echo "<td style='width: 250px;'><input type='text' class='form-control center' id='ttc$var' name='ttc' placeholder='TTC' readonly='' value='$ttc' form='form$var'></td>";
                        echo "<td style='width: 350px;'><input type='text' id='descri$var' name='observaciones' class='form-control' value='$observaciones' form='form$var'></td>";
                        echo "<td><button type='submit' id='boton' class='btn btn-warning'>Modificar</button></td>";

                    echo "</form>";

                   
                echo "</tr>";
                
                //echo "<script src='./assets/js/jquery-1.11.1.min.js'></script>";
              echo "<script> 
                        $('#date_$var').datepicker({ 
                            format: 'dd-mm-yyyy', 
                            language:'es'
                        }); 
                    </script>"; 
                
                    echo "<script>
                        $(document).ready(function(){

                            $('#estatus_$var').change(function(){

                                var estado$var = $('#estatus_$var').children('option:selected').val();
                                var fec_nula$var   = document.getElementById('date_$var').value;

                                if(estado$var == 'P'){
                                    $('#tc$var').attr('readonly',false);
                                    $('#te$var').attr('readonly',false);
                                    $('#ttc$var').attr('readonly',false);
                                }else{
                                    $('#tc$var').attr('readonly',true);
                                    $('#te$var').attr('readonly',true);
                                    $('#ttc$var').attr('readonly',true);
                                }

                                if(estado$var == 'A'){
                                    $('#date_$var').attr('readonly',false);
                                    $( '#date_$var' ).removeClass('mostrar');
                                }else{
                                    $('#date_$var').attr('readonly',true);
                                    $( '#date_$var' ).addClass('mostrar');
                                }  
                            }) 
                        })
                      </script>";

                    echo "<script>
                        function validacion(){
                            var estado$var = $('#estatus_$var').children('option:selected').val();

                            var tc          = document.getElementById('tc$var').value;
                            var te          = document.getElementById('te$var').value;
                            var ttc         = document.getElementById('ttc$var').value;
                            var monto       = document.getElementById('monto').value;
                            var descripcion = document.getElementById('descri$var').value;

                            suma = parseInt(tc)+parseInt(te)+parseInt(ttc);

                            
                            if(estado$var == 'P'){
                                if(suma != monto){
                                    alert('El Total no coincide con los montos...');
                                    return false;
                                }
                                if(suma == 0){
                                    alert('Debe de ingresar los metodos de pago....');
                                    return false;
                                }
                            }

                            if(estado$var == 'C'){
                                alert('No puede modificar el estado a Credito');
                                return false;
                            }

                            if(descripcion == ''){
                                alert('Debe ingresar la observación del movimiento...');
                                return false;
                            }
                        }
                      </script>";   
				}  
			?>           
	        </tbody>
	    </table>

    </div>