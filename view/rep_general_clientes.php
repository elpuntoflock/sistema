<?php
require_once '../fpdf/fpdf.php';
require_once '../db/conexion.php';

$contacto   = $_REQUEST['tfc'];
$moneda     = $_REQUEST['mon'];

$detalle = mysqli_query($conn, "SELECT B.ID_CASO, B.DESCRIPCION, A.SALDO, A.MONEDA, B.CAUSA, B.ABOGA_RESPONSABLE
								FROM tb_corriente A,
									 tb_caso B,
								     tb_contacto C
								WHERE A.ID_CASO 	    = B.ID_CASO
								  AND B.ID_CONTACTO     = C.ID_CONTACTO
									AND C.ID_CONTACTO   = '".$contacto."'
                                    AND A.MONEDA 	    = '".$moneda."'");



$nom_contacto = mysqli_query($conn, "SELECT TRIM(CONCAT_WS(' ', NOMBRES,' ',APELLIDOS,' ',NOMBRE_EMPRESA))NOMBRE
                                    FROM tb_contacto
                                    WHERE ID_CONTACTO = '".$contacto."'");

while($result = $nom_contacto->fetch_array(MYSQLI_ASSOC)){
    
    $nombre = $result['NOMBRE'];

    }                                    


    $todo = mysqli_query($conn, "SELECT SUM(A.SALDO) TODO, A.MONEDA
                                    FROM tb_corriente A,
                                         tb_caso B,
                                         tb_contacto C
                                         WHERE A.ID_CASO 	    = B.ID_CASO
								  AND B.ID_CONTACTO     = C.ID_CONTACTO
									AND C.ID_CONTACTO   = '".$contacto."'
                                    AND A.MONEDA 	    = '".$moneda."'");


while($ftc = $todo->fetch_array(MYSQLI_ASSOC)){
    
    $saldo_casos = number_format($ftc['TODO'],2,'.',',');

    }                                         


                                    
$pdf = new FPDF('L');
$pdf->AddPage();
$pdf->SetFont('Arial','',12);

$pdf->Image('../img/logo/Law.jpg',25,15,35,0);

$pdf->SetY(40);


$pdf->Cell(265,10,'REPORTE DE CASOS POR CLIENTE ('.$nombre.')',0,1,'C');
/*$pdf->Cell(265,10,$nombre,0,1,'C');*/

$pdf->SetY(70);


$pdf->SetFont('Arial','',8);
$pdf->Cell(60,5,'CAUSA', 1,0,'C');
$pdf->Cell(105,5,'DESCRIPCION DE CASO', 1,0,'C');
$pdf->Cell(60,5,'ABOGADO RESPONSABLE', 1,0,'C');
$pdf->Cell(30,5,'SALDO', 1,1,'C');



    
while ($rest = mysqli_fetch_array($detalle)){

    $causa          = $rest[4];
    $descripcion    = strtoupper($rest[1]);
    $moneda         = $rest[3];
    $saldo          = number_format($rest[2],2,'.',',');
    $aboga          = $rest[5];
    
    
    $pdf->Cell(60,5, $causa , 1, 0, 'L');
    $pdf->Cell(105,5, $descripcion ,1, 0, 'L');
    $pdf->Cell(60,5, $aboga ,1, 0, 'L');
    $pdf->Cell(30,5, $moneda.'  '.$saldo ,1, 1, 'R');
    
    

}

$pdf->Cell(225,5, 'SALDO GENERAL DE CASOS:  ' , 1, 0, 'C');
$pdf->Cell(30,5, $moneda.'  '.$saldo_casos ,1, 1, 'R');


$pdf->SetY(140);

$pdf->Cell(265,10,'Firma:____________________________________________',0,1,'C');

$pdf->Output();
                                    


?>