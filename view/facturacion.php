<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="lib_alert/css/sweetalert.css">
<script type="text/javascript" src="lib_alert/js/sweetalert-dev.js"></script>

<script>
	function ErrorAcceso()
		{
		swal({title:"Su usuario no tiene privilegios para esta pantalla..!", type:"error", showConfirmButton:false, text:"COMUNIQUESE CON EL ADMINISTRADOR", timer:'1500'}, 
		function () 
		{
		location.href = "menu.php?id=33"; 
		});
		}
</script>
<?php
require_once 'db/conexion.php';
require_once 'controller/seguridad.php';
require_once 'controller/controller.php';

$sql_permiso = mysqli_query($conn, "SELECT COUNT(*)CUENTA
                                        FROM tb_acceso_item
                                        WHERE id_usuario = '".$nombre."'
                                            AND ITEM = ".$_REQUEST['id']."");
                
while($valida = $sql_permiso->fetch_array(MYSQLI_ASSOC)){

$resultado = $valida['CUENTA'];
}

if($resultado == 1){

}else{
echo "<script>ErrorAcceso();</script>";
}

?>
<style>
.wrapper-ocultar {
    display: none;
}

</style>

<div class="row">
    <div class="col-md-12">
        <div class="wrapper-logo-secondary">
            <img src="img/logo/Law.jpg" alt="Logotipo Firma Law">
        </div>
    </div>
</div>


<div class="wrapper-title">
    <div class="container">
        <div class="row">
            <div class="col-md-4" data-line="mobil">
                <div class="line"></div>
            </div>
            <div class="col-md-4 section-title">
                <h1><i class="fa fa-window-restore" aria-hidden="true"></i> FACTURACI&Oacute;N</h1>
            </div>
            <div class="col-md-4">
                <div class="line"></div>
            </div>
        </div>
    </div>
</div>


    <form action="menu.php?id=25" method="post">
        <div class="col-md-12">
            <div class="col-md-2  offset-md-2">
                <label for="">TIPO DE PAGO</label>
                <select name="tipo_pago" id="tipo_pago" class="form-control" autofocus="" required="">
                    <option value="">SELECCIONAR</option>
                    <option value="1">CONTADO</option>
                    <option value="2">CREDITO</option>
                </select>
            </div>
            <div class="col-md-2">
                <label for="">TIPO DOCUMENTO</label>
                <select name="tipo_documento" id="tipo_doc" class="form-control" required="">
                    <option value="">SELECCIONAR</option>
                    <option value="F">FACTURA</option>
                    <option value="R">RECIBO</option>
                </select>        
            </div>
        
            <div class="col-md-2" id="oculta_docu">
                <label for="">SERIE</label>
                <input type="text" name="serie" id="serie" class="form-control wrapper-upper" placeholder="Serie">    
            </div>
            <div class="col-md-2 wrapper-centrar">
                <label for="">FACTURA / RECIBO</label>
                <input type="text" id="factura" name="factura" class="form-control upper" placeholder="Factura / Recibo" onChange="return concat();">            
            </div>
            <div class="col-md-2 wrapper-ocultar" id="ocultar">
                <label for="">D&Iacute;AS</label>
                <input type="text" name="dias" class="form-control upper center" placeholder="D&iacute;as">            
            </div>
            <div class="col-md-2">
                <label for="">FECHA</label>
                <input type="text" name="fecha" class="form-control fecha" id="datepicker" value="">            
            </div>
        </div>
            
        <div class="col-md-12 wrapper-space wrapper-centrar">
            <div class="col-md-2">
                <label for="">TOTAL</label>
                <input type="number" name="total" id="monto_total" class="form-control upper center" placeholder="Total" required="" value="0.00"; readonly>            
            </div>
            <div class="col-md-2">
                <label for="">TC</label>
                <input type="number" id="tc" class="form-control upper center" name="tc" onChange="sumar (this.value);" value="0.00";>            
            </div>
            <div class="col-md-2">
                <label for="">TE</label>
                <input type="number" id="te" class="form-control upper center" name="te" onChange="sumar (this.value);"
                    value="0.00";>            
            </div>
            <div class="col-md-2">
                <label for="">TTC</label>
                <input type="number" id="ttc" class="form-control upper center" name="ttc" onChange="sumar (this.value);"  value="0.00";>            
            </div>
        </div>

        <div class="col-md-12 wrapper-space">
            <div class="col-md-4 wrapper-centrar">
                <label for="">CLIENTE</label>
                <select name="cliente" id="cliente" class="form-control" required="">
                    <option value="">SELECCIONAR</option>
                <?php
                    while ($row = mysqli_fetch_array($sql1))
                    {
                        echo '<option value="' . $row['ID_CONTACTO']. '">'. $row['NOMBRE'] . '</option>' . "\n";
                    }
				?>
                </select>            
            </div>
            <div class="col-md-3 wrapper-centrar">
                <label for="">NOMBRE</label>
                <input type="text" id="nombre" name="nombre" class="form-control" placeholder="NOMBRE" readonly="">
            </div>
            <div class="col-md-3 wrapper-centrar">
                <label for="">DIRECCI&Oacute;N</label>
                <input type="text" id="direccion" name="direccion" class="form-control" placeholder="DIRECCI&Oacute;N" readonly="">
            </div>
            <div class="col-md-2 wrapper-centrar">
                <label for="">NIT</label>
                <input type="text" id="nit" name="nit" class="form-control center" placeholder="NIT" readonly="">
            </div>                         
        </div>
        <div class="col-md-12 wrapper-space">
            <div class="col-md-12">
                <label for="">OBSERVACIONES</label>
                <input type="text" id="observa_encabeza" name="observa_encabeza" class="form-control wrapper-upper" placeholder="Observaciones">
            </div>
        </div>

        <div class="col-md-12 table-responsive">
            <div class="col-md-12 wrapper-space">
                <table id="dynamic_field" class="table table-striped table-bordered ">
                    <thead>
                        <tr>
                        <th class="wrapper-centrar">Caso</th>
                        <th class="wrapper-centrar">Descripci&oacute;n Caso</th>
                        <th class="wrapper-centrar">Observaciones</th>
                        <th class="wrapper-centrar">Saldo Caso</th>
                        <th class="wrapper-centrar">Total</th>
                        <th class="wrapper-centrar"><i class="fa fa-plus-circle" aria-hidden="true"></i> Detalle</th>                        
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <div class="col-md-2">
                                    <select name="caso[]" id="caso0" data="caso" class="form-control" style="width: 200px;">
                                        <option value=""></option>
                                    </select>
                                </div>
                            </td>
                            <td>
                                <div class="col-md-4">
                                    <input type="text" name="descripcion[]" id="descripcion0" class="form-control upper" placeholder="DESCRIPCI&Oacute;N CASO" readonly="" style="width: 250px;">
                                </div>                        
                             </td>
                             <td>
                                <div class="col-md-4">
                                    <input type="text" name="observaciones[]" id="observaciones0" class="form-control upper" placeholder="OBSERVACIONES CASO" style="width: 250px;">
                                </div>
                             </td>
                             <td>
                                <div class="col-md-4">
                                    <input type="text" name="saldo_caso[]" id="saldo_caso0" class="form-control input-table center" style="width: 100px;" readonly="">
                                </div>                       
                             </td>
                             <td>
                                <div class="col-md-4">
                                    <input type="text" name="moneda[]" id="moneda0" class="form-control center monto" data="moneda" placeholder="TOTAL" style="width: 150px;" required="">
                                </div>
                             </td>
                             <td>
                                <div class="col-md-4">
                                    <button type="button" name="add" id="add" class="btn btn-primary boton_add"><i class="fa fa-plus-circle" aria-hidden="true"></i></button>
                                </div>
                             </td>                              
                        </tr>
                    </tbody>
                </table>            
            </div>
        </div>

    <div class="col-md-12">
        <div class="col-md-9"></div>
        <div class="col-md-3">
        <label for="">TOTAL:</label>
        <input type="text" name="suma" value="" id="test" class="form-control total center wrapper-total" readonly="">
        </div>
    </div>

    <div class="col-md-12">
        <div class="col-md-4"></div>
        <div class="col-md-2">
            <button type="submit" id="boton" class="btn btn-success">GRABAR</button>
        </div>
        <div class="col-md-2 ocultar-boton" id="mostrar-boton">

            <?php
            if(isset($_REQUEST['num_fac']))
                {
                $var = $_REQUEST['num_fac'];  
                echo "<button type='button' class='btn btn-warning'><a href='factura_pdf.php?num_factura=$var' target='_blank'>IMPRESI&Oacute;N</a></button>";
                }
                else{
                    $var = '0';    
                }

            ?>

        </div>
    </div>

</form>        

<script>
    $(document).ready(function(){
        var i=0;
        
        $('#cliente').change(function(){
            var cliente = $('#cliente').children('option:selected').val();
            
            document.cookie = cliente;

            $.post('datos_clientes.php', {cliente: cliente}).done(function( respuesta )
            {
                $('#nombre').val(respuesta);
            })
            $.post('address_clientes.php', {cliente: cliente}).done(function( respuesta )
            {
                $('#direccion').val(respuesta);
            })
            $.post('nit_clientes.php', {cliente: cliente}).done(function( respuesta )
            {
                $('#nit').val(respuesta);
            })

            $.post('casos_clientes.php', {cliente: cliente}).done(function( respuesta )
            {
                for (j = 0; j < i+1; j++) { 
                    $('select[id="caso'+j+ '"]').html(respuesta);
                    $("#descripcion"+j).val("");
                    $("#observaciones"+j).val("");
                    $("#saldo_caso"+j).val("");
                    }
            })   
  
        })
        //CMOTTA 20190823 Agregar funcionalidad para cuando hay más de un caso
       
        $("body").on('click', "select", function(event){
            //CMOTTA 20190827 Permite obtener información de los elementos creados dinámicamente
            event.preventDefault();
         
            var k = $(this).attr('id');
            k = k.substr(4);

            var caso = $("#caso"+ k).children('option:selected').val();
           
            $.post('desc_casos.php', {caso: caso}).done(function( respuesta )
            {
                $('#descripcion'+ k).val(respuesta) ;

            })
            $.post('saldo_caso.php', {caso: caso}).done(function( respuesta )
            {
                $('#saldo_caso'+ k).val(respuesta);
            })            

        })
    

        $('#moneda0').change(function(){

            var monto_ingresado = $('#moneda0').val();
            
            var saldo_caso      = $('#saldo_caso').val();

            if(monto_ingresado > saldo_caso){
                alert('El monto del Caso es Mayor al Saldo...!!!! VERIFICAR...');
                return false;
            }else{

            }

        })

        $(".monto").change(function(){
            
            var total = 0;
            
            $(".monto").each(function() {

            if (isNaN(parseFloat($(this).val()))) {

            total += 0;

            }else{
                total += parseFloat($(this).val());
            }
            });
            document.getElementById('test').value = total;
        })
        
     
        $('#add').click(function(event){
            i++;
            var contacto = document.cookie;
            //CMOTTA 20190905 Selecciona el codigo del cliente
            var x = document.getElementById("cliente").value;
            var button = document.getElementById(i-1);

            //CMOTTA 20190823 Agrega una nueva linea al detalle de la factura
            $('#dynamic_field').append('<tr id="row'+i+'"><td><div class="col-md-2"><select name="caso[]" id="caso'+i+'" data="caso'+i+'" class="form-control" style="width: 200px;"><option value="-1">SELECCIONAR CASO</option></select></div></td><td><div class="col-md-4"><input type="text" name="descripcion[]" id="descripcion'+i+'" class="form-control upper" placeholder="DESCRIPCI&Oacute;N CASO" readonly="" style="width: 250px;"></div></td><td><div class="col-md-4"><input type="text" name="observaciones[]" id="observaciones'+i+'" class="form-control upper" placeholder="OBSERVACIONES CASO" style="width: 250px;"></div></td><td><div class="col-md-4"><input type="text" name="saldo_caso[]" id="saldo_caso'+i+'" class="form-control input-table center" style="width: 100px;" readonly=""></div></td><td><div class="col-md-4"><input type="text" name="moneda[]" id="moneda'+i+'" class="form-control center monto" data="moneda'+i+'" placeholder="TOTAL" style="width: 150px;" required=""></div></td><td class="wrapper-centrar"><button type="button" id="'+i+'" class="btn btn-danger btn_remove" name="remove"><i class="fa fa-minus-circle" aria-hidden="true"></i></button></td></tr>');
            if (i > 1) {
               button.disabled = true;
            } 
            //CMOTTA 20190827 Permite obtener información de los elementos creados dinámicamente
            event.preventDefault();
            $.post('caso_cliente_x.php', {x: x}).done(
                function(respuesta)
                    {
                        $('select[data="caso'+i+'"]').html(respuesta);   
                    }
                ) 
        
            $(".monto").change(function(){
                var total = 0;
                
                $(".monto").each(function() {

                if (isNaN(parseFloat($(this).val()))) {
                    total += 0;
                }else{
                    total += parseFloat($(this).val());
                }
                });

                document.getElementById('test').value = total;
            })        
        })

// CMOTTA 20190824 Elimina la fila y habilita el botón para remover
        $(document).on('click','.btn_remove', function (){
            var button_id = $(this).attr("id");
            var button = document.getElementById(i-1);
            if (i > 1) {
              button.disabled = false;  
            }
            

            $('#row'+button_id+'').remove();
            i--;
        })

    })
</script>

<script>
    function validacion(){
        var tc = document.getElementById('tc').value;
        var te = document.getElementById('te').value;
        var ttc = document.getElementById('ttc').value;
        var monto = document.getElementById('monto_total').value;
        var fecha = document.getElementById('datepicker').value;

        var total = document.getElementById('test').value;

        suma = parseFloat(tc)+parseFloat(te)+parseFloat(ttc);

        if(total != monto){
            alert('El Total no coincide con los montos...');
            return false;
        }
    }
</script>

<script>
    $("#tipo_pago").change(function(){
        var tipo_pago = document.getElementById('tipo_pago').value; 
        
        if(tipo_pago == 1){
            $('#ocultar').addClass('wrapper-ocultar');
            $('#tc').attr('readonly',false);
            $('#te').attr('readonly',false);
            $('#ttc').attr('readonly',false);
            $('#serie').attr('required',true);
            $('#factura').attr('required',true);
            $('#monto_total').attr('readonly',true);
        }
        if(tipo_pago != 1){
            $('#ocultar').removeClass('wrapper-ocultar');
            $('#tc').attr('readonly',true);
            $('#te').attr('readonly',true);
            $('#ttc').attr('readonly',true);
            $('#serie').attr('required',false);
        }

        if(tipo_docu == 'R'){
            $('#ocultar').removeClass('wrapper-ocultar');
        }
        })

    $("#tipo_doc").change(function(){
        var tipo_docu = document.getElementById('tipo_doc').value;

        if(tipo_docu == 'R'){
            $('#oculta_docu').addClass('wrapper-ocultar');
            $('#factura').attr('readonly',true);
            $('#serie').attr('required',false);
        }

        if(tipo_docu != 'R'){
            $('#oculta_docu').removeClass('wrapper-ocultar');
            $('#factura').attr('readonly',false);
            $('#serie').attr('required',true);
        }
        })     


        $('#boton').click(function(){
            $('#mostrar-boton').removeClass('ocultar-boton');
        })

</script>

<script>
    function sumar (valor){
        var total = 0;
        
        var tc = document.getElementById('tc').value;
        var te = document.getElementById('te').value;
        var ttc = document.getElementById('ttc').value;
        
        //valor = parseInt(valor); //Convierte el valor en un entero (número).

        total = document.getElementById('monto_total').value;

        total = (total == null || total == undefined || total == "") ? 0 : total;

        //total = (parseInt(total) + parseInt(valor));

        total = (parseFloat(tc) + parseFloat(te) + parseFloat(ttc));

        document.getElementById('monto_total').value = total;
    }
</script>

<script>
    function concat(){
        var serie   = document.getElementById('serie').value;
        var factura = document.getElementById('factura').value;
        var nombre = "Factura";

        var cadena = nombre + " " + serie + " - " + factura;
        $('#observa_encabeza').val(cadena);
    }
</script>        

    <script>
        $('#datepicker').datepicker({ format: 'dd-mm-yyyy' });
    </script>