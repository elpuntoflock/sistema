<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="lib_alert/css/sweetalert.css">
<script type="text/javascript" src="lib_alert/js/sweetalert-dev.js"></script>


<script>
	function ErrorAcceso()
		{
		swal({title:"Su usuario no tiene privilegios para esta pantalla..!", type:"error", showConfirmButton:false, text:"COMUNIQUESE CON EL ADMINISTRADOR", timer:'1500'}, 
		function () 
		{
		location.href = "menu.php?id=33"; 
		});
		}
</script>
<?php
require_once 'controller/seguridad.php';
require_once 'controller/controller.php';
require_once 'db/conexion.php';

$usuario 	= $_SESSION['usuario'];
$usuario 	= strtoupper($usuario);

require_once('db/conexion.php');

$sql_permiso = mysqli_query($conn, "SELECT COUNT(*)CUENTA
										FROM tb_acceso_item
										WHERE id_usuario = '".$usuario."'
										  AND ITEM = ".$_REQUEST['id']."");
								
	while($valida = $sql_permiso->fetch_array(MYSQLI_ASSOC)){

		$resultado = $valida['CUENTA'];
	}

	if($resultado == 1){
		
	}else{
		echo "<script>ErrorAcceso();</script>";
	}

if(isset($_POST['contacto']))
    {
      $contacto = $_POST['contacto'];  
    }
    else{
      $contacto = '';
		}
		
		if(isset($_POST['moneda']))
    {
      $moneda = $_POST['moneda'];  
    }
    else{
      $moneda = '';
    }		


$detalle = mysqli_query($conn, "SELECT B.ID_CASO, B.DESCRIPCION, A.SALDO, A.MONEDA, B.CAUSA
								FROM tb_corriente A,
									 	 tb_caso B,
								     tb_contacto C
								WHERE A.ID_CASO 	= B.ID_CASO
								  AND B.ID_CONTACTO = C.ID_CONTACTO
									AND C.ID_CONTACTO = '".$contacto."'
									AND A.MONEDA 		= '".$moneda."'");
						
									
$saldo = mysqli_query($conn, "SELECT SUM(A.SALDO) DETALLE, A.MONEDA
                                FROM tb_corriente A,
                                            tb_caso B,
                                        tb_contacto C
                                WHERE A.ID_CASO 	= B.ID_CASO
                                    AND B.ID_CONTACTO = C.ID_CONTACTO
                                    AND C.ID_CONTACTO = '".$contacto."'
                                    AND A.MONEDA 		= '".$moneda."'");
                                                                

    $sql_permiso = mysqli_query($conn, "SELECT COUNT(*)CUENTA
                                        FROM tb_acceso_item
                                        WHERE id_usuario = '".$nombre."'
                                            AND ITEM = ".$_REQUEST['id']."");
                                                        
    while($valida = $sql_permiso->fetch_array(MYSQLI_ASSOC)){

        $resultado = $valida['CUENTA'];
    }

    if($resultado == 1){
        
    }else{
        echo "<script>ErrorAcceso();</script>";
    }

?>

<script type="text/javascript">

    function selectItemByValue(elmnt, value){

    for(var i=0; i < elmnt.options.length; i++)
    {
        if(elmnt.options[i].value == value)
        elmnt.selectedIndex = i;
    }
    }

</script>

<div class="row">
    <div class="col-md-12">
        <div class="wrapper-logo-secondary">
            <img src="img/logo/Law.jpg" alt="Logotipo Firma Law">
        </div>
    </div>
</div>

<div class="wrapper-title">
    <div class="container">
        <div class="row">
            <div class="col-md-4" data-line="mobil">
                <div class="line"></div>
            </div>
            <div class="col-md-4 section-title">
                <h1><!--i class="fa fa-credit-card" aria-hidden="true"></i> <i class="fa fa-users" aria-hidden="true"></i--> CUENTA CORRIENTE POR CLIENTE </h1>
            </div>
            <div class="col-md-4">
                <div class="line"></div>
            </div>
        </div>
    </div>
</div>

<div class="wrapper-title">
    <div class="container">
        <div class="row">
            <form action="menu.php?id=16" method="post">
                <div class="col-md-7 wrapper-centrar">
                    <label for="">SELECCIONAR CLIENTE</label>
                    <select  name="contacto" class="form-control upper" id="contacto" placeholder="SELECCIONAR CLILENTE">
                    <option value="">SELECCIONAR CLIENTE</option>
			            <?php
			            while ($rowx = mysqli_fetch_array($sql))
			            {
			              echo '<option value="' . $rowx['ID_CONTACTO']. '">' . $rowx['NOMBRE'] . '</option>' . "\n";
			            }
			            ?>                         
                    </select>
                    <script language="javascript">
                        var numberMI = document.getElementById("contacto");
                        selectItemByValue(numberMI,<?= "'".$contacto."'"?>);
                    </script>                    
                </div>
                <div class="col-md-4 wrapper-centrar">
                    <label for="">SELECCIONAR CAUSA</label>
                    <select name="moneda" class="form-control" id="moneda">
                        <option value="Q">QUETZALES</option>
                        <option value="$">DOLARES</option>
                    </select>
                </div>
                <div class="col-md-1" style="margin-top: 30px;">
                    <button type="submit" class="btn btn-success"><i class="fa fa-search" aria-hidden="true"></i> BUSCAR</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="card wrapper-space">
    <div class="wrapper-centrar card-header">
        <!--button type="button" class="btn btn-success" data-toggle="modal" data-target="#ModalCrea"><i class="fa fa-address-card" aria-hidden="true"></i> Crear Casos Nuevos</button-->
        <a href="view/rep_general_clientes.php?tfc=<?= $contacto?>&mon=<?= $moneda ?>" target="_blanck"><button type="button" class="btn btn-warning"><i class="fa fa-file" aria-hidden="true"></i> Reporte PDF</button></a>
        <!--a href="view/repo_casos_excel.php"><button type="button" class="btn btn-primary"><i class="fa fa-table" aria-hidden="true"></i> Reporte Excel</button></a-->
    </div>
</div>

<div class="container wrapper-space" style="margin-top: 45px;">
    <div class="row">
        <div class="col-md-12">
            <table id="example" class="table table-striped table-bordered dt-responsive nowrap">
                <thead>
                    <tr>
                        <th class="centrar">IR A DETALLE</th>
						<th class="centrar">CAUSA</th>                        
						<th class="centrar">DESCRIPCI&Oacute;N DEL CASO</th>
						<th class="centrar">SALDO</th>
                    </tr>
                </thead>   
                <tbody>
                    <?php
                   while ($res = mysqli_fetch_array($detalle)){

                        $plata 	= number_format($res[2],2,'.',',');
                    
                        echo "<tr>";
                            echo "<td class='center'>";
                                echo "<a href='menu.php?id=49&fft=$res[0]'><i class='fa fa-search' aria-hidden='true'></i> Detalle</a>";
                            echo "</td>";
                            echo "<td>";
                                echo $res[4];
                            echo "</td>"; 
                            echo "<td>";
                                echo $res[1];
                            echo "</td>"; 
                            echo "<td>";
                                echo $res[3]."&nbsp&nbsp".$plata;
                            echo "</td>";                                                                                   
                        echo "</td>";
                            
                        echo "</tr>";                          
                   
                    }
                    ?>  
                                  
                </tbody>             
            </table>
        </div>
    </div>
</div>