<?php
require_once '../fpdf/fpdf.php';
require_once '../db/conexion.php';

$caso = $_REQUEST['ftc'];

$detalle = mysqli_query($conn, "SELECT DATE_FORMAT(FECHA,'%d/%m/%Y')FEC, DESCRIPCION, MONTO, TIPO, MONEDA
                                  FROM tb_cargo_abono
                                 WHERE TIPO <> 6
                                   AND ID_CASO = '".$caso."'
                                ORDER BY FECHA ASC");

$sql1 = mysqli_query($conn, "SELECT SALDO
                                FROM tb_corriente
                            WHERE ID_CASO = '".$caso."'");

    while($rowAA = $sql1->fetch_array(MYSQLI_ASSOC)){
        $saldo_caso = number_format($rowAA['SALDO'],2,'.',',');
    }                                    


 $det_causa = mysqli_query($conn, "SELECT CAUSA
                                    FROM tb_caso
                                    WHERE ID_CASO = '".$caso."'");   

$pdf = new FPDF();
$pdf->AddPage();
$pdf->SetFont('Arial','',12);

$pdf->Image('../img/logo/Law.jpg',25,15,35,0);

$pdf->SetY(40);


$pdf->Cell(200,10,'CONSULTA GENERAL DE CASOS POR CLIENTE',0,1,'C');

$pdf->SetY(70);


$pdf->SetFont('Arial','',8);

$pdf->Cell(40,5,'FECHA', 1,0,'C');
$pdf->Cell(110,5,'DETALLE', 1,0,'C');
$pdf->Cell(40,5,'MONTO', 1,1,'C');

while ($rest = mysqli_fetch_array($detalle)){

    $fecha          = $rest[0];
    $descripcion    = strtoupper($rest[1]);
    $moneda         = $rest[4];
    $saldo          = number_format($rest[2],2,'.',',');
    
    
    $pdf->Cell(40,5, $fecha , 1, 0, 'C');
    $pdf->Cell(110,5, $descripcion ,1, 0, 'L');
    $pdf->Cell(40,5, $moneda.' '.$saldo  ,1, 1, 'C');

}

$pdf->Cell(150,5, 'SALDO DEL CASO:  ' , 1, 0, 'C');
$pdf->Cell(40,5, $moneda.'  '.$saldo_caso ,1, 1, 'C');

$pdf->SetY(140);

$pdf->Cell(160,10,'Firma:____________________________________________',0,1,'C');

$pdf->Output();

?>