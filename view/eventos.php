<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="lib_alert/css/sweetalert.css">
<script type="text/javascript" src="lib_alert/js/sweetalert-dev.js"></script>


<script>
	function ErrorAcceso()
		{
		swal({title:"Su usuario no tiene privilegios para esta pantalla..!", type:"error", showConfirmButton:false, text:"COMUNIQUESE CON EL ADMINISTRADOR", timer:'1500'}, 
		function () 
		{
		location.href = "menu.php?id=33"; 
		});
		}
</script>
<?php
$usuario 	= $_SESSION['usuario'];
$usuario 	= strtoupper($usuario);
require_once 'controller/controller.php';
require_once 'db/conexion.php';




if(isset($_POST['caso']))
    {
      $caso = $_POST['caso'];  
    }
    else{
      $caso = '';        
    }

    $tareas = mysqli_query($conn, "SELECT A.ID, A.DESCRIPTION, DATE_FORMAT(A.START,'%d/%m/%Y %H:%i:%s') FECHA, DATE_FORMAT(A.END,'%d/%m/%Y %H:%i:%s') FECHA1, TITLE,
										  A.OBSERVACIONES, A.responsable
                                    FROM events A,
                                         tb_acceso B
                                    WHERE A.ID_CASO     = B.ID_CASO
                                    AND A.ID_CASO       = '".$caso."'
                                    AND B.ID_USUARIO    = '".$usuario."'
									ORDER BY FECHA ASC");    
									

	$sql_permiso = mysqli_query($conn, "SELECT COUNT(*)CUENTA
										FROM tb_acceso_item
										WHERE id_usuario = '".$nombre."'
											AND ITEM = ".$_REQUEST['id']."");
							
while($valida = $sql_permiso->fetch_array(MYSQLI_ASSOC)){

	$resultado = $valida['CUENTA'];
}

if($resultado == 1){
	
}else{
	echo "<script>ErrorAcceso();</script>";
}									

?>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="lib_alert/css/sweetalert.css">
<script type="text/javascript" src="lib_alert/js/sweetalert-dev.js"></script>
<!--LIBRERIAS-->
<link href="calendar/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">	
<link href="calendar/css/bootstrap-colorpicker.css" rel="stylesheet">


  <script type="text/javascript">

    function selectItemByValue(elmnt, value){

    for(var i=0; i < elmnt.options.length; i++)
    {
        if(elmnt.options[i].value == value)
        elmnt.selectedIndex = i;
    }
    }

</script>

<style>
/* Important part */
.modal-dialog{
    overflow-y: initial !important
}
.cuerpo{
    height: 650px;
    overflow-y: auto;
}
.wrapper-ocultar {
    display: none;
}
</style>


<div class="row">
    <div class="col-md-12">
        <div class="wrapper-logo-secondary">
            <img src="img/logo/Law.jpg" alt="Logotipo Firma Law">
        </div>
    </div>
</div>

<div class="wrapper-title">
    <div class="container">
        <div class="row">
            <div class="col-md-4" data-line="mobil">
                <div class="line"></div>
            </div>
            <div class="col-md-4 section-title">
                <h1><i class="fa fa-envelope" aria-hidden="true"></i> TAREAS</h1>
            </div>
            <div class="col-md-4">
                <div class="line"></div>
            </div>
        </div>
    </div>  
</div>



<div class="wrapper-space">
    <form action="menu.php?id=12" method="post">
        <div class="col-md-12">
            <div id="ocultar" class="col-md-6 offset-md-2 wrapper-centrar">
                <label for="">SELECCIONAR CAUSA</label>
                <select name="caso" id="caso" class="form-control">
                    <option value="0">Seleccionar Causa</option>
                <?php
                    while ($row = mysqli_fetch_array($events))
                    {
                    echo '<option value="' . $row['ID_CASO']. '">' . $row['DETALLE_CAUSA'] .' - '. utf8_encode(strtoupper($row['DESCRIPCION'])).'</option>' . "\n";
                    }
                    ?>                
                </select>
                <script language="javascript">
                    var numberMI = document.getElementById("caso");
                    selectItemByValue(numberMI,<?= "'".$caso."'"?>);
                </script>            
            </div>
	
            <div class="col-md-1 wrapper-button">
                <button type="submit" class="btn btn-success"><i class="fa fa-search" aria-hidden="true"></i> Buscar</button>
            </div>        
        </div>
    </form>
</div>


    <div class="card table-space">
        <div class="wrapper-centrar card-header">
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-envelope" aria-hidden="true"></i> Crear Nueva Tarea</button>
            <a href="view/repo_eventos.php?ftc=<?= $caso; ?>" target="_blanck"><button type="button" class="btn btn-warning"><i class="fa fa-file" aria-hidden="true"></i> Reporte PDF</button></a>
            <!--button type="button" class="btn btn-primary"><i class="fa fa-table" aria-hidden="true"></i> Reporte Excel</button-->
        </div>
    </div>



<div class="container" style="margin-top: 40px;">
    <div class="row">
        <div class="col-md-12">
   
            <table id="example" class="table table-striped table-bordered dt-responsive nowrap" >
                <thead>
                    <tr>
                        <th class='center'>TIPO TAREA</th>
                        <th class='center'>DESCRIPCI&Oacute;N TAREA</th>
						<th class='center'>OBSERVACIONES</th>
						<th class='center'>RESPONSABLE DE TAREA</th>
                        <th class='center'>FECHA INICIO</th>
                        <th class='center'>FECHA FINAL</th>
                    </tr>
                </thead>   
                <tbody>
                    <?php
                   while ($row = mysqli_fetch_array($tareas)){

                        echo "<tr>";
                            echo "<td class='wrapper-centrar'>";
                                echo $row[4];
                            echo "</td>";                        
                            echo "<td>";
                                echo $row[1];
							echo "</td>";
                            echo "<td>";
                                echo $row[5];
							echo "</td>";
                            echo "<td>";
                                echo $row[6];
                            echo "</td>";							 							
                            echo "<td class='wrapper-centrar'>";
                                echo $row[2];
                            echo "</td>";
                            echo "<td class='wrapper-centrar'>";
                                echo $row[3];
							echo "</td>";  
							                          
                        echo "</tr>";                          
                   
                    }
                    ?>                  
                </tbody>             
            </table>
        </div>
    </div>
</div>

<!--MODAL TAREA-->
<div class="modal fade" id="exampleModal" role="dialog">
    <div class="modal-dialog">
    
      	<div class="modal-content">
			<div class="modal-header">
			
			<h4 class="modal-title" id="myModalLabel"> <i class="fa fa-calendar" aria-hidden="true"></i> INGRESO DE TAREAS</h4>
			</div>
			<div class="modal-body cuerpo">
			<?php

			

			$valor = 1;

			
				echo "";
			

			?>

				<form action="menu.php?id=13" method="post" onSubmit="return validacion();">
					<div class="wrapper-space wrapper-centrar">
						<label>SELECCIONAR TIPO</label>
						<select name='title' class="form-control input-md">
							
							<?php 
							require_once('db/conexion.php');
							$query = mysqli_query($conn, "select * from type ORDER BY id DESC");
							
								echo "<option value='No type Selected' required>SELECCIONAR TIPO</option>";
								
							while ($row = mysqli_fetch_assoc($query)) {
									
								echo "<option value='".$row['title']."'>".$row['title']."</option>";
													
								}
						
							?>
						</select>
					</div>

					<div class="wrapper-space wrapper-centrar">
						<label>SELECCIONAR CAUSA</label>
						<select name="id_caso" id="id_caso" class="form-control abono">
							<option value="-1">SELECCIONAR CASO</option>
								<?php

								$sql = mysqli_query($conn, "SELECT A.ID_CASO, A.DESCRIPCION, CASE 
															WHEN A.CAUSA = '' THEN 'SIN CAUSA'
															ELSE A.CAUSA
															END AS DETALLE_CAUSA
															FROM tb_caso A,
																	tb_acceso B
														WHERE A.ID_CASO = B.ID_CASO
															AND B.ID_USUARIO = '".$usuario."'");


								while ($row = mysqli_fetch_array($sql))
								{
								echo '<option value="' . $row['ID_CASO']. '">' . $row['DETALLE_CAUSA'] .'-'. $row['DESCRIPCION'] .'</option>' . "\n";
								}
								?>                  
						</select>
						<script language="javascript">
							var numberMI = document.getElementById("id_caso");
							selectItemByValue(numberMI,<?= "'".$caso."'"?>);
						</script>						
					</div>

					<div class="wrapper-space wrapper-centrar">
						<label>FECHA INICIO</label>
						<div class="input-group date form_date" data-date="" data-date-format="yyyy-mm-dd hh:ii" data-link-field="start" data-link-format="yyyy-mm-dd hh:ii">
							<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span><input id="ini" class="form-control" size="16" type="text" value="" readonly="" onchange="return FuncionConsulta();">
						</div>
						<input id="start" name="start" type="hidden" value="" required="" >
					</div>

					<div id="result" class="wrapper-space"></div>

					<div class="wrapper-space wrapper-centrar">
						<label>FECHA FINAL</label>
						<div class="input-group date form_date" data-date="" data-date-format="yyyy-mm-dd hh:ii" data-link-field="end" data-link-format="yyyy-mm-dd hh:ii">
							<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span><input class="form-control" size="16" type="text" value="" readonly>
						</div>
						<input id="end" name="end" type="hidden" value="">

					</div>
				
					<div class="wrapper-space wrapper-centrar">
						<label>DESCRIPCI&Oacute;N</label>
						<input type="text" class="form-control upper" name="description" id="description">
					</div>

					<div class="wrapper-space wrapper-centrar">
						<label>OBSERVACIONES</label>
						<input type="text" class="form-control upper" name="observa" id="observa">
					</div>

					<div class="wrapper-space wrapper-centrar">
							<label for="">NOTIFICAR EMAIL No. 1</label>
							<select name="email" id="" class="form-control centrar">
								<option value="">NOTIFICAR A EMAIL No. 1</option>
								<?php
									//require_once('db/conexion.php');
									$email1 = mysqli_query($conn, "SELECT ID_CONTACTO, EMAIL, TRIM(CONCAT_WS(' ', NOMBRES,' ',APELLIDOS,' ',NOMBRE_EMPRESA))NOMBRE
																	FROM tb_contacto
																	WHERE EMAIL IS NOT NULL");
									while ($rowX = mysqli_fetch_array($email1))
									{
										echo '<option value="' . $rowX['EMAIL']. '">' . $rowX['NOMBRE'] .' - '. $rowX['EMAIL'] . '</option>' . "\n";
									}
									?> 							
							</select>

					<div style="text-align: left;" class="bajar wrapper-space">
						<a data-toggle='collapse' href='#demo' data-toggle="collapse"><i class="fa fa-plus-circle" aria-hidden="true"></i> Agregar Nuevo Correo</a>
					</div>

					</div>


					<div id="demo" class="collapse bajar wrapper-space wrapper-centrar">
						<label for="">NOTIFICAR EMAIL No. 2</label>
						<select name="email2" id="" class="form-control">
								<option value="">NOTIFICAR A EMAIL No. 2</option>
								<?php

								require_once('db/conexion.php');

								$email2 = mysqli_query($conn, "SELECT ID_CONTACTO, EMAIL, TRIM(CONCAT_WS(' ', NOMBRES,' ',APELLIDOS,' ',NOMBRE_EMPRESA))NOMBRE
																FROM tb_contacto
																WHERE EMAIL IS NOT NULL");
								while ($rowX1 = mysqli_fetch_array($email2))
								{
									echo '<option value="' . $rowX1['EMAIL']. '">' . $rowX1['NOMBRE'] .' - '. $rowX1['EMAIL'] . '</option>' . "\n";
								}
								?> 								
						</select>

						<div style="text-align: left;" class="bajar wrapper-space">
							<a data-toggle='collapse' href='#email8' data-toggle="collapse"><i class="fa fa-plus-circle" aria-hidden="true"></i> Agregar Nuevo Correo</a>
						</div>

					</div>

					<div id="email8" class="collapse bajar wrapper-space wrapper-centrar">
							<label for="">NOTIFICAR EMAIL No. 3</label>
							<select name="email3" class="form-control">
									<option value="">NOTIFICAR A EMAIL No. 3</option>
									<?php

									require_once('db/conexion.php');

									$emailX = mysqli_query($conn, "SELECT ID_CONTACTO, EMAIL, TRIM(CONCAT_WS(' ', NOMBRES,' ',APELLIDOS,' ',NOMBRE_EMPRESA))NOMBRE
																	FROM tb_contacto
																	WHERE EMAIL IS NOT NULL");
									while ($rowX2 = mysqli_fetch_array($emailX))
									{
										echo '<option value="' . $rowX2['EMAIL']. '">' . $rowX2['NOMBRE'] .' - '. $rowX2['EMAIL'] . '</option>' . "\n";
									}
									?> 									
							</select>
					</div>
					<div class="wrapper-space wrapper-centrar">
						<label for="">DIAS PREVIOS AL AVISO:</label>
						<select name="dias" class="form-control">
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
								<option value="6">6</option>
								<option value="7">7</option>
								<option value="8">8</option>
								<option value="9">9</option>
								<option value="10">10</option>
						</select>
					</div>	


					<div class="wrapper-space wrapper-centrar">
						<label>RESPONSABLE</label>
						<input type="text" class="form-control upper" name="responsable" placeholder="Responsable">
					</div>						
				
					<div class="modal-footer wrapper-centrar">
						<button type="submit" class="btn btn-success"><i class="fa fa-hdd" aria-hidden="true"></i> GRABAR</button>
						<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> CERRAR</button>
					</div>			
				</form>
			</div>

      	</div>
      
    </div>
  </div>

  <script type="text/javascript" src="calendar/js/bootstrap-datetimepicker.js"></script>
  <!--script src="calendar/js/bootstrap-colorpicker.js"></script-->
  <script type="text/javascript">
	$('.form_date').datetimepicker({
		language:  'en',
		weekStart: 1,
		todayBtn:  0,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		forceParse: 0
	});
</script>
<script>
	function validacion() {

		var fecha_inicio = document.getElementById('start').value;
		var fecha_final  = document.getElementById('end').value;

		if(fecha_final < fecha_inicio){
			alert('Fecha Final no puede ser menor a la Fecha Inicio, Verificar las Fechas...!!!');
			return false;
		}
		
	}
</script>


<script>

function FuncionConsulta() {
    var formData = new FormData();
    formData.append('start',  document.getElementById("ini").value);
	formData.append('caso',  document.getElementById("id_caso").value);
     
	$.ajax({
	url:  "controller/date_event.php",
	type: "POST",
	dataType: "text",
	data: formData,
	cache: false,
	contentType: false,
	processData: false,

		})
	.done(function(res){
	$("#result").html( res);
	});

}

</script>





  