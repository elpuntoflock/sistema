<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="lib_alert/css/sweetalert.css">
<script type="text/javascript" src="lib_alert/js/sweetalert-dev.js"></script>


<script>
	function ErrorAcceso()
		{
		swal({title:"Su usuario no tiene privilegios para esta pantalla..!", type:"error", showConfirmButton:false, text:"COMUNIQUESE CON EL ADMINISTRADOR", timer:'1500'}, 
		function () 
		{
		location.href = "menu.php?id=33"; 
		});
		}
</script>
<?php
$usuario = $_SESSION['usuario'];    
require_once 'controller/controller.php';
require_once 'controller/seguridad.php';


$sql_permiso = mysqli_query($conn, "SELECT COUNT(*)CUENTA
										FROM tb_acceso_item
										WHERE id_usuario = '".$nombre."'
										  AND ITEM = ".$_REQUEST['id']."");
								
	while($valida = $sql_permiso->fetch_array(MYSQLI_ASSOC)){

		$resultado = $valida['CUENTA'];
	}

	if($resultado == 1){
		
	}else{
		echo "<script>ErrorAcceso();</script>";
    }
?>

<style>
/* Important part */
.modal-dialog{
    overflow-y: initial !important
}
.cuerpo{
    height: 650px;
    overflow-y: auto;
}
</style> 

<div class="row">
    <div class="col-md-12">
        <div class="wrapper-logo-secondary">
            <img src="img/logo/Law.jpg" alt="Logotipo Firma Law">
        </div>
    </div>
</div>

<div class="wrapper-return">
<a href="menu.php?id=1"><button type="button" class="btn btn-info"><i class="fa fa-arrow-left" aria-hidden="true"></i> Regresar</button></a>
</div>

<div class="wrapper-title">
    <div class="container">
        <div class="row">
            <div class="col-md-4" data-line="mobil">
                <div class="line"></div>
            </div>
            <div class="col-md-4 section-title">
                <h1><i class="fa fa-users" aria-hidden="true"></i> CONTACTOS</h1>
            </div>
            <div class="col-md-4">
                <div class="line"></div>
            </div>
        </div>
    </div>
</div>

<div class="card wrapper-space">
    <div class="wrapper-centrar card-header">
        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-users" aria-hidden="true"></i> Crear Nuevo Contacto</button>
        <a href="view/repo_contacto.php" target="_black"><button type="button" class="btn btn-warning"><i class="fa fa-file" aria-hidden="true"></i> Reporte PDF</button></a>
        <a href="view/repo_contacto_excel.php"><button type="button" class="btn btn-primary"><i class="fa fa-table" aria-hidden="true"></i> Reporte Excel</button></a>
    </div>
</div>

<div class="container wrapper-space" style="margin-top: 75px;">
    <div class="row">
        <div class="col-md-12">
            <table id="example" class="table table-striped table-bordered dt-responsive nowrap">
                <thead>
                    <tr>
                        <th class='center'>CODIGO</th>
                        <th class='center'>EDITAR</th>
                        <th class='center'>NOMBRE</th>
                        <th>CUI</th>
                        <th>TEL&Eacute;FONO</th>
                        <th class='center'>NIT</th>
                        <th>DIRECCI&Oacute;N</th>
                        <th>ZONA</th>
                        <th>EMAIL</th>
                        <th>NOMBRE REFERENCIA</th>
                        <th>TELEFONO REFERENCIA</th>
                        <th>CELULAR REFERENCIA</th>
                        <th>OBSERVACIONES</th>
                    </tr>
                </thead>   
                <tbody>
                    <?php
                   while ($row = mysqli_fetch_array($sql)){

                        echo "<tr>";
                            echo "<td class='wrapper-centrar'>";
                                echo $row[0];
                            echo "</td>";
                            echo "<td class='wrapper-centrar'>";
                                echo "<a href='#' data-toggle='modal' data-target='#Modal'
                                        data-id_contacto = '$row[0]'
                                        data-nom_empresa = '$row[9]'
                                        data-cargo_empre = '$row[10]'
                                        data-nombres     = '$row[11]'
                                        data-apellidos   = '$row[12]'
                                        data-ape_casada  = '$row[13]'
                                        data-cui         = '$row[2]'
                                        data-nit         = '$row[8]'
                                        data-telefono    = '$row[3]'
                                        data-direccion   = '$row[4]'
                                        data-zona        = '$row[5]'
                                        data-sexo        = '$row[14]'
                                        data-email       = '$row[6]'
                                        data-nombre_ref  = '$row[15]'
                                        data-tel_referencia  = '$row[16]'
                                        data-celular_ref = '$row[17]'
                                        data-observaciones = '$row[18]'><i class='fa fa-edit' aria-hidden='true'></i></a>";
                            echo "</td>";
                            echo "<td>";
                                echo $row[1];
                            echo "</td>";
                            echo "<td class='wrapper-centrar'>";
                                echo $row[2];
                            echo "</td>";
                            echo "<td class='wrapper-centrar'>";
                                echo $row[3];
                            echo "</td>";
                            echo "<td class='wrapper-centrar'>";
                                echo $row[8];
                            echo "</td>";
                            echo "<td>";
                                echo $row[19];
                            echo "</td>";
                            echo "<td class='wrapper-centrar'>";
                                echo $row[5];
                            echo "</td>";                            
                            echo "<td>";
                                echo strtolower($row[20]);
                            echo "</td>"; 
                            echo "<td>";
                                echo $row[21];
                            echo "</td>";
                            echo "<td>";
                                echo $row[22];
                            echo "</td>";
                            echo "<td>";
                                $celular = $row[23];
                                if($celular == 0){
                                    $celular = 'SIN CELULAR';
                                }else{
                                    $celular = $row[23];
                                }
                                echo $celular;
                            echo "</td>";
                            echo "<td>";
                                echo $row[24];
                            echo "</td>";                                                                                                                                                                                                      
                        echo "</tr>";
                    }
                    ?>                  
                </tbody>             
            </table>
        </div>
    </div>
</div>


<!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-user-plus" aria-hidden="true"></i> Creaci&oacute;n Contactos</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body cuerpo">
          <div role="tabpanel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#uploadTab" aria-controls="uploadTab" role="tab" data-toggle="tab"><i class="fa fa-plus-circle" aria-hidden="true"></i> Datos Generales</a>

                        </li>
                        <li role="presentation"><a href="#browseTab" aria-controls="browseTab" role="tab" data-toggle="tab"><i class="fa fa-plus-circle" aria-hidden="true"></i> Datos Complemento</a>

                        </li>
                    </ul>
                    <!-- Tab panes -->
                    <form action="menu.php?id=4" method="post">
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="uploadTab">
                                <div class="wrapper-space wrapper-centrar">
                                    <label for="">Nombre Empresa</label>
                                    <input type="text" name="nombre_empresa" class="form-control wrapper-upper" placeholder="Nombre Empresa" autofocus="">
                                </div>
                                <div class="wrapper-space wrapper-centrar">
						            <label for="">Cargo Empresa</label>
						            <input type="text" name="cargo_empresa" class="form-control wrapper-upper" placeholder="Cargo Empresa">
                                </div>
                                <div class="wrapper-space wrapper-centrar">
                                    <label for="">Nombres</label>
                                    <input type="text" name="nombres" class="form-control wrapper-upper" placeholder="Ingresar Nombres" >				
                                </div>
                                <div class="wrapper-space wrapper-centrar">
                                    <label for="">Apellidos</label>
                                    <input type="text" name="apellidos" class="form-control wrapper-upper" placeholder="Ingresar Apellidos">
                                </div>
                                <div class="wrapper-space wrapper-centrar">
                                    <label for="">Apellido de Casada</label>
                                    <input type="text" name="apellido_casada" class="form-control wrapper-upper" placeholder="Apellido Casada">
                                </div>
                                <div class="wrapper-space wrapper-centrar">
                                    <label for="">Profesi&oacute;n</label>	
                                    <input type="text" name="profesion" class="form-control wrapper-upper" placeholder="Profesi&oacute;n">
                                </div>
                                <div class="wrapper-space wrapper-centrar">
                                    <label for="">Cui</label>
                                    <input type="text" name="cui" class="form-control centrar wrapper-upper" placeholder="Ingresar Cui" onkeypress="return valida(event)" maxlength="13" minlength="13" required="">
                                </div>
                                <div class="wrapper-space wrapper-centrar">
                                    <label>Nit</label>
                                    <input type="text" name="nit" class="form-control wrapper-upper" placeholder="Nit">
                                </div>
                                <div class="wrapper-space wrapper-centrar">
                                    <label for="">Pasaporte</label>
                                    <input type="text" name="pasaporte" class="form-control centrar wrapper-upper" placeholder="Ingresar Pasaporte">
                                </div>
                                <div class="wrapper-space wrapper-centrar">
                                    <label for="">Tel&eacute;fono</label>
                                    <input type="text" name="telefono" class="form-control centrar wrapper-upper" placeholder="Ingresar Telefono" onkeypress="return valida(event)" maxlength="8" minlength="8" required="">
                                </div>
                                <div class="wrapper-space wrapper-centrar">
                                    <label>Direcci&oacute;n</label>
                                    <input type="text" name="direccion" class="form-control wrapper-upper" placeholder="Direcci&oacute;n" required="">
                                </div>
                                <div class="wrapper-space wrapper-centrar">
                                    <label for="">ZONA</label>
                                    <select name="zona" class="form-control" required="">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                        <option value="13">13</option>
                                        <option value="14">14</option>
                                        <option value="15">15</option>
                                        <option value="16">16</option>
                                        <option value="17">17</option>
                                        <option value="18">18</option>
                                        <option value="19">19</option>
                                        <option value="21">21</option>
                                        <option value="24">24</option>
                                        <option value="25">25</option>
                                    </select>					
                                </div>
                                <div class="wrapper-space wrapper-centrar"> 
                                    <label for="">SEXO</label>
                                    <select name="sexo" class="form-control">
                                        <option value="M">MASCULINO</option>
                                        <option value="F">FEMENINO</option>
                                    </select>
                                </div>
                                <div class="wrapper-space wrapper-centrar">
                                    <label for="">CORREO ELECTRONICO</label>
					                <input type="text" name="email" class="form-control" placeholder="Ingresar Correo Electronico" style="text-transform: lowercase;">	
                                </div>                                                                
                                <div class="wrapper-space wrapper-centrar">
                                    <label>Pais</label>
                                    <select name="pais" class="form-control" id="pais" required="">
                                    <option>Seleccionar Pais</option>
                                    </select>				
                                </div>
                                <div class="wrapper-space wrapper-centrar">
                                    <label>Departamento</label>
                                    <select name="departamento" class="form-control" id="departamento" required="">
                                        <option>Seleccionar Departamento</option>
                                    </select>
                                </div>
                                <div class="wrapper-space wrapper-centrar">
                                    <label>Municipio</label>
                                    <select name="municipio" class="form-control" id="municipio" required="">
                                        <option>Seleccionar Municipio</option>
                                    </select>
                                </div>                                                                

                            </div>

                            <div role="tabpanel" class="tab-pane" id="browseTab">
                                <div class="wrapper-space wrapper-centrar">
						            <label for="">Nombre Referencia</label>
						            <input type="text" name="nombre_ref" class="form-control wrapper-upper" placeholder="Nombre Referencia">
                                </div>
                                <div class="wrapper-space wrapper-centrar">
                                    <label for="">Relaci&oacute;n Referencia</label>
                                    <input type="text" name="relacion_ref" class="form-control wrapper-upper" placeholder="Relaci&oacute;n Referencia">
                                </div>
                                <div class="wrapper-space wrapper-centrar">
                                    <label for="">Tel&eacute;fono Referencia</label>
                                    <input type="text" name="telefono_ref" class="form-control" placeholder="Tel&eacute;fono Referencia">
                                </div>
                                <div class="wrapper-space wrapper-centrar">
                                    <label for="">Celular Referencia</label>
                                    <input type="text" name="celular_ref" class="form-control" placeholder="Celular Referencia">
                                </div>
                                <div class="wrapper-space wrapper-centrar">
                                    <label for="">Observaciones</label>
                                    <input type="text" name="observaciones" class="form-control wrapper-upper" placeholder="Observaciones">
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-success"><i class="fa fa-hdd" aria-hidden="true"></i> GRABAR</button>
                                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> CERRAR</button>
                                </div>                                                                                                
                            </div>
                        </div>
                    </form>
                </div>
      </div>


    </div>
  </div>
</div>

<script type="text/javascript">
		  /*Funcion de Validar solo numeros*/
    function valida(e){
      tecla = (document.all) ? e.keyCode : e.which;

      if (tecla==8){
          return true;
      }
      patron =/[0-9]/;
      tecla_final = String.fromCharCode(tecla);
      return patron.test(tecla_final);
  	}
</script>


<!-- Modal -->
<div class="modal fade" id="Modal" tabindex="-1" role="dialog" aria-labelledby="Modal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="Modal"><i class="fa fa-user-plus" aria-hidden="true"></i>  Modificaci&oacute;n de Contactos</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body cuerpo">
        
          <div role="tabpanel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#uploadTab1" aria-controls="uploadTab" role="tab" data-toggle="tab">
                        <i class="fa fa-plus-circle" aria-hidden="true"></i> Datos Generales</a>

                        </li>
                        <li role="presentation"><a href="#browseTab1" aria-controls="browseTab" role="tab" data-toggle="tab">
                            <i class="fa fa-plus-circle" aria-hidden="true"></i> Datos Complemento</a>

                        </li>
                    </ul>
                    <!-- Tab panes -->
                    <form action="menu.php?id=19" method="post">
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="uploadTab1">
                            
                                <div class="wrapper-space wrapper-centrar">
                                    <label for="">Id Contacto</label>
                                    <input type="text" name="id_contacto" class="form-control wrapper-upper wrapper-centrar" readonly="">
                                </div>                                
                                <div class="wrapper-space wrapper-centrar">
                                    <label for="">Nombre Empresa</label>
                                    <input type="text" name="nombre_empresa" class="form-control wrapper-upper" placeholder="Nombre Empresa" autofocus="">
                                </div>
                                <div class="wrapper-space wrapper-centrar">
						            <label for="">Cargo Empresa</label>
						            <input type="text" name="cargo_empresa" class="form-control wrapper-upper" placeholder="Cargo Empresa">
                                </div>
                                <div class="wrapper-space wrapper-centrar">
                                    <label for="">Nombres</label>
                                    <input type="text" name="nombres" class="form-control wrapper-upper" placeholder="Ingresar Nombres" >				
                                </div>
                                <div class="wrapper-space wrapper-centrar">
                                    <label for="">Apellidos</label>
                                    <input type="text" name="apellidos" class="form-control wrapper-upper" placeholder="Ingresar Apellidos">
                                </div>
                                <div class="wrapper-space wrapper-centrar">
                                    <label for="">Apellido de Casada</label>
                                    <input type="text" name="apellido_casada" class="form-control wrapper-upper" placeholder="Apellido Casada">
                                </div>
                                <div class="wrapper-space wrapper-centrar">
                                    <label for="">Cui</label>
                                    <input type="text" name="cui" class="form-control centrar wrapper-upper" placeholder="Ingresar Cui" onkeypress="return valida(event)" maxlength="13" minlength="13" required="">
                                </div>
                                <div class="wrapper-space wrapper-centrar">
                                    <label>Nit</label>
                                    <input type="text" name="nit" class="form-control wrapper-upper" placeholder="Nit">
                                </div>
                                <div class="wrapper-space wrapper-centrar">
                                    <label for="">Pasaporte</label>
                                    <input type="text" name="pasaporte" class="form-control centrar wrapper-upper" placeholder="Ingresar Pasaporte">
                                </div>
                                <div class="wrapper-space wrapper-centrar">
                                    <label for="">Tel&eacute;fono</label>
                                    <input type="text" name="telefono" class="form-control centrar wrapper-upper" placeholder="Ingresar Telefono" onkeypress="return valida(event)" maxlength="8" minlength="8" required="">
                                </div>
                                <div class="wrapper-space wrapper-centrar">
                                    <label>Direcci&oacute;n</label>
                                    <input type="text" name="direccion" class="form-control wrapper-upper" placeholder="Direcci&oacute;n" required="">
                                </div>
                                <div class="wrapper-space wrapper-centrar">
                                    <label for="">ZONA</label>
                                    <select name="zona" class="form-control" required="">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                        <option value="13">13</option>
                                        <option value="14">14</option>
                                        <option value="15">15</option>
                                        <option value="16">16</option>
                                        <option value="17">17</option>
                                        <option value="18">18</option>
                                        <option value="19">19</option>
                                        <option value="21">21</option>
                                        <option value="24">24</option>
                                        <option value="25">25</option>
                                    </select>					
                                </div>
                                <div class="wrapper-space wrapper-centrar"> 
                                    <label for="">SEXO</label>
                                    <select name="sexo" class="form-control">
                                        <option value="M">MASCULINO</option>
                                        <option value="F">FEMENINO</option>
                                </select>
                                </div>
                                <div class="wrapper-space wrapper-centrar">
                                    <label for="">CORREO ELECTRONICO</label>
					                <input type="text" name="email" class="form-control" placeholder="Ingresar Correo Electronico" style="text-transform: lowercase;">	
                                </div>                                                                

                            </div>

                            <div role="tabpanel" class="tab-pane" id="browseTab1">
                                    <div class="wrapper-space wrapper-centrar">
                                        <label for="">Nombre Referencia</label>
                                        <input type="text" name="nombre_ref" class="form-control wrapper-upper" placeholder="Nombre Referencia">
                                    </div>
                                    <div class="wrapper-space wrapper-centrar">
                                        <label for="">Tel&eacute;fono Referencia</label>
                                        <input type="text" name="telefono_ref" class="form-control" placeholder="Tel&eacute;fono Referencia">
                                    </div>
                                    <div class="wrapper-space wrapper-centrar">
                                        <label for="">Celular Referencia</label>
                                        <input type="text" name="celular_ref" class="form-control" placeholder="Celular Referencia">
                                    </div>
                                    <div class="wrapper-space wrapper-centrar">
                                        <label for="">Observaciones</label>
                                        <input type="text" name="observaciones" class="form-control wrapper-upper" placeholder="Observaciones">
                                    </div>                                
                                <div class="wrapper-centrar table-space">
                                    <button type="submit" class="btn btn-success"><i class="fa fa-plus-square" aria-hidden="true"></i> GRABAR</button>
                                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-ban" aria-hidden="true"></i> CERRAR</button>
                                </div>

                            </div>
                        </div>
                    </form>
                </div>
      </div>


    </div>
  </div>
</div>

<script src="assets/js/bootstrap.min.js"></script>

  <script>
	$('#Modal').on('show.bs.modal', function(e)
	{
        var id 			= $(e.relatedTarget).data('id_contacto');
        var nom_empresa = $(e.relatedTarget).data('nom_empresa');
        var cargo_empre = $(e.relatedTarget).data('cargo_empre');
        var nombres     = $(e.relatedTarget).data('nombres');
        var apellidos   = $(e.relatedTarget).data('apellidos');
        var ape_casada  = $(e.relatedTarget).data('ape_casada');
        var cui         = $(e.relatedTarget).data('cui');
        var nit         = $(e.relatedTarget).data('nit');
        var telefono    = $(e.relatedTarget).data('telefono');
        var direccion   = $(e.relatedTarget).data('direccion');
        var zona        = $(e.relatedTarget).data('zona');
        var sexo        = $(e.relatedTarget).data('sexo');
        var email       = $(e.relatedTarget).data('email');
        var referencia  = $(e.relatedTarget).data('nombre_ref');
        var tel_referencia  = $(e.relatedTarget).data('tel_referencia');
        var cel_referencia  = $(e.relatedTarget).data('celular_ref');
        var observaciones   = $(e.relatedTarget).data('observaciones');

 
        $(e.currentTarget).find('input[name="id_contacto"]').val(id);
        $(e.currentTarget).find('input[name="nombre_empresa"]').val(nom_empresa);
        $(e.currentTarget).find('input[name="cargo_empresa"]').val(cargo_empre);
        $(e.currentTarget).find('input[name="nombres"]').val(nombres);
        $(e.currentTarget).find('input[name="apellidos"]').val(apellidos);
        $(e.currentTarget).find('input[name="apellido_casada"]').val(ape_casada);
        $(e.currentTarget).find('input[name="cui"]').val(cui);
        $(e.currentTarget).find('input[name="nit"]').val(nit);
        $(e.currentTarget).find('input[name="telefono"]').val(telefono);
        $(e.currentTarget).find('input[name="direccion"]').val(direccion);
        $(e.currentTarget).find('select[name="zona"]').val(zona);
        $(e.currentTarget).find('select[name="sexo"]').val(sexo);
        $(e.currentTarget).find('input[name="email"]').val(email);
        $(e.currentTarget).find('input[name="nombre_ref"]').val(referencia);
        $(e.currentTarget).find('input[name="telefono_ref"]').val(tel_referencia);
        $(e.currentTarget).find('input[name="celular_ref"]').val(cel_referencia);
        $(e.currentTarget).find('input[name="observaciones"]').val(observaciones);

	});
  </script>
