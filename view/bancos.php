<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="lib_alert/css/sweetalert.css">
<script type="text/javascript" src="lib_alert/js/sweetalert-dev.js"></script>


<script>
	function ErrorAcceso()
		{
		swal({title:"Su usuario no tiene privilegios para esta pantalla..!", type:"error", showConfirmButton:false, text:"COMUNIQUESE CON EL ADMINISTRADOR", timer:'1500'}, 
		function () 
		{
		location.href = "menu.php?id=33"; 
		});
		}
</script>

<?php
require_once 'controller/seguridad.php';
require_once 'controller/controller.php';
require_once 'db/conexion.php';

$sql_permiso = mysqli_query($conn, "SELECT COUNT(*)CUENTA
										FROM tb_acceso_item
										WHERE id_usuario = '".$nombre."'
										  AND ITEM = ".$_REQUEST['id']."");
								
	while($valida = $sql_permiso->fetch_array(MYSQLI_ASSOC)){

		$resultado = $valida['CUENTA'];
	}

	if($resultado == 1){
		
	}else{
		echo "<script>ErrorAcceso();</script>";
    }

?>
<div class="row">
    <div class="col-md-12">
        <div class="wrapper-logo-secondary">
            <img src="img/logo/Law.jpg" alt="Logotipo Firma Law">
        </div>
    </div>
</div>

<div class="wrapper-title">
    <div class="container">
        <div class="row">
            <div class="col-md-4" data-line="mobil">
                <div class="line"></div>
            </div>
            <div class="col-md-4 section-title">
                <h1><i class="fa fa-building" aria-hidden="true"></i> BANCOS</h1>
            </div>
            <div class="col-md-4">
                <div class="line"></div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="wrapper-centrar">
        <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#ModalCrea"><i class="fa fa-plus-circle" aria-hidden="true"></i> Crear Bancos</button>
    </div>
</div>

<div class="container wrapper-space" style="margin-top: 75px;">
    <div class="row">
        <div class="col-md-12">
            <table id="example" class="table table-striped table-bordered dt-responsive nowrap">
                <thead>
                    <tr>
                    <th># NUMERO</th>
                    <th>NOMBRE BANCO</th>
                    <th>FECHA CREACION</th>
                    <!--th>USUARIO GRABO</th-->
                    </tr>
                </thead>   
                <tbody>
                    <?php
                   while ($res = mysqli_fetch_array($banco)){

                        echo "<tr>";
                            echo "<td class='wrapper-centrar'>";
                                echo $res[0];
                            echo "</td>";

                            echo "<td>";
							    echo $res[1];   
                            echo "</td>";
                            
                            echo "<td>";
                                echo $res[2];
                            echo "</td>";

                            /*echo "<td class='wrapper-centrar'>";
                                echo $res[3];
                            echo "</td>";*/
                                
                        echo "</tr>";                          
                   
                    }
                    ?>                  
                </tbody>             
            </table>
        </div>
    </div>
</div>

<!--MODAL TAREA-->
<div class="modal fade" id="ModalCrea" role="dialog">
    <div class="modal-dialog">
    
      	<div class="modal-content">
			<div class="modal-header">
			
			<h4 class="modal-title" id="myModalLabel"> <i class="fa fa-building" aria-hidden="true"></i> Creaci&oacute;n de Bancos</h4>
			</div>
			<div class="modal-body cuerpo">
				<form action="menu.php?id=30" method="post" onSubmit="return validacion();">
						
                    <div class="wrapper-space wrapper-centrar">
                        <label for="">Nombre de Banco</label>
                        <input type="text" name="banco" class="form-control wrapper-upper" placeholder="NOMBRE DE BANCO" required="">
                    </div>
				
                    <div class="modal-footer wrapper-centrar">
                        <button type="submit" class="btn btn-success"><i class="fa fa-plus-square" aria-hidden="true"></i> GRABAR</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-ban" aria-hidden="true"></i> CERRAR</button>
                    </div>			
				</form>
			</div>

      	</div>
      
    </div>
  </div>