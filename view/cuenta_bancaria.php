<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="lib_alert/css/sweetalert.css">
<script type="text/javascript" src="lib_alert/js/sweetalert-dev.js"></script>


<script>
	function ErrorAcceso()
		{
		swal({title:"Su usuario no tiene privilegios para esta pantalla..!", type:"error", showConfirmButton:false, text:"COMUNIQUESE CON EL ADMINISTRADOR", timer:'1500'}, 
		function () 
		{
		location.href = "menu.php?id=33"; 
		});
		}
</script>
<?php
require_once 'controller/seguridad.php';
require_once 'controller/controller.php';
require_once 'db/conexion.php';

$sql_permiso = mysqli_query($conn, "SELECT COUNT(*)CUENTA
										FROM tb_acceso_item
										WHERE id_usuario = '".$nombre."'
										  AND ITEM = ".$_REQUEST['id']."");
								
	while($valida = $sql_permiso->fetch_array(MYSQLI_ASSOC)){

		$resultado = $valida['CUENTA'];
	}

	if($resultado == 1){
		
	}else{
		echo "<script>ErrorAcceso();</script>";
    }

?>
<div class="row">
    <div class="col-md-12">
        <div class="wrapper-logo-secondary">
            <img src="img/logo/Law.jpg" alt="Logotipo Firma Law">
        </div>
    </div>
</div>

<div class="wrapper-title">
    <div class="container">
        <div class="row">
            <div class="col-md-4" data-line="mobil">
                <div class="line"></div>
            </div>
            <div class="col-md-4 section-title">
                <h1><i class="fa fa-building" aria-hidden="true"></i> CUENTAS BANCARIAS</h1>
            </div>
            <div class="col-md-4">
                <div class="line"></div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="wrapper-centrar">
        <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#ModalCrea"><i class="fa fa-plus-circle" aria-hidden="true"></i> Crear Cuenta Bancaria</button>
    </div>
</div>

<div class="container wrapper-space" style="margin-top: 75px;">
    <div class="row">
        <div class="col-md-12">
            <table id="example" class="table table-striped table-bordered dt-responsive nowrap">
                <thead>
                    <tr>
                    <th class="wrapper-centrar">CORRELATIVO</th>
                    <th class="wrapper-centrar">BANCO</th>
                    <th class="wrapper-centrar">NOMBRE DE CUENTA</th>
                    <th class="wrapper-centrar">N&Uacute;MERO DE CUENTA</th>
                    <th class="wrapper-centrar">MONEDA</th>
                    </tr>
                </thead>   
                <tbody>
                    <?php
                   while ($row = mysqli_fetch_array($cuentas)){

                        echo "<tr>";

                            echo "<td class='wrapper-centrar'>";
                                echo $row[0];
                            echo "</td>";

                            echo "<td>";
                                echo $row[1];
                            echo "</td>";
                            
                            echo "<td>";
                                echo $row[2];
                            echo "</td>";

                            echo "<td class='wrapper-centrar'>";
                                echo $row[3];
                            echo "</td>";

                            echo "<td class='wrapper-centrar'>";
                                $moneda = $row[4];

                                if($moneda == 'Q'){
                                    $moneda = 'QUETZALES';
                                }elseif($moneda == '$'){
                                    $moneda = 'DOLARES';
                                }

                                echo $moneda;
                            echo "</td>";                          
                                
                        echo "</tr>";                          
                   
                    }
                    ?>                  
                </tbody>             
            </table>
        </div>
    </div>
</div>

<!--MODAL TAREA-->
<div class="modal fade" id="ModalCrea" role="dialog">
    <div class="modal-dialog">
    
      	<div class="modal-content">
			<div class="modal-header">
			
			<h4 class="modal-title" id="myModalLabel"> <i class="fa fa-building" aria-hidden="true"></i> Creaci&oacute;n de Cuenta Bancarias</h4>
			</div>
			<div class="modal-body cuerpo">
				<form action="menu.php?id=32" method="post">
						
                <div class="wrapper-centrar wrapper-space">
                    <label for="">SELECCIONAR BANCO</label>
                    <select name="banco" class="form-control" autofocus="">
                    <?php
                    while ($row = mysqli_fetch_array($sql_banco))
                    {
                        echo '<option value="' . $row['ID_BANCO']. '">' . $row['BANCO'] . ' </option>' . "\n";
                    }
                    ?>
                    </select>
                </div>

                <div class="wrapper-centrar wrapper-space">
                    <label for="">NOMBRE CUENTA</label>
                    <input type="text" name="nombre" class="form-control upper" placeholder="Nombre Cuenta">
                </div>

                <div class="wrapper-centrar wrapper-space">
                    <label for="">NO. CUENTA</label>
                    <input type="text" name="num_cuenta" class="form-control upper" placeholder="Ingresar Cuenta">
                </div>

                <div class="wrapper-centrar wrapper-space">
                    <label for="">MONEDA</label>
                    <select name="moneda" class="form-control">
                        <option value="Q">QUETZALES</option>
                        <option value="$">DOLARES</option>
                    </select>
                </div>                	

                    <div class="modal-footer wrapper-centrar">
                        <button type="submit" class="btn btn-success"><i class="fa fa-plus-square" aria-hidden="true"></i> GRABAR</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-ban" aria-hidden="true"></i> CERRAR</button>
                    </div>			
				</form>
			</div>

      	</div>
      
    </div>
  </div>