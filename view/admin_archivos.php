<?php
require_once 'controller/seguridad.php';
?>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="lib_alert/css/sweetalert.css">
<script type="text/javascript" src="lib_alert/js/sweetalert-dev.js"></script>


<script>
	function ErrorAcceso()
		{
		swal({title:"Su usuario no tiene privilegios para esta pantalla..!", type:"error", showConfirmButton:false, text:"COMUNIQUESE CON EL ADMINISTRADOR", timer:'1500'}, 
		function () 
		{
		location.href = "menu.php?id=33"; 
		});
		}
</script>

<?php
$nombre = $_SESSION['usuario'];
require_once('db/conexion.php');

$sql_permiso = mysqli_query($conn, "SELECT COUNT(*)CUENTA
										FROM tb_acceso_item
										WHERE id_usuario = '".$nombre."'
										  AND ITEM = ".$_REQUEST['id']."");
								
	while($valida = $sql_permiso->fetch_array(MYSQLI_ASSOC)){

		$resultado = $valida['CUENTA'];
	}

	if($resultado == 1){
		
	}else{
		echo "<script>ErrorAcceso();</script>";
    }
?>
<link rel="stylesheet" type="text/css" href="assets/css/archivos.css">

<div class="row">
    <div class="col-md-12">
        <div class="wrapper-logo-secondary">
            <img src="img/logo/Law.jpg" alt="Logotipo Firma Law">
        </div>
    </div>
</div>

<!--AREA PARA AGREGAR NUEVAS CARPETAS RAIZ PRINCIPALES-->
<div class="wrapper-title">
    <div class="container">
        <div class="row">
            <div class="col-md-4" data-line="mobil">
                <div class="line"></div>
            </div>
            <div class="col-md-4 section-title">
                <h1>AREA DE TRABAJO</h1>
            </div>
            <div class="col-md-4">
                <div class="line"></div>
            </div>
        </div>
    </div>
</div>


	

    <div class="container">
       <form action="menu.php?id=9" method="post">
           <div class="add">
                <a data-toggle='collapse' href='#principal' data-toggle="collapse"><h3 class=''><i class="fa fa-plus-circle "></i> <i class="fa fa-folder " aria-hidden="true"></i></h3></a>
           </div>
           <div class="col-md-12 collapse" style="margin-bottom: 15px;" id="principal">
               <div class="col-md-6">
                   <input type="text" name="nombre" id="nombre" class="form-control upper" autofocus="" required="" placeholder="Nombre de Carpeta Raiz">
               </div>
               <div class="col-md-1" style="margin-top: 0px;">
                    <div class="boton-formulario">
                      <button type="submit" class="btn btn-success"><i class='fa fa-hdd' aria-hidden='true'></i> CREAR CARPETA</button>
                    </div>                   
               </div>
           </div>
       </form> 
    </div>    
<!--FINALIZA CARPETAS PRINCIPALES RAIZ-->
<?php
$nombre = $_SESSION['usuario'];
require_once('db/conexion.php');

$nivel_uno = mysqli_query($conn, "SELECT id_area, descripcion
                                FROM tb_archivos
                                WHERE tb__casos is null");


$casos = mysqli_query($conn, "SELECT COUNT(*)CUENTA
FROM tb_acceso_item
WHERE id_usuario = '".$nombre."'
  AND ITEM = 2");

while($val_caso = $casos->fetch_array(MYSQLI_ASSOC)){

$res_caso1 = $val_caso['CUENTA'];

} 



    $var = 0;
        /*INICA PROCESO NIVEL UNO PRINCIPAL*/
	echo "<div class='wrapper-area' >";
				$folder_name = '';
                while($txt = mysqli_fetch_array($nivel_uno)){
                    $var  ++;

                    echo "<div class=''>";
                        echo "<div class='card contenido'>";
                            echo "<div class='card-body wrapper-folder file-move'><a href='#' data-toggle='collapse' data-target='#$txt[1]$var'><p> <i class='fa fa-folder fa-2x'></i> $txt[1] </p> </a>";
                            if($res_caso1 == 1){
                                echo "<a href='#' class='wrapper-add-file' data-toggle='modal' data-target='#$txt[1]'> <p><i class='fa fa-plus-circle'></i> <i class='fa fa-address-card' aria-hidden='true'></i> Nuevo Caso</p></a>";
                            }
                            
                                echo "</div>";
                                    $nivel_dos = mysqli_query($conn, "SELECT a.id_caso, a.description, c.causa
                                                                        FROM tb_archivos a,
                                                                            tb_acceso b,
                                                                            tb_caso c
                                                                        WHERE a.id_caso = b.ID_CASO
                                                                        AND b.id_caso = c.id_caso
                                                                        AND a.id_caso = c.id_caso
                                                                        AND a.tb__casos = '".$txt[0]."'
                                                                        AND b.ID_USUARIO = '".$nombre."'");
                       
                                        $tmp = 0;
                                        echo "<div id='$txt[1]$var' class='collapse card file-move-dos wrapper-folder-two contenido'>";
                                                while($row = mysqli_fetch_array($nivel_dos)){
													$tmp ++;
                                                    $add_carpeta = $txt[1].$tmp.$txt[0];
                                                   	$add_file    = $tmp.$txt[1].$txt[0];
												   
                                                       echo utf8_encode(" 
                                                                          <div class='card-body sub-group-folder-two' >
                                                                            <a href='#' onclick='myFunction3$var$add_file()' data-toggle='collapse' data-target='#$row[0]$tmp'><p class='text'><i class='fa fa-folder fa-2x'></i>  $row[1] ( CAUSA $row[2] ) </p> </a>
                                                                            <div>
                                                                                <a href='#' onclick='myFunction2$var$add_file()' class='wrapper-add-file' data-toggle='collapse' data-target='#$add_carpeta'><i class='fa fa-plus-circle fa-2x'></i> <i class='fa fa-folder fa-2x' aria-hidden='true'></i></a> 
                                                                                <a href='#' onclick='myFunction$var$add_file()' class='wrapper-add-doc' data-toggle='collapse' data-target='#$add_file'><i class='fa fa-plus-circle fa-2x'></i> <i class='fa fa-file fa-2x' aria-hidden='true'></i> </a>
                                                                            </div>
                                                                          </div>
																		  ");
				  
																		  echo "<script>
																		  function myFunction$var$add_file() {
																				 var element = document.getElementById('$add_carpeta');
																				 var file = document.getElementById('$row[0]$tmp');
																				 element.classList.remove('in');
																				 file.classList.remove('in');
																				 
																		  }
																		  function myFunction2$var$add_file() {
																			var element = document.getElementById('$add_file');
																			var file = document.getElementById('$row[0]$tmp');
																			element.classList.remove('in');
																			file.classList.remove('in');
																			 }

																			 function myFunction3$var$add_file() {
																				var element = document.getElementById('$add_file');
																				var element2 = document.getElementById('$add_carpeta');

																				element.classList.remove('in');
																				element2.classList.remove('in');
																				 }
																			
																		  </script>";
                                                        
                                                    /**** CREACION DE CARPETA EN NIVEL DEL CASO ******/ 
                                                    $detalle_caso = utf8_encode($row[1]);    
                                                        echo "<div id='$add_carpeta' class='collapse' style='margin-top: 15px;'>";
                                                            echo "<form action='menu.php?id=10&area=$txt[0]&caso=$row[0]&raiz=$txt[1]&des_caso=$detalle_caso&tipo=C' method='post'>";
                                                                echo "<div class='col-md-5'>";
                                                                    echo "<input type='text' name='carpeta' class='form-control upper' placeholder='Nombre Carpeta'>";
                                                                echo "</div>";
                                                                echo "<div class='col-md-1'>";
                                                                    echo "<div class='boton-formulario'>";
                                                                        echo "<button type='submit' class='btn btn-success'><i class='fa fa-hdd' aria-hidden='true'></i> GRABAR</button>";
                                                                    echo "</div>";
                                                                echo "</div>";
                                                            echo "</form>";
														echo "</div>";
														

                                                    /*********** FINAL CARPETA AL CASO **************/

                                                    /**** CREACION DE DOCUMENTO EN NIVEL DEL CASO *******/ 
                                                       
                                                        echo "<div id='$add_file' class='collapse' style='margin-top: 15px;'>";
                                                            echo "<form action='menu.php?id=10&area=$txt[0]&caso=$row[0]&raiz=$txt[1]&des_caso=$detalle_caso&tipo=D' method='post' enctype='multipart/form-data'>";
                                                                echo "<div class='col-md-5'>";
                                                                    echo "<input type='file' name='file' class='form-control'>";
                                                                echo "</div>";
                                                                echo "<div class='col-md-1'>";
                                                                    echo "<div class='boton-formulario'>";
                                                                        echo "<button type='submit' class='btn btn-success'><i class='fa fa-hdd' aria-hidden='true'></i> GRABAR</button>";
                                                                    echo "</div>";
                                                                echo "</div>";
                                                            echo "</form>";
                                                        echo "</div>";
                                                    /*********** FINAL CARPETA AL CASO **************/                                                    

                                                        $carpeta = mysqli_query($conn, "SELECT id_carpeta, nombre_carpeta
                                                                                        FROM tb_carpetas
                                                                                        WHERE id_area = '".$txt[0]."'
                                                                                          AND id_caso = '".$row[0]."'");


                                                        $archivos = mysqli_query($conn, "SELECT b.ruta, b.descripcion 
                                                                                            FROM tb_archivos a,
                                                                                                tb_documento b
                                                                                            WHERE a.id_area = b.id_area
                                                                                            AND b.id_area = '".$txt[0]."'
                                                                                            AND b.tb__casos = '".$txt[0]."'
																							AND b.id_caso = '".$row[0]."'
																							order by descripcion desc");

                                                            echo "<div id='$row[0]$tmp' class='collapse wrapper-download'>";
                                                                $file = 0;
                                                                while($carpetas = mysqli_fetch_array($carpeta)){
                                                                    $file ++;
                                                                    echo "<div class='wrapper-carpetas'>";
                                                                        echo utf8_encode("<div><a href='#' data-toggle='collapse' data-target='#$carpetas[1]$file'><p class='text'><i class='fa fa-folder '></i>  $carpetas[1]</p></a>
                                                                        <a href='#'  class='wrapper-file' data-toggle='collapse' data-target='#$file$carpetas[1]'> <i class='fa fa-plus-circle'></i> <i class='fa fa-file fa-2x' aria-hidden='true'></i></a></div>");
                                                                    echo "</div>";

                                                                /**** CREACION DE DOCUMENTO EN NIVEL DEL CASO *******/ 
                                                                    
                                                                echo "<div id='$file$carpetas[1]' class='collapse wrapper-document-three'>";
                                                                    echo "<form class='wrapper-form wrapp-form-three' action='menu.php?id=10&area=$txt[0]&caso=$row[0]&raiz=$txt[1]&des_caso=$detalle_caso&tipo=V&carpeta=$carpetas[1]&num=$carpetas[0]' method='post' enctype='multipart/form-data'>";
                                                                        echo "<div class='col-md-5'>";
                                                                            echo "<input type='file' name='file_dos' class='form-control'>";
                                                                        echo "</div>";
                                                                        echo "<div class='col-md-1'>";
                                                                            echo "<div class='boton-formulario'>";
                                                                                echo "<button type='submit' class='btn btn-success'><i class='fa fa-hdd' aria-hidden='true'></i> GRABAR</button>";
                                                                            echo "</div>";
                                                                        echo "</div>";
                                                                    echo "</form>";
                                                                echo "</div>";
                                                            /*********** FINAL CARPETA AL CASO **************/ 


                                                                        $doc_carpeta = mysqli_query($conn, "SELECT ruta, descripcion
                                                                                                                FROM tb_documento
                                                                                                                WHERE id_area   = '".$txt[0]."'
                                                                                                                AND id_caso     = '".$row[0]."'
                                                                                                                AND id_carpeta  = '".$carpetas[0]."'");
                                                                    echo "<div id='$carpetas[1]$file' class='collapse wrapper-download'>";
                                                                        while($document_file = mysqli_fetch_array($doc_carpeta)){
																				echo "<div class='wrapper-end'>";
                                                                                	echo utf8_encode("<a href='$document_file[0]' target='_blank'><p class='text'><i class='fa fa-download'></i>  $document_file[1] </p></a><br>"); 
																				echo "</div>";
                                                                        }
                                                                        echo "</div>";

                                                                }

                                                                while($res = mysqli_fetch_array($archivos)){
                                                                    echo "<div class='wrapper-file-three'>";
																	echo utf8_encode("<a href='$res[0]' target='_blank'><p class='text'><i class='fa fa-download '></i>  $res[1]</p></a><br>"); 
																	echo "</div>";

                                                                } 
                                                            echo "</div>";                                   

                                                }
                                        echo "</div>";
                        echo "</div>";
                    echo "</div>";            


echo "  <div class='modal fade' id='$txt[1]' role='dialog'>
		<div class='modal-dialog'>

		<!-- Modal content-->
		<div class='modal-content'>
				<div class='modal-header'>
					<!--button type='button' class='close' data-dismiss='modal'>&times;</button-->
					<h4 class='modal-title'><i class='fas fa-th-large'></i> INGRESO DE CASOS DE $txt[1]</h4>
				</div>
				<div class='modal-body'>
					<form action='menu.php?id=11' method='post' enctype='multipart/form-data' onsubmit='return validar();'>
					<ul class='nav nav-tabs'>
						<li class='active'><a data-toggle='tab' href='#home$var'><i class='fa fa-plus-circle' aria-hidden='true'></i> GENERALES CASO</a></li>
						<li><a data-toggle='tab' href='#menu1$var'><i class='fa fa-plus-circle' aria-hidden='true'></i> DATOS JUZGADOS</a></li>
					</ul>
						<div class='tab-content'>
						<div id='home$var' class='tab-pane fade in active'>";
						
						echo "<div class='wrapper-space wrapper-centrar'>
						<label>CONTACTO</label>
							<select name='id_contacto' class='form-control' autofocus=''>
								<option value=''>SELECCIONAR CONTACTO</option>";
					
	
				$contacto = mysqli_query($conn,"SELECT ID_CONTACTO, TRIM(CONCAT_WS(' ', NOMBRES,' ',APELLIDOS,' ',NOMBRE_EMPRESA))NOMBRES
							FROM tb_contacto");
	
	
					while ($row = mysqli_fetch_array($contacto))
					{
						echo '<option value=" '. $row['ID_CONTACTO'].' ">' . $row['ID_CONTACTO'].'-'. $row['NOMBRES'] . '</option>' . "\n";
					}
					
				echo	"</select>
				</div>";

				echo "<div class='wrapper-space wrapper-centrar'>
				<label for=''>RUTA DEL CASO</label>
				<select name='ruta' id='ruta' class='form-control'>";
					
					
					$ruta = mysqli_query($conn, "SELECT id_area, descripcion
													FROM tb_archivos
													WHERE tb__casos is null
													  AND id_area = '".$txt[0]."'");
	
						while($ftp = mysqli_fetch_array($ruta)){
							echo "<option value='$ftp[0]'>$ftp[1]</option>";
						}														
					
					
				echo "</select>
	
				</div>";
				
				echo "<div class='wrapper-space wrapper-centrar'>
				<label>CASO ORIGEN</label>
				<select name='origen' class='form-control'>
					<option value='0'>SIN CASO ORIGEN</option>";
					
						$sdl = mysqli_query($conn, "SELECT a.ID_CASO, a.CAUSA
													FROM tb_caso a,
														 tb_acceso b
													WHERE a.id_caso = b.id_caso
													  AND b.id_usuario = '".$nombre."'");
						while($res = mysqli_fetch_array($sdl)){
							echo '<option value="' . $res['ID_CASO']. '">' . $res['CAUSA'] . '</option>' . "\n";
						}							
					
			echo "</select>
			</div>";
			
			echo "<div class='wrapper-space wrapper-centrar'>
					<label>DESCRIPCION</label>
					<input type='text' name='descripcion' class='form-control upper' placeholder='DESCRIPCI&Oacute;N DEL CASO' required=''>
				</div>
				<div class='wrapper-space wrapper-centrar'>
					<label>FECHA INICIO</label>
					<input type='text' name='fec_inicio' class='form-control centrar' id='fecha_inicio_$var' placeholder='FECHA INICIO' required=''>
				</div>
				<div class='wrapper-space wrapper-centrar'>
					<label>FECHA FINAL</label>
					<input type='text' name='fec_final' class='form-control centrar' id='fecha_final_$var' placeholder='FECHA FINAL'>
				</div>
				<div class='wrapper-space wrapper-centrar'>
					<label>OBSERVACIONES</label>
					<input type='text' name='text' class='form-control upper' placeholder='OBSERVACIONES'>
				</div>

				<div class='wrapper-space wrapper-centrar'>
					<label for=''>ADJUNTAR ARCHIVO</label>
					<input type='file' name='file' class='form-control'>
				</div>
				<div class='wrapper-space wrapper-centrar'>
				<label>ABOGADO RESPONSABLE</label>
				<input type='text' name='abogado' class='form-control upper' placeholder='Abogado Responsable'>
			</div>				
				</div>";			
				

						
				echo "<div id='menu1$var' class='tab-pane fade'>
				<div class='wrapper-space wrapper-centrar'>
				<label>N&Uacute;MERO EXPEDIENTE</label>
				<input type='text' name='causa' class='form-control upper' placeholder='N&uacute;mero Expediente' id='causa'>
			</div>
			<div class='wrapper-space wrapper-centrar'>
				<label>N&Uacute;MERO MP</label>
				<input type='text' name='mun_mp' class='form-control upper' placeholder='N&uacute;mero MP'>
			</div>
			<div class='wrapper-space wrapper-centrar'>
				<label>FISCALIA</label>
				<input type='text' name='fiscalia' class='form-control upper' placeholder='Fiscalia'>
			</div>
			<div class='wrapper-space wrapper-centrar'>
				<label>JUZGADO</label>
				<input type='text' name='juzgado' class='form-control upper' placeholder='Juzgado'>
			</div>
			<div class='wrapper-space wrapper-centrar'>
				<label>JUEZ</label>
				<input type='text' name='juez' class='form-control upper' placeholder='Juez'>
			</div>
			<div class='wrapper-space wrapper-centrar'>
				<label>OFICIAL</label>
				<input type='text' name='oficial' class='form-control upper' placeholder='oficial'>
			</div>	
			<div class='wrapper-space wrapper-centrar'>
				<label>DIRECCI&Oacute;N JUZGADO</label>
				<input type='text' name='direccion_juzgado' class='form-control upper' placeholder='direcci&oacute;n juzgado'>
			</div>";	

			echo "<div class='wrapper-space wrapper-centrar'>
				<label>TELEFONO JUZGADO</label>
				<input type='text' name='tel_juzgado' class='form-control upper' placeholder='telefono juzgado'>
			</div>															

			<div class='wrapper-space wrapper-centrar'>
				<button type='submit' class='btn btn-success'><i class='fa fa-hdd' aria-hidden='true'></i> GRABAR</button>
				<button type='button' class='btn btn-danger' data-dismiss='modal'><i class='fa fa-times' aria-hidden='true'></i> CERRAR</button>
			</div>

						
				      </div>
						</div>
					</form>
				</div>
		</div>
		
		</div>
		</div>";


        echo  "<script>
                  $('#fecha_inicio_$var').datetimepicker({ footer: true, modal: true, format: 'dd/mm/yyyy' });
                  $('#fecha_final_$var').datetimepicker({ footer: true, modal: true, format: 'dd/mm/yyyy' });
               </script>";

				}
				
    echo "</div>";                                

?>