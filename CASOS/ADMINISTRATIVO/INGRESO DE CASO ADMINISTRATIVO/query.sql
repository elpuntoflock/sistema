SELECT plpoin_id
,plpoin_descripcion
,plcggg_id
,plcggg_descripcion
,plpoin_precio
,plpoin_cantidad
,plpoin_cantidad - ifnull(plpoin_cant_costo,0)
$todos	
FROM pltbpo_insumos
	,pltbcg_grupogasto
WHERE plpoin_anyo       = 2017
AND plpoin_dependencia  = '100'
AND plpoin_oestrategico = 34
AND plpoin_ooperativo   = 1
AND plpoin_meta         = 1
AND plpoin_umedida      = 2303
AND plpoin_accion       = 1
AND plpoin_tarea        = 1
AND plpoin_grupogasto   = plcggg_id


/*Trae el Objetivo Estrategico Encabezado*/
SELECT plcgoe_id
		,plcgoe_descripcion
FROM pltbpo_oestrategicos  
		,pltbcg_oestrategicos
WHERE plpooe_anyo         = 2017
 AND plpooe_dependencia  = 100
 AND plpooe_oestrategico = plcgoe_id
 AND plpooe_restrictiva  = 'N'
 AND plcgoe_restrictiva  = 'N'
ORDER BY plcgoe_id



SELECT plpotr_id
	,plpotr_descripcion, plpotr_umedidaref, plpotr_cantidad
FROM pltbpo_tareas
WHERE plpotr_restrictiva  = 'N'
AND plpotr_anyo           = 2017
AND plpotr_dependencia    = 100
AND plpotr_oestrategico   = 34
AND plpotr_ooperativo     = 1
AND plpotr_meta           = 1
AND plpotr_umedida        = 2303
AND plpotr_accion         = 1
ORDER BY plpotr_id



select * from pltbpo_tareas
WHERE plpotr_restrictiva  = 'N'
AND plpotr_anyo           = 2017
AND plpotr_dependencia    = 100
AND plpotr_oestrategico   = 34
AND plpotr_ooperativo     = 1
AND plpotr_meta           = 1
AND plpotr_umedida        = 2303
AND plpotr_accion         = 1
ORDER BY plpotr_id



