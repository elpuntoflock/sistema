<?php
require_once 'db/conexion.php';
require_once 'fpdf/fpdf.php';
require_once 'num.letras.php';

$Ncheque     = $_REQUEST['ftc'];

$sql = mysqli_query($conn, "SELECT  DATE_FORMAT(a.FECHA_CHEQUE,'%d-%m-%Y')FECHA, a.MONTO_CHEQUE, a.NUM_CHEQUE, a.REFERENCIA, a.PROVEEDOR, a.MOTIVO, b.NUM_CUENTA, 
                                    a.BENEFICIARIO, a.NEGOCIABLE, c.BANCO
                            FROM tb_cheque a,
                            tb_cuenta_banco b,
                            tb_banco c
                            WHERE a.IDCUENTA = b.IDCUENTA
                            AND b.ID_BANCO = c.ID_BANCO
                            AND a.ID_CHEQUE = '".$Ncheque."'");


/*
$globalx = 0;
$globaly = 2;

$chequexy = array (
			"fecha" => 		array ($globalx + 25, $globaly + 21),
			"monto" => 		array ($globalx + 132, $globaly + 21),
			"letras" =>		array ($globalx + 20, $globaly + 33),
			"referencia" =>	array ($globalx + 37, $globaly + 50),
			"beneficiario"=>array ($globalx + 25, $globaly + 27),
			"nonegociable"=>array ($globalx + 19, $globaly + 55)
        );

$pdf = new FPDF();

$pdf->AddPage();
​
$pdf->SetFont('Arial','',11);
$pdf->setxy(1,1);
$pdf->Cell(170,60,' ',0,1,l);
$pdf->setxy($chequexy["fecha"][0],$chequexy["fecha"][1]);
$pdf->Cell(95,5, 'Guatemala,' . $fecha,0,1,1);
$pdf->setxy($chequexy["monto"][0],$chequexy["monto"][1]);
$pdf->Cell(35,5, $monto,0,1,l);
$pdf->setxy($chequexy["letras"][0],$chequexy["letras"][1]);
$pdf->Cell(138,5, $letras,0,1,l);
$pdf->SetFont('Arial','',9);
$pdf->setxy($chequexy["referencia"][0],$chequexy["referencia"][1]);
$pdf->Cell(35,3, 'Ref. ' . $referencia,0,1,l);
$pdf->SetFont('Arial','B',12);
$pdf->setxy($chequexy["beneficiario"][0],$chequexy["beneficiario"][1]);
$pdf->Cell(105,5, $beneficiario,0,1,l);
$pdf->SetFont('Arial','B',12);
$pdf->SetTextColor(0,144,255);
$pdf->setxy($chequexy["nonegociable"][0],$chequexy["nonegociable"][1]);
$pdf->Cell(59,3, $nonegociable,0,1,C);
​
//Voucher
$pdf->AddPage();
$pdf->Image('http://seprovet.esy.es/seprovet/img/logo-active.png',8,5,20,0);
$pdf->setdrawcolor(200);
$pdf->setxy(40,20);
$pdf->SetFont('Arial','',10);
$pdf->SetTextColor(0,0,200);
$pdf->Cell(15,5, 'Fecha',1,0,L);
$pdf->SetFont('Arial','B',11);
$pdf->SetTextColor(0);
$pdf->Cell(40,5, $fecha,1,0,C);
$pdf->SetFont('Arial','',10);
$pdf->SetTextColor(0,0,200);
$pdf->Cell(20,5, 'No.Cheque',1,0,L);
$pdf->SetFont('Arial','B',11);
$pdf->SetTextColor(0);
$pdf->Cell(40,5,$cheque,1,1,R);
$pdf->setx(10);
$pdf->SetFont('Arial','',10);
$pdf->SetTextColor(0,0,200);
$pdf->Cell(25,5, 'Beneficiario',1,0,L);
$pdf->SetFont('Arial','B',11);
$pdf->SetTextColor(0);
$pdf->Cell(125,5, $beneficiario,1,1,L);
$pdf->setx(10);
$pdf->SetFont('Arial','',10);
$pdf->SetTextColor(0,0,200);
$pdf->Cell(25,5, 'Referencia',1,0,L);
$pdf->SetFont('Arial','B',11);
$pdf->SetTextColor(0);
$pdf->Cell(50,5, $referencia,1,0,L);
$pdf->SetFont('Arial','',10);
$pdf->SetTextColor(0,0,200);
$pdf->Cell(25,5, 'Monto',1,0,L);
$pdf->SetFont('Arial','B',11);
$pdf->SetTextColor(0);
$pdf->Cell(50,5, $monto,1,1,R);
$pdf->setx(10);
$pdf->SetFont('Arial','',10);
$pdf->SetTextColor(0,0,200);
$pdf->Cell(25,5, 'Letras',1,0,L);
$pdf->SetFont('Arial','B',11);
$pdf->SetTextColor(0);
$pdf->Cell(125,5, $letras,1,1,L);
$pdf->setx(10);
$pdf->SetFont('Arial','',10);
$pdf->SetTextColor(0,0,200);
$pdf->Cell(25,5, 'Banco',1,0,L);
$pdf->SetFont('Arial','B',11);
$pdf->SetTextColor(0);
$pdf->Cell(50,5, 'BAC Credomatic',1,0,L);
$pdf->SetFont('Arial','',10);
$pdf->SetTextColor(0,0,200);
$pdf->Cell(25,5, 'No. Cuenta',1,0,L);
$pdf->SetFont('Arial','B',11);
$pdf->SetTextColor(0);
$pdf->Cell(50,5, $cuenta,1,1,R);
$pdf->setx(10);
$pdf->SetFont('Arial','',10);
$pdf->SetTextColor(0,0,200);
$pdf->Cell(25,15, 'Descripción',1,0,L);
$pdf->SetFont('Arial','B',11);
$pdf->SetTextColor(0);
$pdf->Cell(125,15, $motivo,1,1,L);
$pdf->setx(10);
$pdf->Cell(50,15, ' ',0,1,L);
$pdf->setx(10);
$pdf->SetFont('Arial','',9);
$pdf->SetTextColor(0,0,200);
$pdf->Cell(5,5, ' ',0,0,L);
$pdf->Cell(40,5, 'Fecha recibido',T,0,C);
$pdf->Cell(5,5, ' ',0,0,L);
$pdf->Cell(100,5, 'Nombre quién recibe',T,1,C);
$pdf->Cell(5,5, ' ',0,0,L);
$pdf->Cell(45,10, ' ',0,0,L);
$pdf->Cell(100,10, ' ',0,1,L);
$pdf->Cell(5,5, ' ',0,0,L);
$pdf->Cell(45,5, ' ',0,0,L);
$pdf->Cell(100,5, 'DPI (Identificación)',T,1,C);
$pdf->Cell(5,5, ' ',0,0,L);
$pdf->Cell(45,10, ' ',0,0,L);
$pdf->Cell(100,10, ' ',0,1,L);
$pdf->Cell(5,5, ' ',0,0,L);
$pdf->Cell(45,5, ' ',0,0,L);
$pdf->Cell(100,5, 'Firma',T,1,C);
​
$pdf->Output();
*/
while($row = $sql->fetch_array(MYSQLI_ASSOC)){

    $fecha          = $row['FECHA'];
    $monto          = $row['MONTO_CHEQUE'];
    $cheque         = $row['NUM_CHEQUE'];
    $letras         = numtoletras($monto);
    $referencia     = $row['REFERENCIA'];
    $beneficiario   = iconv('UTF-8', 'windows-1252', $row['BENEFICIARIO']);
    $nonegociable   = $row['NEGOCIABLE'];
    $motivo         = iconv('UTF-8', 'windows-1252', $row['MOTIVO']);
    $motivo         = $row['MOTIVO'];
    $cuenta         = $row['NUM_CUENTA'];
    $banco          = strtoupper($row['BANCO']);

    if($nonegociable == 'S'){
        $nonegociable = '';
    }elseif($nonegociable == 'N'){
        $nonegociable = '** NO NEGOCIABLE **';
    }


} 

$globalx = 0;
$globaly = 2;

$chequexy = array (
			"fecha" => 		array ($globalx + 25, $globaly + 21),
			"monto" => 		array ($globalx + 132, $globaly + 21),
			"letras" =>		array ($globalx + 20, $globaly + 33),
			"referencia" =>	array ($globalx + 37, $globaly + 50),
			"beneficiario"=>array ($globalx + 25, $globaly + 27),
			"nonegociable"=>array ($globalx + 19, $globaly + 55)
        );

$pdf = new FPDF();
$pdf->AddPage();
$pdf->SetFont('Arial','',11);
$pdf->setxy(1,1);
$pdf->Cell(170,60,' ',0,1,'l');
$pdf->setxy($chequexy["fecha"][0],$chequexy["fecha"][1]);
$pdf->Cell(95,5, 'Guatemala, ' . $fecha,0,1,1);
$pdf->setxy($chequexy["monto"][0],$chequexy["monto"][1]);
$pdf->Cell(35,5, $monto,0,1,'l');
$pdf->setxy($chequexy["letras"][0],$chequexy["letras"][1]);
$pdf->Cell(138,5, $letras,0,1,'l');
$pdf->SetFont('Arial','',9);
$pdf->setxy($chequexy["referencia"][0],$chequexy["referencia"][1]);
$pdf->Cell(35,3, 'Ref. ' . $referencia,0,1,'l');
$pdf->SetFont('Arial','B',12);
$pdf->setxy($chequexy["beneficiario"][0],$chequexy["beneficiario"][1]);
$pdf->Cell(105,5, $beneficiario,0,1,'l');
$pdf->SetFont('Arial','B',12);
$pdf->SetTextColor(0,144,255);
$pdf->setxy($chequexy["nonegociable"][0],$chequexy["nonegociable"][1]);
$pdf->Cell(59,3, $nonegociable,0,1,'C');

//Voucher
$pdf->AddPage();
$pdf->Image('img/logo/Law.jpg',8,5,20,0);
$pdf->setdrawcolor(200);
$pdf->setxy(40,20);
$pdf->SetFont('Arial','',10);
$pdf->SetTextColor(0,0,200);
$pdf->Cell(15,5, 'Fecha',1,0,'L');
$pdf->SetFont('Arial','B',11);
$pdf->SetTextColor(0);
$pdf->Cell(40,5, $fecha,1,0,'C');
$pdf->SetFont('Arial','',10);
$pdf->SetTextColor(0,0,200);
$pdf->Cell(20,5, 'No.Cheque',1,0,'L');
$pdf->SetFont('Arial','B',11);
$pdf->SetTextColor(0);
$pdf->Cell(40,5,$cheque,1,1,'R');
$pdf->setx(10);
$pdf->SetFont('Arial','',10);
$pdf->SetTextColor(0,0,200);
$pdf->Cell(25,5, 'Beneficiario',1,0,'L');
$pdf->SetFont('Arial','B',11);
$pdf->SetTextColor(0);
$pdf->Cell(125,5, $beneficiario,1,1,'L');
$pdf->setx(10);
$pdf->SetFont('Arial','',10);
$pdf->SetTextColor(0,0,200);
$pdf->Cell(25,5, 'Referencia',1,0,'L');
$pdf->SetFont('Arial','B',11);
$pdf->SetTextColor(0);
$pdf->Cell(60,5, $referencia,1,0,'L');
$pdf->SetFont('Arial','',10);
$pdf->SetTextColor(0,0,200);
$pdf->Cell(25,5, 'Monto',1,0,'L');
$pdf->SetFont('Arial','B',11);
$pdf->SetTextColor(0);
$pdf->Cell(40,5, $monto,1,1,'C');
$pdf->setx(10);
$pdf->SetFont('Arial','',10);
$pdf->SetTextColor(0,0,200);
$pdf->Cell(25,5, 'Letras',1,0,'L');
$pdf->SetFont('Arial','B',11);
$pdf->SetTextColor(0);
$pdf->Cell(125,5, $letras,1,1,'L');
$pdf->setx(10);
$pdf->SetFont('Arial','',10);
$pdf->SetTextColor(0,0,200);
$pdf->Cell(25,5, 'Banco',1,0,'L');
$pdf->SetFont('Arial','B',11);
$pdf->SetTextColor(0);
$pdf->Cell(50,5, $banco,1,0,'L');
$pdf->SetFont('Arial','',10);
$pdf->SetTextColor(0,0,200);
$pdf->Cell(25,5, 'No. Cuenta',1,0,'L');
$pdf->SetFont('Arial','B',11);
$pdf->SetTextColor(0);
$pdf->Cell(50,5, $cuenta,1,1,'R');
$pdf->setx(10);
$pdf->SetFont('Arial','',10);
$pdf->SetTextColor(0,0,200);
$pdf->Cell(25,15, 'Descripcion',1,0,'L');
$pdf->SetFont('Arial','B',11);
$pdf->SetTextColor(0);
$pdf->Cell(125,15, $motivo,1,1,'L');
$pdf->setx(10);
$pdf->Cell(50,15, ' ',0,1,'L');
$pdf->setx(10);
$pdf->SetFont('Arial','',9);
$pdf->SetTextColor(0,0,200);
$pdf->Cell(5,5, ' ',0,0,'L');
$pdf->Cell(40,5, 'Fecha recibido','T',0,'C');
$pdf->Cell(5,5, ' ',0,0,'L');
$pdf->Cell(100,5, 'Nombre quien recibe','T',1,'C');
$pdf->Cell(5,5, ' ',0,0,'L');
$pdf->Cell(45,10, ' ',0,0,'L');
$pdf->Cell(100,10, ' ',0,1,'L');
$pdf->Cell(5,5, ' ',0,0,'L');
$pdf->Cell(45,5, ' ',0,0,'L');
$pdf->Cell(100,5, 'DPI (Identificacion)','T',1,'C');
$pdf->Cell(5,5, ' ',0,0,'L');
$pdf->Cell(45,10, ' ',0,0,'L');
$pdf->Cell(100,10, ' ',0,1,'L');
$pdf->Cell(5,5, ' ',0,0,'L');
$pdf->Cell(45,5, ' ',0,0,'L');
$pdf->Cell(100,5, 'Firma','T',1,'C');

$pdf->Output();
?>