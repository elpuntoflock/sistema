<?php
require('fpdf.php');


$fecha = $_GET['fecha'];
$monto = $_GET['monto'];
$cheque = $_GET['cheque'];
$letras = $_GET['letras'];
$referencia =$_GET['ref'];
$beneficiario = iconv('UTF-8', 'windows-1252', $_GET['para']);
$nonegociable =$_GET['nonegociable'];
$motivo  = iconv('UTF-8', 'windows-1252', $_GET['motivo']);
$motivo  = $_GET['motivo'];
$cuenta  = $_GET['cuenta'];

$pdf = new FPDF();


$pdf->AddPage();

$pdf->SetFont('Arial','',11);
$pdf->setxy(1,1);
$pdf->Cell(170,60,' ',0,1,l);
$pdf->setxy(26,21);
$pdf->Cell(95,5, 'Guatemala,' . $fecha,0,1,1);
$pdf->setxy(129,21);
$pdf->Cell(35,5, $monto,0,1,l);
$pdf->setxy(20,33);
$pdf->Cell(138,5, $letras,0,1,l);
$pdf->SetFont('Arial','',9);
$pdf->setxy(35,48);
$pdf->Cell(35,3, 'Ref. ' . $referencia,0,1,l);
$pdf->SetFont('Arial','B',12);
$pdf->setxy(25,27);
$pdf->Cell(105,5, $beneficiario,0,1,l);
$pdf->SetFont('Arial','B',12);
$pdf->SetTextColor(0,144,255);
$pdf->setxy(19,55);
$pdf->Cell(59,3, $nonegociable,0,1,C);

//Voucher
$pdf->AddPage();
$pdf->Image('http://seprovet.esy.es/seprovet/img/logo-active.png',8,5,20,0);
$pdf->setdrawcolor(200);
$pdf->setxy(40,20);
$pdf->SetFont('Arial','',10);
$pdf->SetTextColor(0,0,200);
$pdf->Cell(15,5, 'Fecha',1,0,L);
$pdf->SetFont('Arial','B',11);
$pdf->SetTextColor(0);
$pdf->Cell(40,5, $fecha,1,0,C);
$pdf->SetFont('Arial','',10);
$pdf->SetTextColor(0,0,200);
$pdf->Cell(20,5, 'No.Cheque',1,0,L);
$pdf->SetFont('Arial','B',11);
$pdf->SetTextColor(0);
$pdf->Cell(40,5,$cheque,1,1,R);
$pdf->setx(10);
$pdf->SetFont('Arial','',10);
$pdf->SetTextColor(0,0,200);
$pdf->Cell(25,5, 'Beneficiario',1,0,L);
$pdf->SetFont('Arial','B',11);
$pdf->SetTextColor(0);
$pdf->Cell(125,5, $beneficiario,1,1,L);
$pdf->setx(10);
$pdf->SetFont('Arial','',10);
$pdf->SetTextColor(0,0,200);
$pdf->Cell(25,5, 'Referencia',1,0,L);
$pdf->SetFont('Arial','B',11);
$pdf->SetTextColor(0);
$pdf->Cell(50,5, $referencia,1,0,L);
$pdf->SetFont('Arial','',10);
$pdf->SetTextColor(0,0,200);
$pdf->Cell(25,5, 'Monto',1,0,L);
$pdf->SetFont('Arial','B',11);
$pdf->SetTextColor(0);
$pdf->Cell(50,5, $monto,1,1,R);
$pdf->setx(10);
$pdf->SetFont('Arial','',10);
$pdf->SetTextColor(0,0,200);
$pdf->Cell(25,5, 'Letras',1,0,L);
$pdf->SetFont('Arial','B',11);
$pdf->SetTextColor(0);
$pdf->Cell(125,5, $letras,1,1,L);
$pdf->setx(10);
$pdf->SetFont('Arial','',10);
$pdf->SetTextColor(0,0,200);
$pdf->Cell(25,5, 'Banco',1,0,L);
$pdf->SetFont('Arial','B',11);
$pdf->SetTextColor(0);
$pdf->Cell(50,5, 'BAC Credomatic',1,0,L);
$pdf->SetFont('Arial','',10);
$pdf->SetTextColor(0,0,200);
$pdf->Cell(25,5, 'No. Cuenta',1,0,L);
$pdf->SetFont('Arial','B',11);
$pdf->SetTextColor(0);
$pdf->Cell(50,5, $cuenta,1,1,R);
$pdf->setx(10);
$pdf->SetFont('Arial','',10);
$pdf->SetTextColor(0,0,200);
$pdf->Cell(25,15, 'Descripción',1,0,L);
$pdf->SetFont('Arial','B',11);
$pdf->SetTextColor(0);
$pdf->Cell(125,15, $motivo,1,1,L);
$pdf->setx(10);
$pdf->Cell(50,15, ' ',0,1,L);
$pdf->setx(10);
$pdf->SetFont('Arial','',9);
$pdf->SetTextColor(0,0,200);
$pdf->Cell(5,5, ' ',0,0,L);
$pdf->Cell(40,5, 'Fecha recibido',T,0,C);
$pdf->Cell(5,5, ' ',0,0,L);
$pdf->Cell(100,5, 'Nombre quién recibe',T,1,C);
$pdf->Cell(5,5, ' ',0,0,L);
$pdf->Cell(45,10, ' ',0,0,L);
$pdf->Cell(100,10, ' ',0,1,L);
$pdf->Cell(5,5, ' ',0,0,L);
$pdf->Cell(45,5, ' ',0,0,L);
$pdf->Cell(100,5, 'DPI (Identificación)',T,1,C);
$pdf->Cell(5,5, ' ',0,0,L);
$pdf->Cell(45,10, ' ',0,0,L);
$pdf->Cell(100,10, ' ',0,1,L);
$pdf->Cell(5,5, ' ',0,0,L);
$pdf->Cell(45,5, ' ',0,0,L);
$pdf->Cell(100,5, 'Firma',T,1,C);


$pdf->Output();
?>
