<?php
session_start();
$usuario = $_SESSION['usuario'];
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Aboga</title>

    <!--Link to source-->
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">

    <!--Data Table-->
    <link rel="stylesheet" href="dataTable/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="dataTable/css/responsive.bootstrap4.min.css">
   
    <!--Libreria para Calendario-->
    <script src="assets/datetime/js/jquery-3.3.1.min.js"></script>
    <script src="assets/datetime/js/gijgo.min.js" type="text/javascript"></script>
    <link href="assets/datetime/css/gijgo.min.css" rel="stylesheet" type="text/css" />   

    
        <!-- Favicon and touch icons -->
        <link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">

    <script defer src="assets/js/solid.js"></script>
    <script defer src="assets/js/fontawesome.js"></script>

    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="assets/css/style.css">
   
</head>

<body>
    <div class="wrapper">
        <!-- Sidebar  -->
        <nav id="sidebar">
            <div class="sidebar-header">
                <!--h3>ABOGA</h3-->
                <img src="img/balance.png" alt=""><h4>ABOGA</h4>
            </div>

            <ul class="list-unstyled">
                <li class="active">
                    <a style="text-align: center;">Men&uacute;</a>
                </li>
                
                <li class="">
                    <a href="menu.php?id=1"><i class="fa fa-th-large" aria-hidden="true"></i> Dashboards</a>
                </li>
                <li>
                    <a href="menu.php?id=2"><i class="fa fa-folder" aria-hidden="true"></i> Carpetas</a>
                </li> 
                <li>
                    <a href="menu.php?id=3"><i class="fa fa-users" aria-hidden="true"></i> Contactos</a>
                </li>
                <li>
                    <a href="menu.php?id=8"><i class="fa fa-address-card" aria-hidden="true"></i> Casos</a>
                </li>
                <li>
                    <a href="menu.php?id=12"><i class="fa fa-envelope" aria-hidden="true"></i> Tareas</a>
                </li>
                <li>
                    <a href="menu.php?id=6"><i class="fa fa-calendar" aria-hidden="true"></i> Calendario</a>
                </li>
                <li>
                    <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-credit-card" aria-hidden="true"></i> Cuenta Corriente</a>
                    <ul class="collapse list-unstyled" id="homeSubmenu">
                        <li>
                            <a href="menu.php?id=14">Cuenta Corriente</a>
                        </li>
                        <li>
                            <a href="menu.php?id=16">Consultas por Cliente</a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="#homeSubmenuX" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-window-restore" aria-hidden="true"></i> Facturaci&oacute;n</a>
                    <ul class="collapse list-unstyled" id="homeSubmenuX">
                        <li>
                            <a href="menu.php?id=24">Facturas</a>
                        </li>
                        <li>
                            <a href="menu.php?id=26">Modificaci&oacute;n de Facturas</a>
                        </li>
                    </ul>
                </li>                

                <li>
                    <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-credit-card" aria-hidden="true"></i> Cheques</a>
                    <ul class="collapse list-unstyled" id="pageSubmenu">
                        <li>
                            <a href="menu.php?id=22">Proveedores</a>
                        </li>
                        <li>
                            <a href="menu.php?id=29">Bancos</a>
                        </li>
                        <li>
                            <a href="menu.php?id=31">Cuentas Bancarias</a>
                        </li>
                        <li>
                            <a href="menu.php?id=37">Facturas Proveedores</a>
                        </li>
                        <li>
                            <a href="menu.php?id=39">Consulta de Facturas</a>
                        </li>
                        <li>
                            <a href="menu.php?id=40">Emisi&oacute;n de Cheques</a>
                        </li>                        
                    </ul>
                </li>

                <li>
                    <a href="menu.php?id=21"><i class="fa fa-search" aria-hidden="true"></i> Buscar Documentos</a>
                </li>

                <li>
                    <a href="menu.php?id=33"><i class="fa fa-user" aria-hidden="true"></i> Perfil</a>
                </li>

                <?php
                require_once 'db/conexion.php';
                $sql = mysqli_query($conn, "SELECT TIPO_USUARIO
                                                FROM tb_usuario
                                                WHERE ID_USUARIO = '".$usuario."'
                                                AND TIPO_USUARIO = 1");

                while($valida = $sql->fetch_array(MYSQLI_ASSOC)){

                $val_item = $valida['TIPO_USUARIO'];

                    if($val_item == 1){
                        echo "<li>";
                            echo "<a href='menu.php?id=36'><i class='fa fa-lock' aria-hidden='true'></i> Administrador</a>";
                        echo "</li>";
                    }
                }
                ?>

                <li>
                    <a href="menu.php?id=100"><i class="fa fa-lock" aria-hidden="true"></i> Salir</a>
                </li>                 

            </ul>

        </nav>

        <!-- Page Content  -->
        <div id="content">

            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">

                    <button type="button" id="sidebarCollapse" class="btn btn-info">
                        <i class="fas fa-align-left"></i>
                        <span>Men&uacute; </span>
                    </button>
                </div>
            </nav>

            <div class="container">

            <?php
                if (!isset($_REQUEST['id']) ){ 
                    require_once("view/dashboard.php");
        
                }else{
                $opc = $_REQUEST['id'];
                if ( strlen($opc) >= 0 ) {
                    switch ($opc)               
                { 

                case 1  : require_once("view/dashboard.php"); break; 
                case 2  : require_once("view/admin_archivos.php"); break;
                case 3  : require_once("view/contacto.php"); break;
                case 4  : require_once("insert/inserta_cliente.php"); break;
                case 6  : require_once("calendar_events.php"); break;
                case 7  : require_once("events_edit.php"); break;
                case 8  : require_once("view/casos.php"); break;
                case 9  : require_once("insert/crea_admin_area.php"); break;
                case 10  : require_once("insert/carpeta_niv_caso.php"); break;
                case 11  : require_once("insert/inserta_caso.php"); break;
                case 12  : require_once("view/eventos.php"); break;
                case 13  : require_once("insert/graba_tarea.php"); break;
                case 14  : require_once("view/cuenta_corriente.php"); break;
                case 15  : require_once("insert/insert_cta_corriente.php"); break;
                case 16  : require_once("view/consulta.php"); break;
                case 19  : require_once("insert/editar_contact.php"); break;
                case 20  : require_once("insert/editar_caso.php"); break;
                case 21  : require_once("view/buscador.php"); break;
                case 22  : require_once("view/proveedores.php"); break;
                case 23  : require_once("insert/insert_proveedor.php"); break;
                case 24  : require_once("view/facturacion.php"); break;
                case 25  : require_once("insert/inserta_factura.php"); break;
                case 26  : require_once("view/modifica_factura.php"); break;
                case 27  : require_once("insert/insert_edit_fac.php"); break;
                case 28  : require_once("insert/edita_proveedor.php"); break;
                case 29  : require_once("view/bancos.php"); break;
                case 30  : require_once("insert/insert_bancos.php"); break;
                case 31  : require_once("view/cuenta_bancaria.php"); break;
                case 32  : require_once("insert/insert_cuentas.php"); break;
                case 33  : require_once("view/perfil.php"); break;
                case 34  : require_once("insert/modifica_perfil.php"); break;
                case 35  : require_once("insert/modifica_pass.php"); break;
                case 36  : require_once("view/admin_accesos.php"); break;
                case 37  : require_once("view/factura_proveedor.php"); break;
                case 38  : require_once("insert/inserta_fac_provee.php"); break;
                case 39  : require_once("view/consulta_fac_pro.php"); break;
                case 40  : require_once("view/cheques.php"); break;
                case 41  : require_once("insert/insert_cheque.php"); break;
                case 42  : require_once("insert/insert_acceso_caso.php"); break;
                case 43  : require_once("insert/crea_usuario.php"); break;
                case 44  : require_once("insert/edita_usuario.php"); break;
                case 45  : require_once("insert/elimina_usuario.php"); break; 
                case 46  : require_once("insert/insert_items_menu.php"); break;
                case 47  : require_once("insert/elimina_acceso_items.php"); break;
                case 48  : require_once("insert/elimina_acceso_casos.php"); break;
                case 49  : require_once("view/detalle_cuenta.php"); break;

                case 100 : require_once("controller/logout.php"); break;

                                
                default : echo 'no existe esta opci&oacute;n'; break;
                        }
                    }
                }
            ?>
            </div>
            
        </div>
    </div>

    <?php
        if($_REQUEST['id'] != 6){
              echo "<script src='dataTable/js/jquery-3.3.1.js'></script>";
        }else{}
        
    ?>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>

    <?php
        if($_REQUEST['id'] != 6){
            echo "<script src='assets/js/jquery-3.3.1.slim.min.js'></script>";
            echo "<script src='dataTable/js/jquery-3.3.1.js'></script>";
        }else{}
        
    ?>
    <script src="dataTable/js/jquery.dataTables.min.js"></script>
    <script src="dataTable/js/dataTables.bootstrap4.min.js"></script>
    <script src="dataTable/js/dataTables.responsive.min.js"></script>
    <script src="dataTable/js/responsive.bootstrap4.min.js"></script> 
    
    <script src="assets/js/funcion_select.js"></script>


    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });
        });
    </script>

<script>
    $(document).ready(function() {
        
    $('#example').DataTable({
        "order": [[ 0, "asc" ]],
        "language": {
            "url": "assets/js/Spanish.json"
        }
    });
} );
</script>
<?php
    $git_version = file_get_contents("./.git/FETCH_HEAD");
    echo "ver:";
    echo  substr($git_version,0,6);

?>   

</body>

</html>