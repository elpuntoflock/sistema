<?php
require_once('db/conexion.php');

$caso = $_REQUEST['tmp'];

$detalle = mysqli_query($conn, "SELECT DATE_FORMAT(FECHA,'%d/%m/%Y')FEC, DESCRIPCION, MONTO, TIPO, MONEDA
                                  FROM tb_cargo_abono
                                  WHERE TIPO <> 6
                                    AND ID_CASO = '".$caso."'
                                  ORDER BY FECHA ASC");


$sql1 = mysqli_query($conn, "SELECT SALDO, MONEDA
              								FROM tb_corriente
              							  WHERE ID_CASO = '".$caso."'");

while($rowAA = $sql1->fetch_array(MYSQLI_ASSOC)){
	$saldo 	= $rowAA['SALDO'];
	$mone 	= $rowAA['MONEDA'];
}

if (isset($saldo) <= 0){
	$saldo = "0.00";
}else{
	$saldo = number_format($saldo,2,'.',',');
}

if (isset($mone) != TRUE){
	$tipo_moneda = " ";
}else{
	$tipo_moneda = $mone;
}

$encabezado = mysqli_query($conn, "SELECT DATE_FORMAT(FECHA,'%d/%m/%Y')FEC, MONTO, MONEDA
									FROM tb_cargo_abono
									WHERE TIPO = 6
									  AND ID_CASO = '".$caso."'");

while($rowA1 = $encabezado->fetch_array(MYSQLI_ASSOC)){
	$monto = number_format($rowA1['MONTO'],2,'.',',');
	$fecha = $rowA1['FEC'];
	$monedax = $rowA1['MONEDA'];
}


if (isset($monedax) != TRUE){
	$moneda_T = " ";
}else{
	$moneda_T = $monedax;
}

if (isset($monto) <= 0){
	$monto = "0.00";
}else{
	$monto = $monto;
}

if (isset($fecha) == null){
	$fecha = "";
}else{
	$fecha = $fecha;
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
  </head>
<body>

    <form action="menu.php?id=9" method="post">

		<div class="">
    <div class="row">
        <div class="col-md-12">
            <div class="wrapper-logo-secondary">
                <img src="img/logo/Law.png" alt="Logotipo Firma Law">
            </div>
        </div>
    </div>
</div>
        
            <div class="boton-formulario">
              <button type="submit" class="boton3">REGRESAR</button>
            </div>        


        <div class="">
              <div class="top-line" style="margin-top: 25px !important; margin-bottom: 30px;">
                  <div class="col-md-4" data-line="movil"><div class="line" style="margin-top: 25px !important;"></div></div>
                  <div class="col-md-4 titulo-seccion" style="margin-top: 15px !important;"><p>DETALLE DE CASO</p></div>
                  <div class="col-md-4"><div class="line" style="margin-top: 25px !important;"></div></div>
              </div>     
        </div>

       
    <div class="col-md-12 table-responsive bajar">

    	<table class="display nowrap table table-striped table-bordered" style="width:100%;">
    		<thead>
    			<tr>
    				<td>FECHA</td>
    				<td>DESCRIPCI&Oacute;N</td>
    				<td>MONTO</td>
    			</tr>
    		</thead>
    		<tbody>
          	<tr style="background-color: #005691;">
        		<td style="color: #fff; font-weight: bold;"><?php echo $fecha; ?></td>
        		<td style="color: #fff; font-weight: bold;">CARGO INICIAL DE CASO:</td>
        		<td style="color: #fff; font-weight: bold;"><?php echo $moneda_T.'&nbsp;&nbsp;'.$monto; ?></td>
        	</tr>     			
    		</tbody>
    	</table>

      <table id="example" class="display nowrap table table-striped table-bordered" style="width:100%;">
          <thead>
              <tr>
                  <th class="centrar">FECHA</th>
                  <th class="centrar">DETALLE</th>
                  <th class="centrar">MONTO</th>
              </tr>
          </thead>
          <tbody>
          <?php
      		while ($row = mysqli_fetch_array($detalle)){
        			$tipo 	= $row[3];
							$plata 	= number_format($row[2],2,'.',',');
							$moneda1 = $row[4];
						
						echo "<tr>";
	          echo "<td>$row[0]</td>";
	          echo "<td style='text-align: left;'>$row[1]</td>";
	        if(($tipo == 2) or ($tipo == 4) or ($tipo == 5)){
	          echo "<td style='text-align: right; color: blue; font-weight: bold;'>$moneda1&nbsp;&nbsp;$plata</td>";
	        }elseif (($tipo == 1) or ($tipo == 3)) {
	          echo "<td style='text-align: right; color: red; font-weight: bold;' >$moneda1&nbsp;&nbsp;$plata</td>";
					}
					echo "</tr>";

	        } 
      	?>       
        <tr style="background-color: #005691;">
        	<td style="color: #fff; font-weight: bold;">FECHA: <?php echo date('d/m/Y') ?> </td>
        	<td style="color: #fff; font-weight: bold;">SALDO TOTAL:</td>
        	<td style="color: #fff; font-weight: bold;"><?php echo $tipo_moneda.'&nbsp;&nbsp;'.$saldo; ?></td>
        </tr>  
          </tbody>
      </table>
    </div>
        </div>
    </div>      
    </form>
  
</body>
</html>
