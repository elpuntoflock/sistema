<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="alerta/css/sweetalert.css">
<script type="text/javascript" src="alerta/js/sweetalert-dev.js"></script>

<script>
	function ErrorAcceso()
		{
		swal({title:"Su usuario no tiene privilegios para esta pantalla..!", type:"error", showConfirmButton:false, text:"COMUNIQUESE CON EL ADMINISTRADOR", timer:'900'}, 
		function () 
		{
		location.href = "menu.php?id=22"; 
		});
		}
</script>

<?php
$nombre = $_SESSION['usuario'];
require_once('db/conexion.php');

$sql_permiso = mysqli_query($conn, "SELECT COUNT(*)CUENTA
										FROM tb_acceso_item
										WHERE id_usuario = '".$nombre."'
										  AND ITEM = ".$_REQUEST['id']."");
								
	while($valida = $sql_permiso->fetch_array(MYSQLI_ASSOC)){

		$resultado = $valida['CUENTA'];
	}

	if($resultado == 1){
		
	}else{
		echo "<script>ErrorAcceso();</script>";
    }
?>
<link rel="stylesheet" type="text/css" href="assets/css/archivos.css">
<div class="row">
    <div class="col-md-12">
        <div class="wrapper-logo-secondary">
            <img src="img/logo/Law.png" alt="Logotipo Firma Law">
        </div>
    </div>
</div>
<!--AREA PARA AGREGAR NUEVAS CARPETAS RAIZ PRINCIPALES-->
    <div class="row">
        <div class="top-line">
            <div class="col-md-4" data-line="movil"><div class="line" style="margin-top: 25px !important;"></div></div>
            <div class="col-md-4 titulo-seccion" style="margin-top: 15px !important;"><p>AREA DE TRABAJO</p></div>
            <div class="col-md-4"><div class="line" style="margin-top: 25px !important;"></div></div>
        </div>
    </div>

    <div class="container">
       <form action="menu.php?id=12" method="post">
           <div class="add">
                <a data-toggle='collapse' href='#principal' data-toggle="collapse"><h3 class=''><i class="fa fa-plus-circle"></i> Agregar Carpeta</h3></a>
           </div>
           <div class="col-md-12 collapse" style="margin-bottom: 15px;" id="principal">
               <div class="col-md-6">
                   <input type="text" name="nombre" id="nombre" class="form-control upper" autofocus="" required="" placeholder="Nombre de Carpeta Raiz">
               </div>
               <div class="col-md-1" style="margin-top: 1px;">
                    <div class="boton-formulario">
                      <button type="submit" class="boton3">CREAR</button>
                    </div>                   
               </div>
           </div>
       </form> 
    </div>    
<!--FINALIZA CARPETAS PRINCIPALES RAIZ-->
<?php
$nombre = $_SESSION['usuario'];
require_once('db/conexion.php');

$nivel_uno = mysqli_query($conn, "SELECT id_area, descripcion
                                FROM tb_archivos
                                WHERE tb__casos is null");


$casos = mysqli_query($conn, "SELECT COUNT(*)CUENTA
FROM tb_acceso_item
WHERE id_usuario = '".$nombre."'
  AND ITEM = 2");

while($val_caso = $casos->fetch_array(MYSQLI_ASSOC)){

$res_caso1 = $val_caso['CUENTA'];

} 



    $var = 0;
        /*INICA PROCESO NIVEL UNO PRINCIPAL*/
    echo "<div class='wrapper-area'>";
                while($txt = mysqli_fetch_array($nivel_uno)){
                    $var  ++;

                    echo "<div class=''>";
                        echo "<div class='panel-default contenido'>";
                            echo "<div class='panel-heading file-move'><a href='#' data-toggle='collapse' data-target='#$txt[1]$var'><p> <i class='far fa-folder fa-3x'></i> $txt[1] </p> </a>";
                            if($res_caso1 == 1){
                                echo "<a href='#' class='wrapper-add-file' data-toggle='modal' data-target='#ModalCrea'> <p><i class='fa fa-plus-circle'></i> CREAR CASO</p></a>";
                            }else{
                                
                            }
                            
                                echo "</div> <br>";
                                    $nivel_dos = mysqli_query($conn, "SELECT a.id_caso, a.description
                                                                        FROM tb_archivos a,
                                                                            tb_acceso b
                                                                        WHERE a.id_caso = b.ID_CASO
                                                                        AND a.tb__casos = '".$txt[0]."'
                                                                        AND b.ID_USUARIO = '".$nombre."'");
                       
                                        $tmp = 0;
                                        echo "<div id='$txt[1]$var' class='collapse panel panel-body file-move-dos contenido'>";
                                                while($row = mysqli_fetch_array($nivel_dos)){
                                                    $tmp ++;
                                                    $add_carpeta = $txt[1].$tmp.$txt[0];
                                                    $add_file    = $tmp.$txt[1].$txt[0];
                                                        echo utf8_encode("<a href='#' data-toggle='collapse' data-target='#$row[0]$tmp'><p class='text'><i class='far fa-folder fa-2x'></i>  $row[1]</p> </a> 
                                                                          <a href='#' class='wrapper-add-file' data-toggle='collapse' data-target='#$add_carpeta'><i class='fa fa-plus-circle'></i> AGREGAR CARPETA</a> 
                                                                          <a href='#' class='wrapper-add-doc' data-toggle='collapse' data-target='#$add_file'><i class='fa fa-plus-circle'></i> AGREGAR DOCUMENTO</a> <br>");
                                                        
                                                    /**** CREACION DE CARPETA EN NIVEL DEL CASO ******/ 
                                                    $detalle_caso = utf8_encode($row[1]);    
                                                        echo "<div id='$add_carpeta' class='collapse'>";
                                                            echo "<form action='menu.php?id=38&area=$txt[0]&caso=$row[0]&raiz=$txt[1]&des_caso=$detalle_caso&tipo=C' method='post'>";
                                                                echo "<div class='col-md-5'>";
                                                                    echo "<input type='text' name='carpeta' class='form-control upper' placeholder='Nombre Carpeta'>";
                                                                echo "</div>";
                                                                echo "<div class='col-md-1'>";
                                                                    echo "<div class='boton-formulario'>";
                                                                        echo "<button type='submit' class='boton3'>GRABAR</button>";
                                                                    echo "</div>";
                                                                echo "</div>";
                                                            echo "</form>";
                                                        echo "</div>";
                                                    /*********** FINAL CARPETA AL CASO **************/

                                                    /**** CREACION DE DOCUMENTO EN NIVEL DEL CASO *******/ 
                                                       
                                                        echo "<div id='$add_file' class='collapse'>";
                                                            echo "<form action='menu.php?id=38&area=$txt[0]&caso=$row[0]&raiz=$txt[1]&des_caso=$detalle_caso&tipo=D' method='post' enctype='multipart/form-data'>";
                                                                echo "<div class='col-md-5'>";
                                                                    echo "<input type='file' name='file' class='form-control'>";
                                                                echo "</div>";
                                                                echo "<div class='col-md-1'>";
                                                                    echo "<div class='boton-formulario'>";
                                                                        echo "<button type='submit' class='boton3'>GRABAR</button>";
                                                                    echo "</div>";
                                                                echo "</div>";
                                                            echo "</form>";
                                                        echo "</div>";
                                                    /*********** FINAL CARPETA AL CASO **************/                                                    

                                                        $carpeta = mysqli_query($conn, "SELECT id_carpeta, nombre_carpeta
                                                                                        FROM tb_carpetas
                                                                                        WHERE id_area = '".$txt[0]."'
                                                                                          AND id_caso = '".$row[0]."'");


                                                        $archivos = mysqli_query($conn, "SELECT b.ruta, b.descripcion 
                                                                                            FROM tb_archivos a,
                                                                                                tb_documento b
                                                                                            WHERE a.id_area = b.id_area
                                                                                            AND b.id_area = '".$txt[0]."'
                                                                                            AND b.tb__casos = '".$txt[0]."'
                                                                                            AND b.id_caso = '".$row[0]."'");

                                                            echo "<div id='$row[0]$tmp' class='collapse wrapper-download'>";
                                                                $file = 0;
                                                                while($carpetas = mysqli_fetch_array($carpeta)){
                                                                    $file ++;
                                                                    echo "<div class='wrapper-carpetas'>";
                                                                        echo utf8_encode("<a href='#' data-toggle='collapse' data-target='#$carpetas[1]$file'><p class='text'><i class='far fa-folder fa-1x'></i>  $carpetas[1]</p></a>
                                                                        <a href='#' class='wrapper-file' data-toggle='collapse' data-target='#$file$carpetas[1]'> <i class='fa fa-plus-circle'></i> AGREGAR DOCUMENTO</a><br>"); 
                                                                    echo "</div>";

                                                                /**** CREACION DE DOCUMENTO EN NIVEL DEL CASO *******/ 
                                                                    
                                                                echo "<div id='$file$carpetas[1]' class='collapse'>";
                                                                    echo "<form class='wrapper-form' action='menu.php?id=38&area=$txt[0]&caso=$row[0]&raiz=$txt[1]&des_caso=$detalle_caso&tipo=V&carpeta=$carpetas[1]&num=$carpetas[0]' method='post' enctype='multipart/form-data'>";
                                                                        echo "<div class='col-md-5'>";
                                                                            echo "<input type='file' name='file_dos' class='form-control'>";
                                                                        echo "</div>";
                                                                        echo "<div class='col-md-1'>";
                                                                            echo "<div class='boton-formulario'>";
                                                                                echo "<button type='submit' class='boton3'>GRABAR</button>";
                                                                            echo "</div>";
                                                                        echo "</div>";
                                                                    echo "</form>";
                                                                echo "</div>";
                                                            /*********** FINAL CARPETA AL CASO **************/ 


                                                                        $doc_carpeta = mysqli_query($conn, "SELECT ruta, descripcion
                                                                                                                FROM tb_documento
                                                                                                                WHERE id_area   = '".$txt[0]."'
                                                                                                                AND id_caso     = '".$row[0]."'
                                                                                                                AND id_carpeta  = '".$carpetas[0]."'");
                                                                    echo "<div id='$carpetas[1]$file' class='collapse wrapper-download'>";
                                                                        while($document_file = mysqli_fetch_array($doc_carpeta)){
                                                                            
                                                                                echo utf8_encode("<a href='$document_file[0]' target='_blank'><p class='text'><i class='fa fa-download fa-1x'></i>  $document_file[1]</p></a><br>"); 
                                                                            
                                                                        }
                                                                        echo "</div>";

                                                                }

                                                                while($res = mysqli_fetch_array($archivos)){
                                                                    
                                                                    echo utf8_encode("<a href='$res[0]' target='_blank'><p class='text'><i class='fa fa-download fa-1x'></i>  $res[1]</p></a><br>"); 

                                                                } 
                                                            echo "</div>";                                   

                                                }
                                        echo "</div>";
                        echo "</div>";
                    echo "</div>";            

                }
    echo "</div>";                                

?>

	<div class="">
	<div class="modal fade" id="ModalCrea" role="dialog">
		<div class="modal-dialog">
		
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">CREACI&Oacute;N DE CASOS</h4>
			</div>
			<div class="modal-body cuerpo">

			<form action="menu.php?id=30" method="post" enctype="multipart/form-data" onsubmit="return validar();">
			
			

			<div class="tabbable">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#tab1" data-toggle="tab">GENERALES CASO</a></li>
					<li><a href="#tab2" data-toggle="tab">DATOS JUZGADOS</a></li>
				</ul>
        <div class="tab-content">
			<div class="tab-pane active" id="tab1">
			<div>
					<label>CONTACTO</label>
						<select name="id_contacto" class="form-control" autofocus="">
							<option value="">SELECCIONAR CONTACTO</option>
						<?php

$contacto = mysqli_query($conn,"SELECT ID_CONTACTO, CONCAT(NOMBRES,' ',APELLIDOS)NOMBRES
								FROM tb_contacto");


						while ($row = mysqli_fetch_array($contacto))
						{
							echo '<option value="' . $row['ID_CONTACTO']. '">' . $row['ID_CONTACTO'].'-'. $row['NOMBRES'] . '</option>' . "\n";
						}
						?>
						</select>
					</div>
					<div>
					<label for="">RUTA DEL CASO</label>
					<select name="ruta" id="ruta" class="form-control">
						<option value="">SELECCIONAR RUTA</option>
						<?php
						
						$ruta = mysqli_query($conn, "SELECT id_area, descripcion
														FROM tb_archivos
														WHERE tb__casos is null");

							while($ftp = mysqli_fetch_array($ruta)){
								echo "<option value='$ftp[0]'>$ftp[1]</option>";
							}														
						
						?>
					</select>
					<?php
/*
					echo "<label for=''>RUTA DE CASO</label>";
									echo "<select name='ruta' id='ruta' class='form-control upper'>";

					function listar_archivos($carpeta){
						if(is_dir($carpeta)){
							if($dir = opendir($carpeta)){
								while(($archivo = readdir($dir)) !== false){
									if($archivo != '.' && $archivo != '..' && $archivo != '.htaccess'){
										$archivo = $archivo;
										echo "<option value='$carpeta$archivo'>$archivo</option>";
									}
								}
								closedir($dir);
							}
						}
						}
							echo listar_archivos(__DIR__."/CASOS/");
						echo "</select>"; */
				?>

					</div>

					<div>
						<label>CASO ORIGEN</label>
						<select name="origen" class="form-control">
							<option value="0">SIN CASO ORIGEN</option>
							<?php
								$sdl = mysqli_query($conn, "SELECT a.ID_CASO, a.CAUSA
															FROM tb_caso a,
																 tb_acceso b
															WHERE a.id_caso = b.id_caso
															  AND b.id_usuario = '".$nombre."'");
								while($res = mysqli_fetch_array($sdl)){
									echo '<option value="' . $res['ID_CASO']. '">' . $res['CAUSA'] . '</option>' . "\n";
								}							
							?>
						</select>
					</div>					
					
					<div>
						<label>DESCRIPCION</label>
						<input type="text" name="descripcion" class="form-control upper" placeholder="DESCRIPCI&Oacute;N DEL CASO" required="">
					</div>
					<div>
						<label>FECHA INICIO</label>
						<input type="text" name="fec_inicio" class="form-control centrar" id="datepicker_33" placeholder="FECHA INICIO" required="">
					</div>
					<div>
						<label>FECHA FINAL</label>
						<input type="text" name="fec_final" class="form-control centrar" id="datepicker_34" placeholder="FECHA FINAL">
					</div>
					<div>
						<label>OBSERVACIONES</label>
						<input type="text" name="text" class="form-control upper" placeholder="OBSERVACIONES">
					</div>

					<div>
						<label for="">ADJUNTAR ARCHIVO</label>
						<input type="file" name="file" class="form-control">
					</div>


			</div>
			<div class="tab-pane" id="tab2">
				<div>
					<label>CAUSA</label>
					<input type="text" name="causa" class="form-control upper" placeholder="Causa de Caso" id="causa">
				</div>
				<div>
					<label>N&Uacute;MERO MP</label>
					<input type="text" name="mun_mp" class="form-control upper" placeholder="N&uacute;mero MP">
				</div>
				<div>
					<label>FISCALIA</label>
					<input type="text" name="fiscalia" class="form-control upper" placeholder="Fiscalia">
				</div>
				<div>
					<label>JUZGADO</label>
					<input type="text" name="juzgado" class="form-control upper" placeholder="Juzgado">
				</div>
				<div>
					<label>JUEZ</label>
					<input type="text" name="juez" class="form-control upper" placeholder="Juez">
				</div>
				<div>
					<label>OFICIAL</label>
					<input type="text" name="oficial" class="form-control upper" placeholder="oficial">
				</div>	
				<div>
					<label>DIRECCI&Oacute;N JUZGADO</label>
					<input type="text" name="direccion_juzgado" class="form-control upper" placeholder="direcci&oacute;n juzgado">
				</div>	
				<div>
					<label>ZONA JUZGADO</label>
					<select name="zona_juz" class="form-control">
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
					<option value="7">7</option>
					<option value="8">8</option>
					<option value="9">9</option>
					<option value="10">10</option>
					<option value="11">11</option>
					<option value="12">12</option>
					<option value="13">13</option>
					<option value="14">14</option>
					<option value="15">15</option>
					<option value="16">16</option>
					<option value="17">17</option>
					<option value="18">18</option>
					<option value="19">19</option>
					<option value="21">21</option>
					<option value="24">24</option>
					<option value="25">25</option>						
					</select>
				</div>
				<div>
					<label>TELEFONO JUZGADO</label>
					<input type="text" name="tel_juzgado" class="form-control upper" placeholder="telefono juzgado">
				</div>															
			</div>
        </div>
		<div class="boton-formulario bajar">
						<button type="submit" class="boton3">GRABAR</button>
						<button type="button" class="boton_close" data-dismiss="modal">CERRAR</button>
					</div>
		</form>
        </div>
				<!--form action="inserta_caso.php" method="post" enctype="multipart/form-data">
					
				</form-->
			</div>
		</div>
		
		</div>
	</div>
</div>
