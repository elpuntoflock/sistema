<?php
require_once('lib/pdf/mpdf.php');
/*Conexion a la Base de Datos*/
require_once('db/conexion.php');

$nombre = strtoupper($_REQUEST['nombre']);

$fecha = date('d/m/Y');

function quitar_tildes($cadena) {
	$no_permitidas= array ("á","é","í","ó","ú","Á","É","Í","Ó","Ú","ñ","À","Ã","Ì","Ò","Ù","Ã™","Ã ","Ã¨","Ã¬","Ã²","Ã¹","ç","Ç","Ã¢","ê","Ã®","Ã´","Ã»","Ã‚","ÃŠ","ÃŽ","Ã”","Ã›","ü","Ã¶","Ã–","Ã¯","Ã¤","«","Ò","Ã","Ã„","Ã‹","Ñ");
	$permitidas= array ("a","e","i","o","u","A","E","I","O","U","n","N","A","E","I","O","U","a","e","i","o","u","c","C","a","e","i","o","u","A","E","I","O","U","u","o","O","i","a","e","U","I","A","E","Ñ");
	$texto = str_replace($no_permitidas, $permitidas ,$cadena);
	return $texto;
	}


$detalle = mysqli_query($conn, "SELECT A.CAUSA, CONCAT(B.NOMBRES,' ',B.APELLIDOS)NOMBRES, A.JUZGADO, A.DESCRIPCION,
                                DATE_FORMAT(A.FECHA_INI,'%d/%m/%Y'), DATE_FORMAT(A.FECHA_FIN,'%d/%m/%Y')
                                FROM tb_caso A,
                                    tb_contacto B,
                                    tb_acceso C
                                WHERE A.ID_CONTACTO = B.ID_CONTACTO
                                AND A.ID_CASO 	= C.ID_CASO
                                AND C.ID_USUARIO  = '".$nombre."'");
                                  
    while ($rowX = mysqli_fetch_array($detalle)){
        
        $loop = $loop .'
        
        <tr>
        <td style="font-weight: bold; color: #000 !important;">'.$rowX[0].'</td>
        <td style="font-weight: bold; color: #000 !important;">'.strtoupper($rowX[1]).'</td>
        <td style="font-weight: bold; color: #000 !important;">'.strtoupper($rowX[2]).'</td>
        <td style="font-weight: bold; color: #000 !important;">'.strtoupper(quitar_tildes($rowX[3])).'</td>
        <td style="font-weight: bold; color: #000 !important;">'.$rowX[4].'</td>
        <td style="font-weight: bold; color: #000 !important;">'.$rowX[5].'</td>
        </tr>
        ';
    
    }  
                         
    
    $html = "<header class='clearfix'>

    <h1>REPORTE CONSOLIDADO DE CASOS</h1>
    <br>
    <br>
    <div style='text-align: right;'>Fecha de Impresi&oacute;n: $fecha</div>
    <br>
    <br>
    <div id='logo'>
    <img src='img/logo/Law.png' style='width: 150px;'>
    </div>
    <br>
    <br>
    <br>

    
    
    </header>
    <main>
    <!--Datos de Encabezado-->
    <table>
    <thead>
    <tr style='background-color: #005691;'>
    <th class='service' style='color: #fff; text-align: center;'>CAUSA</th>
    <th class='service' style='color: #fff; text-align: center;'>CLIENTE</th>
    <th class='desc' style='text-align: center; color: #fff; text-align: center;'>JUZGADO</th>
    <th class='service' style='color: #fff; text-align: center;'>DESCRIPCI&Oacute;N</th>
    <th class='service' style='color: #fff; text-align: center;'>FECHA INICIO</th>
    <th class='service' style='color: #fff; text-align: center;'>FECHA FIN</th>
    
    </tr>
    </thead>
    <tbody>
    $loop;
    <br>
     </tbody>
    </table>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <div style='text-align:center;'>Firma:___________________________________</div>
    <div style='text-align:center;'>Lic. Victor P&eacute;rez</div>
    
    </main>";  
  
  $mpdf = new mPDF('c','A4');
$css = file_get_contents('lib/reportes/css/style.css');
$mpdf->writeHTML($css,1);
$mpdf->WriteHTML(utf8_encode($html));
//$mpdf->writeHTML($html);
$mpdf->Output('reporte_casos.pdf','I');

?>