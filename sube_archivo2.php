<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="alerta/css/sweetalert.css">
    <script type="text/javascript" src="alerta/js/sweetalert-dev.js"></script>

<script>
function Carpeta()
    {
      swal({title:"Carpeta Creada con Exito....!!!", type:"success", showConfirmButton:false, text:"RUTA CREADA...", timer:'2000'}, 

      function () 
    {
      location.href = "menu.php?id=10"; 
    });
    }

function Error()
    {
      swal1({title:"Error la Carpeta / Ruta ya Existe Creada....!", type:"error", showConfirmButton:false, text:"FAVOR DE VERIFICAR.", timer:'1000'}, 

      function () 
    {
      location.href = "menu.php?id=10"; 
    });
    }

</script> 
</head>
<body>
<?php
$ruta = $_POST['ruta'];

$archivo = (isset($_FILES['file'])) ? $_FILES['file'] : null;

if ($archivo) {
   $ruta_destino_archivo = $ruta.'/'."{$archivo['name']}";
   $archivo_ok = move_uploaded_file($archivo['tmp_name'], $ruta_destino_archivo);
   echo "<script>Carpeta();</script>";
}

?>
</body>
</html>