<?php
$usuario =  $_SESSION['usuario'];
require_once('db/conexion.php');

$sql = mysqli_query($conn, "SELECT NOMBRES, NOMBRE2, APELLIDO1, APELLIDO2, EMAIL, CELULAR
                             from tb_usuario
                             where id_usuario = '".$usuario."'");

    while($row = $sql->fetch_array(MYSQLI_ASSOC)){

    $pnombre    = $row['NOMBRES'];
    $snombre    = $row['NOMBRE2'];
    $papellido  = $row['APELLIDO1'];
    $sapellido  = $row['APELLIDO2'];
    $email      = $row['EMAIL'];
    $celular    = $row['CELULAR'];
    
    }                              

?>
<script src="js/jquery.min.js"></script>

<style>
.centrar {
    text-align: center !important;
}
</style>

<div class="">

    <div class="row">
        <div class="col-md-12">
            <div class="wrapper-logo-secondary">
                <img src="img/logo/Law.png" alt="Logotipo Firma Law">
            </div>
        </div>    
    </div>

    <div class="row bajar">
        <div class="col-md-12">
            <div class="top-line" style="margin-top: 25px; margin-bottom: 30px;">
                <div class="col-md-4" data-line="movil"><div class="line"></div></div>
                <div class="col-md-4 titulo-seccion bajar_espacio"><p>PERFIL USUARIO</p></div>
                <div class="col-md-4"><div class="line"></div></div>
            </div>    
        </div>
    </div>

    <div class="row">
        <div class="col-md-10 col-md-offset-1">
        <form action="menu.php?id=26" method="post">
           <div class="wrapper-formulario-pass">
           
           <div class="bajar">
                <label>PRIMER NOMBRE</label>
                <input type="text" class="form-control centrar" name="p_nombre" value="<?php echo $pnombre; ?>" require="">
            </div>

            <div class="bajar">
                <label>SEGUNDO NOMBRE</label>
                <input type="text" class="form-control centrar" name="s_nombre" value="<?php echo $snombre; ?>">
            </div>

            <div class="bajar">
                <label>PRIMER APELLIDO</label>
                <input type="text" class="form-control centrar" name="p_apellido" value="<?php echo $papellido; ?>" require="">
            </div> 

            <div class="bajar">
                <label>SEGUNDO APELLIDO</label>
                <input type="text" class="form-control centrar" name="s_apellido" value="<?php echo $sapellido; ?>">
            </div> 

            <div class="bajar">
                <label for="">EMAIL</label>
                <input type="text" class="form-control centrar" name="email" value="<?php echo $email; ?>" require="">
            </div>   

            <div class="bajar">
                <label>CELULAR</label>
                <input type="text" class="form-control centrar" name="celular" value="<?php echo $celular; ?>" require="">
            </div> 

            <div class="bajar centrar">
                <button type="submit" class="boton3">GRABAR</button>
            </div>  

           </div>
        </form>                                                               
        </div>
    </div>

    <div class="row bajar">
        <div class="col-md-12">
            <div class="top-line" style="margin-top: 25px; margin-bottom: 30px;">
                <div class="col-md-4" data-line="movil"><div class="line"></div></div>
                <div class="col-md-4 titulo-seccion bajar_espacio"><p>REINICIO PASSWORD</p></div>
                <div class="col-md-4"><div class="line"></div></div>
            </div>    
        </div>
    </div> 

    <div class="row bajar">
        <div class="col-md-4 col-md-offset-4">
        <form action="menu.php?id=27" method="post" onSubmit="return validacion();">
            <div class="bajar">
                <label>INGRESAR PASSWORD</label>
                <input type="password" name="password" class="form-control centrar" placeholder="Ingresar Password" id="pass1">
            </div>

            <div class="bajar">
                <label>CONFIRMAR PASSWORD</label>
                <input type="password" name="passwordX" class="form-control centrar" placeholder="Confirmar Password" id="pass2">
            </div>

            <div class="bajar centrar">
                <button type="submit" class="boton3">CAMBIAR PASSWORD</button>
            </div>  
        </form>                                                               
        </div>
    </div>       

</div>

<script>
    function validacion(){
        var valor1 = document.getElementById('pass1').value;
        var valor2 = document.getElementById('pass2').value;
        

        if(valor1 != valor2){
            alert('La contraseña no es igual, Favor de verificar....');
            return false;
        }
        
    }
    
</script>