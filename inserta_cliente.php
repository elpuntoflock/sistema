<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="alerta/css/sweetalert.css">
    <script type="text/javascript" src="alerta/js/sweetalert-dev.js"></script>

<script>
function Error()
    {
      swal({title:"Clave y/o Usuario Invalido!", type:"error", showConfirmButton:false, text:"INICIO DE SESION DENEGADO", timer:'2000'}, 

      function () 
    {
      location.href = "menu.php?id=1"; 
    });
    }

function Ingresado()
    {
      swal({title:"Cliente Grabado con Exito!", type:"success", showConfirmButton:false, text:"DATOS GRABADOS", timer:'2000'}, 
      function () 
    {
      location.href = "menu.php?id=1"; 
    });
    }
</script> 
	<title></title>
</head>
<body>
<?php
session_start();
require_once('db/conexion.php');

$nombre 		= $_SESSION['usuario'];
$usuario 		= strtoupper($nombre);

$nombres		= strtoupper($_POST['nombres']);
$apellidos		= strtoupper($_POST['apellidos']);
$cui 			= $_POST['cui'];
$pasaporte 		= $_POST['pasaporte'];
$sexo 			= strtoupper($_POST['sexo']);
$direccion		= strtoupper($_POST['direccion']);
$zona			= $_POST['zona'];
$pais			= $_POST['pais'];
$departamento	= $_POST['departamento'];
$municipio		= $_POST['municipio'];
$telefono		= $_POST['telefono'];
$ape_cadasa 	= $_POST['apellido_casada'];
$email 			= strtolower($_POST['email']);


$sql = mysqli_query($conn, "SELECT MAX(ID_CONTACTO)+1 CONTEO
							FROM tb_contacto");
while($row = $sql->fetch_array(MYSQLI_ASSOC)){
    $id_contacto = $row['CONTEO'];

    if($id_contacto == 0){
    	$id_contacto = 1;
    }else{
    	$id_contacto = $row['CONTEO'];
    }

}

$insert = mysqli_query($conn, "INSERT INTO tb_contacto (
			ID_CONTACTO,
			ID_USUARIO,
			NOMBRES,
			APELLIDOS,
			APELLIDO_CASADA,
			SEXO,
			DIRECCION,
			ZONA,
			DEPARTAMENTO,
			MUNICIPIO,
			PAIS,
			CUI,
			PASAPORTE,
			FECHA_CREA,
			TELEFONO,
			EMAIL)
			VALUES(
			'".$id_contacto."',
			'".$usuario."',
			'".$nombres."',
			'".$apellidos."',
			'".$ape_cadasa."',
			'".$sexo."',
			'".$direccion."',
			'".$zona."',
			'".$departamento."',
			'".$municipio."',
			'".$pais."',
			'".$cui."',
			'".$pasaporte."',
			CURRENT_TIMESTAMP,
			'".$telefono."',
			'".$email."')");

if($insert == TRUE){
	echo "<script>Ingresado();</script>";
}else{
	echo "<script>Error();</script>";
}

$conn->close();			

?>
</body>
</html>