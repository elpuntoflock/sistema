<?php
$usuario 		= $_SESSION['usuario'];
$usuario 		= strtoupper($usuario);
?>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="alerta/css/sweetalert.css">
<script type="text/javascript" src="alerta/js/sweetalert-dev.js"></script>

<script>

function Corriente()
    {
      swal({title:"Cuenta Corriente Grabada con Exito..!", type:"success", showConfirmButton:false, text:"DATOS GRABADOS", timer:'2000'}, 
      function () 
    {
      location.href = "menu.php?id=4"; 
    });
    }

function Abono()
    {
      swal({title:"El Abono ha sido grabado con Exito..!", type:"success", showConfirmButton:false, text:"DATOS GRABADOS", timer:'2000'}, 
      function () 
    {
      location.href = "menu.php?id=4"; 
    });
    }  

function Aumento()
    {
      swal({title:"El Incremento se ha grabado con Exito..!", type:"success", showConfirmButton:false, text:"DATOS GRABADOS", timer:'2000'}, 
      function () 
    {
      location.href = "menu.php?id=4"; 
    });
    }   

function Insuficiente()
    {
      swal({title:"Saldo insuficiente para poder rebajar..!", type:"error", showConfirmButton:false, text:"FAVOR DE VERIFICAR", timer:'2000'}, 
      function () 
    {
      location.href = "menu.php?id=4"; 
    });
    } 

function SinSaldo()
    {
      swal({title:"La cuenta no tiene saldo pendiente..!", type:"error", showConfirmButton:false, text:"FAVOR DE VERIFICAR", timer:'2000'}, 
      function () 
    {
      location.href = "menu.php?id=4"; 
    });
    }  	 	      
</script> 

<?php
require_once('db/conexion.php');

$id_caso		= $_POST['id_caso'];
$monto 			= $_POST['monto'];
$tipo_caso		= $_POST['tipo'];
$descripcion 	= strtoupper($_POST['desc_abono']);
$observaciones 	= strtoupper($_POST['obs_abono']);
$moneda 		= strtoupper($_POST['moneda']);

/*BUSCA SI YA EXISTE EL ABONO INICIAL DEL CASO*/
$busca = mysqli_query($conn, "SELECT COUNT(*) VALOR
								FROM tb_corriente
								WHERE ID_CASO = '".$id_caso."'");

while($row1 = $busca->fetch_array(MYSQLI_ASSOC)){

    $busca_valor = $row1['VALOR'];
        
}

/*VALIDACION SE YA TIENE CARGO INICIAL PARA INSERT*/
if($busca_valor == 0){

/*BUSCA EL MAXIMO PARA ID_CORRIENTE*/
$sql = mysqli_query($conn, "SELECT MAX(ID_CORRIENTE)+1 CONTEO
							FROM tb_corriente");

while($row = $sql->fetch_array(MYSQLI_ASSOC)){

    $id_corriente = $row['CONTEO'];

    if($id_corriente == 0){
    	$id_corriente = 1;
    }else{
    	$id_corriente = $row['CONTEO'];
    }
        
}


if($tipo_caso == 6){

/****INSERTA tb_corriente EL CARGO INICIAL****/	
$insert = mysqli_query($conn, "INSERT INTO tb_corriente (
								ID_CORRIENTE,
								ID_CASO,
								SALDO,
								FEC_ULT_CA,
								FECHA_ACTUALIZA,
								MONEDA)VALUES(
								'".$id_corriente."',
								'".$id_caso."',
								'".$monto."',
								CURRENT_TIMESTAMP,
								CURRENT_TIMESTAMP,
								'".$moneda."')");

if($insert == TRUE){
	//echo "Cuenta Corriente Insertada Correctamente...!!!";
}else{
	//echo "Error al Insertar cuenta corriente....";
}
/****FINALIZA INSERT DE CUENTA CORRIENTE CARGA INICAL*****/

/*****INICIA INSERT tb_cargo_abono INICIAL*****/

/*VERIFICA SI YA SE INSERTO tb_corriente INICIAL PARA tb_cargo_abono*/
$cuenta = mysqli_query($conn, "SELECT COUNT(ID_CORRIENTE) CUENTA
								FROM tb_corriente
								WHERE ID_CORRIENTE = '".$id_corriente."'");

while($row = $cuenta->fetch_array(MYSQLI_ASSOC)){

    $verifica = $row['CUENTA'];

}
echo "<br>";
/*****VALIDACION SI ENCONTRO EL INSERT DE CORRIENTE*****/
if($verifica == 1){

$cargo_abono = mysqli_query($conn, "SELECT MAX(ID_CA)+1 CONTEO
									FROM tb_cargo_abono");
while($row = $cargo_abono->fetch_array(MYSQLI_ASSOC)){
    $id_cabono = $row['CONTEO'];

    if($id_cabono == 0){
    	$id_cabono = 1;
    }else{
    	$id_cabono = $row['CONTEO'];
    }
        
}	

	$insert_ca = mysqli_query($conn, "INSERT INTO tb_cargo_abono (
										ID_CA,
										ID_CASO,
										DESCRIPCION,
										MONTO,
										TIPO,
										OBSERVACIONES,
										ID_USUARIO,
										FECHA,
										MONEDA)VALUES(
										'".$id_cabono."',
										'".$id_caso."',
										'".$descripcion."',
										'".$monto."',
										'".$tipo_caso."',
										'".$observaciones."',
										'".$usuario."',
										CURRENT_TIMESTAMP,
										'".$moneda."')");
	if($insert_ca == TRUE){
		echo "<script>Corriente();</script>";
	}else{
		echo "Error al insertar Cargo Abono Inicial";
	}

}

}

}elseif ($tipo_caso != 6) {

$dinero = mysqli_query($conn, "SELECT COUNT(*) DINERO
								FROM tb_corriente
								WHERE SALDO >= '".$monto."'");

while($rowAA = $dinero->fetch_array(MYSQLI_ASSOC)){

    $val_dinero = $rowAA['DINERO'];

}

if($val_dinero == 0){

	echo "El caso no tiene saldo o el Valor a ingresar es mayor, Verificar Cuenta...!!!";

}else{
/*INICIA VALIDACION DE ABONOS NORMAL*/
if(($tipo_caso == 2) OR ($tipo_caso == 4) OR ($tipo_caso == 5)) {

$corri = mysqli_query($conn, "SELECT ID_CORRIENTE, SALDO
								FROM tb_corriente
								WHERE ID_CASO = '".$id_caso."'");

while($rowXX = $corri->fetch_array(MYSQLI_ASSOC)){
    $val_corriente = $rowXX['ID_CORRIENTE'];
    $val_saldo 	   = $rowXX['SALDO'];

    $nue_saldo 	   = $val_saldo-$monto;	
}

if($val_saldo == 0){
	echo "<script>SinSaldo();</script>";
}elseif($val_saldo < $monto){
	echo "<script>Insuficiente();</script>";
}else{

$abono_normal = mysqli_query($conn, "UPDATE tb_corriente 
								SET SALDO = '".$nue_saldo."',
								    FEC_ULT_AB 	= CURRENT_TIMESTAMP,
								    FECHA_ACTUALIZA = CURRENT_TIMESTAMP
							  WHERE ID_CORRIENTE = '".$val_corriente."'
							  	AND ID_CASO 	 = '".$id_caso."'");
if($abono_normal == TRUE){
	//echo "Si se hizo el abono....!!!!";
	
$max_cargo = mysqli_query($conn, "SELECT MAX(ID_CA)+1 CONTEO
									FROM tb_cargo_abono");
while($row = $max_cargo->fetch_array(MYSQLI_ASSOC)){

    $id_cabono = $row['CONTEO'];

    if($id_cabono == 0){
    	$id_cabono = 1;
    }else{
    	$id_cabono = $row['CONTEO'];
    }
        
}


	$insert_ca = mysqli_query($conn, "INSERT INTO tb_cargo_abono (
										ID_CA,
										ID_CASO,
										DESCRIPCION,
										MONTO,
										TIPO,
										OBSERVACIONES,
										ID_USUARIO,
										FECHA,
										MONEDA)VALUES(
										'".$id_cabono."',
										'".$id_caso."',
										'".$descripcion."',
										'".$monto."',
										'".$tipo_caso."',
										'".$observaciones."',
										'".$usuario."',
										CURRENT_TIMESTAMP,
										'".$moneda."')");
	if($insert_ca == TRUE){
		echo "<script>Abono();</script>";
	}else{
	}	
	
}else{
	//echo "Ocurrio Algun Problema en el Abono....!!!";
}

}

}

if(($tipo_caso == 1) OR ($tipo_caso == 3)){

$corriente = mysqli_query($conn, "SELECT ID_CORRIENTE, SALDO
								FROM tb_corriente
								WHERE ID_CASO = '".$id_caso."'");

while($rowXY = $corriente->fetch_array(MYSQLI_ASSOC)){

    $val_corriente = $rowXY['ID_CORRIENTE'];
    $val_saldo 	   = $rowXY['SALDO'];

    $nuevo_saldo 	   = $val_saldo+$monto;	
}

$cargo_normal = mysqli_query($conn, "UPDATE tb_corriente
								SET SALDO = '".$nuevo_saldo."',
								    FEC_ULT_AB 	= CURRENT_TIMESTAMP,
								    FECHA_ACTUALIZA = CURRENT_TIMESTAMP
							  WHERE ID_CORRIENTE = '".$val_corriente."'
							  	AND ID_CASO 	 = '".$id_caso."'");
if($cargo_normal == TRUE){
	//echo "Si se hizo el aumento....!!!!";
	
				$max_cargo = mysqli_query($conn, "SELECT MAX(ID_CA)+1 CONTEO
													FROM tb_cargo_abono");
				while($row = $max_cargo->fetch_array(MYSQLI_ASSOC)){

				    $id_cabono = $row['CONTEO'];

				    if($id_cabono == 0){
				    	$id_cabono = 1;
				    }else{
				    	$id_cabono = $row['CONTEO'];
				    }
				        
				}

				$insert_ca = mysqli_query($conn, "INSERT INTO tb_cargo_abono (
														ID_CA,
														ID_CASO,
														DESCRIPCION,
														MONTO,
														TIPO,
														OBSERVACIONES,
														ID_USUARIO,
														FECHA,
														MONEDA)VALUES(
														'".$id_cabono."',
														'".$id_caso."',
														'".$descripcion."',
														'".$monto."',
														'".$tipo_caso."',
														'".$observaciones."',
														'".$usuario."',
														CURRENT_TIMESTAMP,
														'".$moneda."')");
					if($insert_ca == TRUE){
						echo "<script>Aumento();</script>";
					}else{

					}

	}else{
		//echo "Ocurrio Algun Problema en el Aumento....!!!";
	}




}

}

}else{
	echo "Ya tiene cargo Inicial no puede hacer otro a este caso....!!!";
}

?>
