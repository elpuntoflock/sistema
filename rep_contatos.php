<?php
require_once('lib/pdf/mpdf.php');
/*Conexion a la Base de Datos*/
require_once('db/conexion.php');

$contacto = $_REQUEST['tmp'];
$moneda   = $_REQUEST['view'];
$fecha = date('d/m/Y');


$detalle = mysqli_query($conn, "SELECT ID_CONTACTO, NOMBRES, APELLIDOS, CUI, TELEFONO
                                FROM tb_contacto");
                                  
    while ($rowX = mysqli_fetch_array($detalle)){
        
        $loop = $loop .'
        <tr>
        <td style="font-weight: bold; color: #000 !important;">'.$rowX[0].'</td>
        <td style="font-weight: bold; color: #000 !important;">'.$rowX[1].'</td>
        <td style="font-weight: bold; color: #000 !important;">'.$rowX[2].'</td>
        <td style="font-weight: bold; color: #000 !important;">'.$rowX[3].'</td>
        <td style="font-weight: bold; color: #000 !important;">'.$rowX[4].'</td>
        </tr>
        ';
    
    }  
                         
    
    $html = "<header class='clearfix'>

    <h1>REPORTE CONSOLIDADO DE CONTACTOS</h1>
    <br>
    <br>
    <div style='text-align: right;'>Fecha de Impresi&oacute;n: $fecha</div>
    <br>
    <br>
    <div id='logo'>
    <img src='img/logo/Law.png' style='width: 150px;'>
    </div>
    <br>
    <br>
    <br>

    
    
    </header>
    <main>
    <!--Datos de Encabezado-->
    <table>
    <thead>
    <tr style='background-color: #005691;'>
    <th class='service' style='color: #fff; text-align: center;'>CODIGO</th>
    <th class='service' style='color: #fff; text-align: center;'>NOMBRES</th>
    <th class='desc' style='text-align: center; color: #fff; text-align: center;'>APELLIDOS</th>
    <th class='service' style='color: #fff; text-align: center;'>CUI</th>
    <th class='service' style='color: #fff; text-align: center;'>TELEFONO</th>
    
    </tr>
    </thead>
    <tbody>
    $loop;
    <br>
     </tbody>
    </table>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <div style='text-align:center;'>Firma:___________________________________</div>
    <div style='text-align:center;'>Lic. Victor P&eacute;rez</div>
    
    </main>";  
  
  $mpdf = new mPDF('c','A4');
$css = file_get_contents('lib/reportes/css/style.css');
$mpdf->writeHTML($css,1);
$mpdf->WriteHTML(utf8_encode($html));
//$mpdf->writeHTML($html);
$mpdf->Output('reporte_contactos.pdf','I');

?>