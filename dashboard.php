<?php
require_once('db/conexion.php');

$contacto = mysqli_query($conn,"SELECT COUNT(*)CONTEO
                                FROM tb_contacto");

while($row = $contacto->fetch_array(MYSQLI_ASSOC)){
   $num_contacto = $row['CONTEO'];
}

$casos = mysqli_query($conn,"SELECT COUNT(*)TOTAL
                                FROM tb_caso");

while($rest = $casos->fetch_array(MYSQLI_ASSOC)){
    $num_casos = $rest['TOTAL'];
 }                                

?>

    <div class="wrapper-dashboard">
        <div class="row m-t-25">
            <div class="col-md-4">
                <div class="wrapper-logo-main bajar">
                    <img src="img/logo/Law.png" alt="Logotipo Firma Law">
                </div>
            </div>
            <div class="col-md-4">
                <div class="overview-item overview-item--c1">
                    <div class="overview__inner-1">
                        <div class="overview-box clearfix">
                            <div class="icon">
                                <i class="zmdi zmdi-account-o"></i>
                            </div>
                            <div class="text">
                                <span>Contactos</span>
                                <!--h2><i class="far fa-address-card"></i> <?php echo $num_contacto; ?></h2-->
                                <span><a href="menu.php?id=1"><h2><i class="far fa-address-card"></i> <?php echo $num_contacto; ?></h2> </a></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="overview-item overview-item--c2">
                    <div class="overview__inner-2">
                        <div class="overview-box clearfix">
                            <div class="icon">
                                <i class="zmdi zmdi-shopping-cart"></i>
                            </div>
                            <div class="text">
                                <span>Casos</span>
                                <!--h2><i class="far fa-folder"></i> <?php echo $num_casos; ?></h2-->
                                <!--span><button type="button" class="boton-dash"><a href="menu.php?id=2">Ir a Casos <i class="far fa-arrow-alt-circle-right"></i></a></button></span-->
                                <span><a href="menu.php?id=2"><h2><i class="fas fa-th-large"></i> <?php echo $num_casos; ?></h2> </a></span>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>

        <div class="row m-t-25 bajar">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <div class="overview-item overview-item--c1">
                    <div class="overview__inner-1">
                        <div class="overview-box clearfix">
                            <div class="icon">
                                <i class="zmdi zmdi-shopping-cart"></i>
                            </div>
                            <div class="text">
                                <span>calendario</span>
                                <!--h2><i class="far fa-folder"></i> <?php echo $num_casos; ?></h2-->
                                <!--span><button type="button" class="boton-dash"><a href="menu.php?id=2">Ir a Casos <i class="far fa-arrow-alt-circle-right"></i></a></button></span-->
                                <span><a href="menu.php?id=8"><h2><i class="far fa-calendar-alt"></i> </h2> </a></span>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

                        <div class="col-md-4">
                <div class="overview-item overview-item--c2">
                    <div class="overview__inner-2">
                        <div class="overview-box clearfix">
                            <div class="icon">
                                <i class="zmdi zmdi-shopping-cart"></i>
                            </div>
                            <div class="text">
                                <span>Cuenta Corriente</span>
                                <!--h2><i class="far fa-folder"></i> <?php echo $num_casos; ?></h2-->
                                <!--span><button type="button" class="boton-dash"><a href="menu.php?id=2">Ir a Casos <i class="far fa-arrow-alt-circle-right"></i></a></button></span-->
                                <span><a href="menu.php?id=9"><h2><i class="fas fa-credit-card"></i> </h2> </a></span>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>