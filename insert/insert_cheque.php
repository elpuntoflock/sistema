<?php
$usuario 		= $_SESSION['usuario'];
$usuario 		= strtoupper($usuario);
require_once 'db/conexion.php';

$cuenta         = $_POST['cuenta'];
$factura        = $_POST['factura'];
$num_cheque     = $_POST['numero_cheque'];
$fecha          = $_POST['fecha_pago'];
$fecha_pago     = date("Y/m/d", strtotime($fecha));
$monto          = $_POST['monto'];
$referencia     = strtoupper($_POST['referencia']);
$motivo         = strtoupper($_POST['motivo']);
$negociable     = strtoupper($_POST['negociable']);


$proveedor = mysqli_query($conn, "SELECT TRIM(CONCAT(A.NOMBRE_EMPRESA,' ',A.NOMBRE_PROVEEDOR,' ',A.APELLIDO_PROVEEDOR))NOMBRE
                                    FROM tb_proveedor A,
                                         tb_factura_proveedor B
                                    WHERE A.ID_PROVEEDOR = B.ID_PROVEEDOR
                                    AND B.ID_FACTURA = '".$factura."'");

while($rowX = $proveedor->fetch_array(MYSQLI_ASSOC)){
    $nombre_provee = $rowX['NOMBRE'];
}                                    


$cheque = mysqli_query($conn, "SELECT MAX(ID_CHEQUE)+1 MAX_CHEQUE
                                FROM tb_cheque");

while($row = $cheque->fetch_array(MYSQLI_ASSOC)){
    $id_cheque = $row['MAX_CHEQUE'];

    if($id_cheque == 0){
    	$id_cheque = 1;
    }else{
    	$id_cheque = $row['MAX_CHEQUE'];
    }
}


?>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="lib_alert/css/sweetalert.css">
<script type="text/javascript" src="lib_alert/js/sweetalert-dev.js"></script>

<script>
function Ingresado()
    {
      swal({title:"Cheque grabada con Exito!", type:"success", showConfirmButton:false, text:"DATOS GRABADOS", timer:'2000'}, 
      function () 
    {
      location.href = "menu.php?id=40&ftc=<?= $id_cheque; ?>"; 
    });
    }

</script>
<script>
function Error()
    {
      swal({title:"No se pudo grabar.... Ocurrio algun Error!", type:"danger", showConfirmButton:false, text:"VERIFICAR DATOS", timer:'2000'}, 
      function () 
    {
      location.href = "menu.php?id=40"; 
    });
    }

</script>

<?php

$insert = mysqli_query($conn, "INSERT INTO tb_cheque (ID_CHEQUE, IDCUENTA, NUM_CHEQUE, FECHA_CHEQUE, MONTO_CHEQUE, PROVEEDOR, 
                                            USUARIO_GRABA, FECHA_CREO, REFERENCIA, MOTIVO, BENEFICIARIO, NEGOCIABLE, STATUS)
                               VALUES('".$id_cheque."','".$cuenta."','".$num_cheque."','".$fecha_pago."','".$monto."','".$nombre_provee."', 
                                      '".$usuario."', CURRENT_TIMESTAMP, '".$referencia."', '".$motivo."', '".$nombre_provee."', '".$negociable."','I')");

$verifica = mysqli_query($conn, "SELECT COUNT(*) CONTEO_CHEQUE
                                    FROM tb_cheque
                                    WHERE ID_CHEQUE = '".$id_cheque."'
                                     AND IDCUENTA   = '".$cuenta."'");

while($result = $verifica->fetch_array(MYSQLI_ASSOC)){
    $ver_cheque = $result['CONTEO_CHEQUE'];
}


if($ver_cheque == 1){
    echo "<script>Ingresado();</script>";
}else{
    echo "<script>Error();</script>";
}

?>