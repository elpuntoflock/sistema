<?php
$usuario = strtoupper($_SESSION['usuario']);
require_once('db/conexion.php');

$serie              = strtoupper($_POST['serie']);
$factura            = $_POST['factura'];
$dias_credito       = $_POST['dias'];
$fecha              = $_POST['fecha'];
$newDate            = date("Y/m/d", strtotime($fecha));
$total_encabezado   = $_POST['total'];
$tc                 = $_POST['tc'];
$te                 = $_POST['te'];
$ttc                = $_POST['ttc'];
$cliente            = $_POST['cliente'];
$nombre             = strtoupper($_POST['nombre']);
$direccion          = strtoupper($_POST['direccion']);
$nit                = strtoupper($_POST['nit']);
$observa_encabeza   = strtoupper($_POST['observa_encabeza']);
$tipo_documento     = strtoupper($_POST['tipo_documento']);
$tipo_pago          = $_POST['tipo_pago'];
$moneda             = $_POST['moneda'];
$saldo_caso         = $_POST['saldo_caso'];

if($tc == null){
    $tc = "";
}else{
    $tc = $_POST['tc'];
}

if($te == null){
    $te = "";
}else{
    $te = $_POST['te'];
}

if($ttc == null){
    $ttc = "";
}else{
    $ttc = $_POST['ttc'];
}

$abono_caso         = "ABONO NORMAL"."-".$observa_encabeza;

$caso   = count($_POST["caso"]);

/****** trae el correlativo del id factura ********/
$sql = mysqli_query($conn, "SELECT MAX(ID_FACTURA)+1 ID_FACTURA
                              FROM tb_factura_cliente");

while($row = $sql->fetch_array(MYSQLI_ASSOC)){
    $id_factura = $row['ID_FACTURA'];

    if($id_factura == 0){
    	$id_factura = 1;
    }else{
    	$id_factura = $row['ID_FACTURA'];
    }

}
/***** finaliza el correlativo de id factura *****/
if($tipo_documento == 'R'){
    $factura    = $id_factura;
}else{
    $factura    = $factura;
}

if($tipo_pago == 1){
    $estatus   = 'P';
}elseif($tipo_pago != 1){
    $estatus   = 'C';
}


?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="lib_alert/css/sweetalert.css">
    <script type="text/javascript" src="lib_alert/js/sweetalert-dev.js"></script>

<script>
function Ingresado()
    {
      swal({title:"Factura grabada con Exito...!!!", type:"success", showConfirmButton:false, text:"DATOS GRABADOS", timer:'2000'}, 
      function () 
    {
      location.href = "menu.php?id=24&num_fac=<?= $id_factura; ?>"; 
    });
    }
</script> 
	<title></title>
</head>
<body>
<?php

/**INSERT DEL ENCABEZADO */
$insert = mysqli_query($conn, "INSERT INTO tb_factura_cliente (
                                ID_FACTURA,
                                ID_CONTACTO,
                                SERIE,
                                FACTURA,
                                FECHA_EMISION,
                                NIT,
                                NOMBRE,
                                DIRECCION,
                                TOTAL,
                                CREDITO,
                                TCHEQUE,
                                TTC,
                                TEFECTIVO,
                                ESTATUS,
                                USUARIO_GRABA,
                                FECHA_GRABA,
                                OBSERVACIONES,
                                TIPO
                            )VALUES(
                                '".$id_factura."',
                                '".$cliente."',
                                '".$serie."',
                                '".$factura."',
                                '".$newDate."',
                                '".$nit."',
                                '".$nombre."',
                                '".$direccion."',
                                '".$total_encabezado."',
                                '".$dias_credito."',
                                '".$tc."',
                                '".$ttc."',
                                '".$te."',
                                '".$estatus."',
                                '".$usuario."',
                                CURRENT_TIMESTAMP,
                                '".$observa_encabeza."',
                                '".$tipo_documento."'
                            )");

        if($insert == TRUE){

            if($caso > 0) 
            { 
                 for($i=0; $i<$caso; $i++) 
                 { 
                      if($_POST["caso"][$i] != '') 
                      { 

                    /****** trae el correlativo del id factura ********/
					#Se cambia con ifnull para manejo de null
					
                    $detalle = mysqli_query($conn, "SELECT IFNULL(MAX(ID_DETALLE),0)+1 ID_DETALLE
                                                FROM tb_detalle_factura");

                    while($rest = $detalle->fetch_array(MYSQLI_ASSOC)){
                        $id_detalle = $rest['ID_DETALLE'];

                        if($id_detalle == 0){
                            $id_detalle = 1;
                        }else{
                            $id_detalle = $rest['ID_DETALLE'];
                        }

                    }
                    /***** finaliza el correlativo de id factura *****/
                        
                        $num_caso       = $_POST["caso"][$i];
                        $descripcion    = strtoupper($_POST["descripcion"][$i]);
                        $observaciones  = strtoupper($_POST["observaciones"][$i]);
                        $total          = $_POST["moneda"][$i];
                        $saldo_caso     = $_POST["saldo_caso"][$i];

						$sql_txt		= "INSERT INTO tb_detalle_factura (
                                                        ID_DETALLE,
                                                        ID_FACTURA,
                                                        ID_CASO,
                                                        DESCRIPCION,
                                                        OBSERVACIONES,
                                                        TOTAL,
                                                        USUARIO_GRABA,
                                                        FECHA_GRABA
                                                        )VALUES(
                                                            ".$id_detalle.",
                                                            '".$id_factura."',
                                                            '".$num_caso."',
                                                            '".$descripcion."',
                                                            '".$observaciones."',
                                                            '".$total."',
                                                            '".$usuario."',
                                                            CURRENT_TIMESTAMP
                                                        )";

                        $insert_detalle = mysqli_query($conn, $sql_txt);

                            if($insert_detalle == TRUE){

                                if($tipo_pago == 1){
                                    
                                    $abono = mysqli_query($conn, "SELECT MAX(ID_CA)+1 MAX_CARGO
                                                                    FROM tb_cargo_abono");
                        
                        while($ssx = $abono->fetch_array(MYSQLI_ASSOC)){
                        $id_ca = $ssx['MAX_CARGO'];
                        }
                        
                        $moneda = mysqli_query($conn, "SELECT MONEDA 
                                FROM tb_corriente
                                WHERE ID_CASO = '".$num_caso."'");
                                
							while($sdf = $moneda->fetch_array(MYSQLI_ASSOC)){
							$tipo_moneda = $sdf['MONEDA'];
							}                                
                        
                        $ins_corriente = mysqli_query($conn, "INSERT INTO tb_cargo_abono
                                                                (ID_CA,
                                                                ID_CASO,
                                                                ID_USUARIO,
                                                                DESCRIPCION,
                                                                MONTO,
                                                                MONEDA,
                                                                TIPO,
                                                                OBSERVACIONES,
                                                                FECHA
                                                                )VALUES(
                                                                '".$id_ca."',
                                                                '".$num_caso."',
                                                                '".$usuario."',
                                                                'ABONO NORMAL',
                                                                '".$total."',
                                                                '".$tipo_moneda."',
                                                                '2',
                                                                '".$abono_caso."',
                                                                CURRENT_TIMESTAMP
                                                                )");
                        
                        if($ins_corriente == TRUE){
                        
                            $nuevo_saldo = $saldo_caso - $total;
                        
                        $query_insert = mysqli_query($conn, "UPDATE tb_corriente 
                                                               SET SALDO = '".$nuevo_saldo."',
                                                                   FECHA_ACTUALIZA = CURRENT_TIMESTAMP
                                                            WHERE ID_CASO = '".$num_caso."'");
                        
                        if($query_insert == TRUE){
                        echo "<script>Ingresado();</script>";
                        }
                        
                        
                        
                        }else{
                        
                        }
                                }
                               echo "<script>Ingresado();</script>";
                            }else{
                                echo "Algun Error...";
                            }


                      } 
            
                 } 
            
            }else{ 
            
                 echo "Error"; 
            
            }


        }

        

/******* este array realizara el insert del detalle ********/


?>