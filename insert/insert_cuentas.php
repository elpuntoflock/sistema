<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="lib_alert/css/sweetalert.css">
    <script type="text/javascript" src="lib_alert/js/sweetalert-dev.js"></script>

<script>
function Ingresado()
    {
      swal({title:"Cuenta Bancaria grabada con Exito!", type:"success", showConfirmButton:false, text:"DATOS GRABADOS", timer:'2000'}, 
      function () 
    {
      location.href = "menu.php?id=31"; 
    });
    }

</script>
<script>
function Error()
    {
      swal({title:"No se pudo grabar.... Ocurrio algun Error!", type:"danger", showConfirmButton:false, text:"VERIFICAR DATOS", timer:'2000'}, 
      function () 
    {
      location.href = "menu.php?id=31"; 
    });
    }

</script>
	
	<title></title>
</head>
<body>
<?php
require_once 'db/conexion.php';

$banco      = $_POST['banco'];
$nombre     = strtoupper($_POST['nombre']);
$num_cuenta = $_POST['num_cuenta'];
$moneda     = strtoupper($_POST['moneda']);

$cuenta = mysqli_query($conn, "SELECT MAX(IDCUENTA)+1 CONTEO
                                 FROM tb_cuenta_banco");

while($row = $cuenta->fetch_array(MYSQLI_ASSOC)){

    $idcuenta = $row['CONTEO'];

    if($idcuenta == 0){
    	$idcuenta = 1;
    }else{
    	$idcuenta = $row['CONTEO'];
    }
}

$insert = mysqli_query($conn, "INSERT INTO tb_cuenta_banco (IDCUENTA, ID_BANCO, CUENTA, NUM_CUENTA, MONEDA, ESTATUS)
                               VALUES('".$idcuenta."','".$banco."','".$nombre."','".$num_cuenta."','".$moneda."','A')");

$verifica = mysqli_query($conn, "SELECT COUNT(*) VERIFICA
                                   FROM tb_cuenta_banco
                                  WHERE IDCUENTA = '".$idcuenta."'");

while($rest = $verifica->fetch_array(MYSQLI_ASSOC)){
    $ver_cuenta = $rest['VERIFICA'];
}

if($ver_cuenta == 1){
    echo "<script>Ingresado();</script>";
}else{
    echo "<script>Error();</script>";
}

?>