<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="lib_alert/css/sweetalert.css">
    <script type="text/javascript" src="lib_alert/js/sweetalert-dev.js"></script>
    <script>
        function Success()
        {
        swal({title:"Se ha creado la Carpeta Correctamente!!!....", type:"success", showConfirmButton:false, text:"USUARIO MODIFICADO", timer:'1000'}, 
        function () 
        {
        location.href = "menu.php?id=2"; 
        });
        }

        function Delete()
        {
        swal({title:"La Carpeta que se quiere crear ya Existe!!!....", type:"error", showConfirmButton:false, text:"FAVOR DE VERIFICAR", timer:'1000'}, 
        function () 
        {
        location.href = "menu.php?id=2"; 
        });
        }  

        function Archivo()
        {
        swal({title:"El Archivo se ha subido con Exito!!!....", type:"success", showConfirmButton:false, text:"FAVOR DE VERIFICAR", timer:'1000'}, 
        function () 
        {
        location.href = "menu.php?id=2"; 
        });
        }          

    </script>    

</head>
<body>
    
</body>
</html>
<?php
$usuario = strtoupper($_SESSION['usuario']);
require_once('db/conexion.php');

$area       = $_REQUEST['area'];
$caso       = $_REQUEST['caso'];


if(isset($_POST['carpeta'])){
    $nombre     = strtoupper($_POST['carpeta']);
}else{
    $nombre = "";
}



$raiz       = strtoupper($_REQUEST['raiz']);
$des_caso   = utf8_encode(strtoupper($_REQUEST['des_caso']));
$tipo       = $_REQUEST['tipo'];

/**************************** VERIFICAR SI LA CARPETA YA EXISTE POR NOMBRE ******************************/
$calcula = mysqli_query($conn, "SELECT COUNT(*) CONTEO
                                    FROM tb_carpetas
                                    WHERE id_area       = '".$area."'
                                    AND id_caso         = '".$caso."'
                                    AND nombre_carpeta  = '".$nombre."'");

    while($cal_num = $calcula->fetch_array(MYSQLI_ASSOC)){
        $valor = $cal_num['CONTEO'];
    }


    if($valor == 1){
        echo "<script>Delete();</script>";
    }else{
/**************************** FINALIZA LA VERIFICACION DE CARPETA ******************************/


/*************************** CREACION DE CARPETAS A NIVEL DE CASO  ******************************************/
        if($tipo == 'C'){

                    $cuenta = mysqli_query($conn, "SELECT COUNT(*)+1 VALOR
                                                    FROM tb_carpetas");

                        while($txt = $cuenta->fetch_array(MYSQLI_ASSOC)){
                                $id_carpeta = $txt['VALOR'];

                            if($id_carpeta == NULL){
                                    $id_carpeta = 1;
                                }else{
                                    $id_carpeta = $txt['VALOR'];
                                }

                        } 
                        /***************** INSERT DE CARPETA AL CASO ************************** */
                        $insert = mysqli_query($conn, "INSERT INTO tb_carpetas (id_carpeta, id_area, id_caso, nombre_carpeta, fecha_crea, usuario_graba)
                                                        VALUES ('".$id_carpeta."', '".$area."', '".$caso."', '".$nombre."', CURRENT_TIMESTAMP, '".$usuario."')");

                            if($insert == TRUE){
                                /************ SI REALIZA EL INSERT CREA A LA CARPETA ****************/
                                $carpeta = 'CASOS/'.$raiz.'/'.$des_caso.'/'.$nombre;

                                if (!file_exists($carpeta)) {
                                        mkdir($carpeta, 0777, true);
                                        echo "<script>Success();</script>";
                                    }else{
                                        echo "<script>Delete();</script>";
                                    }  
                                /* ******************* FINALIZA CREACION DE CARPETA ******************** */
                            }else{

                            }  

                }
    }

/*************************** FINALIZA LA CREACION DE CARPETAS A NIVEL DE CASO  ******************************************/

/********************* TIPO D ES PARA SUBIR DOCUMENTOS A NIVEL DEL CASO ************************************/
    if($tipo == 'D'){

        
        $archivo = (isset($_FILES['file'])) ? $_FILES['file'] : null;

        if ($archivo) {
            $nombre_file = "{$archivo['name']}";
         }

        $ruta_documento = 'CASOS/'.$raiz.'/'.$des_caso.'/'.$nombre_file;

        $val_documento = mysqli_query($conn, "SELECT COUNT(*)+1 NUM_DOC
                                                FROM tb_documento");
            
            while($ftc = $val_documento->fetch_array(MYSQLI_ASSOC)){

                $id_documento = $ftc['NUM_DOC'];

                if($id_documento == NULL){
                        $id_documento = 1;
                    }else{
                        $id_documento = $ftc['NUM_DOC'];
                }

            }

            /********************** INSERT DE DOCUMENTO AL CASO ******************************* */
            $insert_document = mysqli_query($conn, "INSERT INTO tb_documento (id_document, id_area, tb__casos, id_caso, id_carpeta, descripcion, ruta, fecha_crea, usuario_graba)
                                                        VALUES('".$id_documento."', '".$area."', '".$area."', '".$caso."', '0','".$nombre_file."', '".$ruta_documento."', CURRENT_TIMESTAMP, '".$usuario."')");


            if($insert_document == TRUE){
                /************ SI REALIZA EL INSERT SUBE EL ARCHIVO A LA CARPETA ****************/
                $ruta_archivo = 'CASOS/'.$raiz.'/'.$des_caso.'/';
                $archivo = (isset($_FILES['file'])) ? $_FILES['file'] : null;

                if ($archivo) {
                   $ruta_destino_archivo = $ruta_archivo.'/'."{$archivo['name']}";
                   $archivo_ok = move_uploaded_file($archivo['tmp_name'], $ruta_destino_archivo);
                   echo "<script>Archivo();</script>";
                }
                /* ******************* FINALIZA LA SUBIDA DEL ARCHIVO ******************** */
            }else{
                echo "Error";
            }


    }else{

    }
/********************* FINALIZA LA SUBIDA DE ARCHIVOS A NIVEL CASO ************************************/

/*************************************** SUBE ARCHIVOS A NIVEL DOS *********************************** */

if($tipo == 'V'){

    $archivo_dos = (isset($_FILES['file_dos'])) ? $_FILES['file_dos'] : null;
    $des_carpeta = $_REQUEST['carpeta'];
    $num_carpeta = $_REQUEST['num'];

    if ($archivo_dos) {
        $nombre_dos = "{$archivo_dos['name']}";
     }

     $val_doc = mysqli_query($conn, "SELECT COUNT(*)+1 VAL_DOCU
                                       FROM tb_documento");

        while($fcc = $val_doc->fetch_array(MYSQLI_ASSOC)){
            $id_document = $fcc['VAL_DOCU'];

            if($id_document == NULL){
                $id_document = 1;
            }else{
                $id_document = $fcc['VAL_DOCU'];
            }

        }

        $ruta_carpeta = 'CASOS/'.$raiz.'/'.$des_caso.'/'.$des_carpeta.'/'.$nombre_dos;

        $inserta_doc = mysqli_query($conn, "INSERT INTO tb_documento (id_document, id_area, id_caso, id_carpeta, descripcion, ruta, fecha_crea, usuario_graba)
                                            VALUE('".$id_document."', '".$area."', '".$caso."', '".$num_carpeta."', '".$nombre_dos."', '".$ruta_carpeta."', CURRENT_TIMESTAMP, '".$usuario."')");

            if($inserta_doc == TRUE){

                /************ SI REALIZA EL INSERT SUBE EL ARCHIVO A LA CARPETA ****************/
                $ruta_final = 'CASOS/'.$raiz.'/'.$des_caso.'/'.$des_carpeta.'/';
                $archivo_final = (isset($_FILES['file_dos'])) ? $_FILES['file_dos'] : null;

                if ($archivo_final) {
                    $ruta_destino = $ruta_final.'/'."{$archivo_final['name']}";
                    $archivo_ok = move_uploaded_file($archivo_final['tmp_name'], $ruta_destino);
                    echo "<script>Archivo();</script>";
                }
                /* ******************* FINALIZA LA SUBIDA DEL ARCHIVO ******************** */



            }else{

            }                      

}else{

}

?>
