<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="lib_alert/css/sweetalert.css">
    <script type="text/javascript" src="lib_alert/js/sweetalert-dev.js"></script>

<script>
function Ingresado()
    {
      swal({title:"Caso Grabado con Exito!", type:"success", showConfirmButton:false, text:"DATOS GRABADOS", timer:'2000'}, 
      function () 
    {
      //CMOTTA se modifica para que regrese a la misma pantalla location.href = "menu.php?id=2"; 
	location.href = "menu.php?id=8"; 

    });
    }

</script>	
	<title></title>
</head>
<body>
<?php
$usuario_crea 	= $_SESSION['usuario'];
$usuario_crea 	= strtoupper($usuario_crea);
require_once('db/conexion.php');



$id_contacto	= $_POST['id_contacto'];
$descripcion	= strtoupper($_POST['descripcion']);
$newDate		= $_POST['fec_inicio'];
$fecha_inicio 	= date("Y/m/d", strtotime($newDate));	

$newDate_2		= $_POST['fec_final'];
$fecha_final 	= date("Y/m/d", strtotime($newDate_2));
	
$observacion	= strtoupper($_POST['text']);

$ruta = $_POST['ruta'];

//Estos son nuevos campos solicitados//
$causa 		= strtoupper($_POST['causa']);
$num_mp		= strtoupper($_POST['mun_mp']);
$fiscalia	= strtoupper($_POST['fiscalia']);
$juzgado	= strtoupper($_POST['juzgado']);
$juez 		= strtoupper($_POST['juez']);
$oficial	= strtoupper($_POST['oficial']);
$dir_juz	= strtoupper($_POST['direccion_juzgado']);
/*$zona_juz	= strtoupper($_POST['zona_juz']);*/
$tel_juz	= strtoupper($_POST['tel_juzgado']);
$caso_origen	= $_POST['origen'];
$abogado	= strtoupper($_POST['abogado']);
//Finalizan los nuevos campos//
$archivo = (isset($_FILES['file'])) ? $_FILES['file'] : null;
if ($archivo) {
	$nombre_file = "{$archivo['name']}";
 }


function quitar_tildes($cadena) {
	$no_permitidas= array ("á","é","í","ó","ú","Á","É","Í","Ó","Ú","ñ","À","Ã","Ì","Ò","Ù","Ã™","Ã ","Ã¨","Ã¬","Ã²","Ã¹","ç","Ç","Ã¢","ê","Ã®","Ã´","Ã»","Ã‚","ÃŠ","ÃŽ","Ã”","Ã›","ü","Ã¶","Ã–","Ã¯","Ã¤","«","Ò","Ã","Ã„","Ã‹");
	$permitidas= array ("a","e","i","o","u","A","E","I","O","U","n","N","A","E","I","O","U","a","e","i","o","u","c","C","a","e","i","o","u","A","E","I","O","U","u","o","O","i","a","e","U","I","A","E","�","�","�","�","�","�","�","�","�","�");
	$texto = str_replace($no_permitidas, $permitidas ,$cadena);
	return $texto;
	}

	$detalle = strtoupper(quitar_tildes($descripcion));

$sql = mysqli_query($conn, "SELECT MAX(ID_CASO)+1 CONTEO
							FROM tb_caso");
while($row = $sql->fetch_array(MYSQLI_ASSOC)){
    $id_caso = $row['CONTEO'];

    if($id_caso == 0){
    	$id_caso = 1;
    }else{
    	$id_caso = $row['CONTEO'];
    }
        
}

/********************** INSERT VARIOS ***************************/

$insert = mysqli_query($conn, "INSERT INTO tb_caso (
								ID_CASO,
								ID_CONTACTO,
								CASO_ORIGEN,
								DESCRIPCION,
								FECHA_INI,
								FECHA_FIN,
								USUARIO_CREA,
								FECHA_CREA,
								OBSERVACIONES,
								CAUSA,
								NUMERO_MP,
								FISCALIA,
								JUZGADO,
								JUEZ,
								OFICIAL,
								DIRECCION_JUZGADO,
								TELEFONO_JUZGADO,
								ABOGA_RESPONSABLE,
								ESTATUS)VALUES(
								'".$id_caso."',
								'".$id_contacto."',
								'".$caso_origen."',
								'".utf8_decode($descripcion)."',
								'".$fecha_inicio."',
								'".$fecha_final."',
								'".$usuario_crea."',
								CURRENT_TIMESTAMP,
								'".utf8_decode($observacion)."',
								'".$causa."',
								'".$num_mp."',
								'".$fiscalia."',
								'".strtoupper(utf8_decode($juzgado))."',
								'".$juez."',
								'".$oficial."',
								'".$dir_juz."',
								'".$tel_juz."',
								'".$abogado."',
								'A')
								");

		if($insert == TRUE){

			/************** INSERTA CASO A TB_ARCHIVOS *************/
			$nvl = mysqli_query($conn, "SELECT count(*)+1 NVL_CONTEO 
										FROM tb_archivos");

				while($fpt = $nvl->fetch_array(MYSQLI_ASSOC)){
					$id_area = $fpt['NVL_CONTEO'];

					if($id_area == NULL){
						$id_area = 1;
					}else{
						$id_area = $fpt['NVL_CONTEO'];
					}

				}

				$raiz = mysqli_query($conn, "SELECT descripcion
												FROM tb_archivos
												WHERE id_area = '".$ruta."'");
					
					while($deftc = $raiz->fetch_array(MYSQLI_ASSOC)){
						$nombre = $deftc['descripcion'];
					}

				

				$insert_area = mysqli_query($conn, "INSERT INTO tb_archivos (id_area, tb__casos, id_caso, description, fecha_graba, usuario_graba)
													VALUES('".$id_area."', '".$ruta."', '".$id_caso."', '".$detalle."', CURRENT_TIMESTAMP, '".$usuario_crea."')");

				if($insert_area == TRUE){
					
					$carpeta = 'CASOS/'.$nombre.'/'.$detalle;

					if (!file_exists($carpeta)) {
						mkdir($carpeta, 0777, true);
					}else{
						
					}

					$max_document = mysqli_query($conn, "SELECT COUNT(*)+1 NVL_DOC
															FROM tb_documento");
						
						while($dfc = $max_document->fetch_array(MYSQLI_ASSOC)){
							$id_doc = $dfc['NVL_DOC'];

							if($id_doc == NULL){
								$id_doc = 1;
							}else{
								$id_doc = $dfc['NVL_DOC'];
							}

						}

						$ruta_file = 'CASOS/'.$nombre.'/'.$detalle.'/'.$nombre_file;

						if($nombre_file != null){

						$insrt_doc = mysqli_query($conn, "INSERT INTO tb_documento (id_document, id_area, tb__casos, id_caso, descripcion, ruta, fecha_crea, usuario_graba)
															VALUES('".$id_doc."', '".$ruta."', '".$ruta."', '".$id_caso."', '".$nombre_file."', '".$ruta_file."', CURRENT_TIMESTAMP, '".$usuario_crea."')");

						if($insrt_doc == TRUE){

														

							$archivo = (isset($_FILES['file'])) ? $_FILES['file'] : null;
							$ruta_dos = 'CASOS/'.$nombre.'/'.$detalle;
							if ($archivo) {
								$ruta_destino_archivo = $ruta_dos.'/'."{$archivo['name']}";
								$archivo_ok = move_uploaded_file($archivo['tmp_name'], $ruta_destino_archivo);
								}


						}else{

						}															

						}



				}else{

				}													


			/******************************************************/

			$sql = mysqli_query($conn, "SELECT MAX(ID_ACCESO)+1 CONTEO
									FROM tb_acceso");

		while($row = $sql->fetch_array(MYSQLI_ASSOC)){
			$id_acceso = $row['CONTEO'];

			if($id_acceso == 0){
				$id_acceso = 1;
			}else{
				$id_acceso = $row['CONTEO'];
			}
		}

		$cuenta = mysqli_query($conn, "SELECT COUNT(*)CUENTA
										FROM tb_acceso
										WHERE ID_USUARIO = '".$usuario_crea."'
										AND ID_CASO = '".$id_caso."'");

		while($linea = $cuenta->fetch_array(MYSQLI_ASSOC)){
			$conteo = $linea['CUENTA'];
		}

		if($conteo == 0){

			$insert_acceso = mysqli_query($conn, "INSERT INTO tb_acceso (
											ID_ACCESO,
											ID_USUARIO,
											ID_CASO,
											FECHA_CREA)VALUES(
											'".$id_acceso."',
											'".$usuario_crea."',
											'".$id_caso."',
											CURRENT_TIMESTAMP)
											");
			
			if($insert_acceso == TRUE){
				echo "<script>Ingresado();</script>";
			}

		}else{
			//echo "<script>Error();</script>";
			//echo "Error: " . $insert . "<br>" . $conn->error;
		}

	}else{
		
	}
?>
</body>
</html>