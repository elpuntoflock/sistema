<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="lib_alert/css/sweetalert.css">
    <script type="text/javascript" src="lib_alert/js/sweetalert-dev.js"></script>

<script>

function Ingresado()
    {
      swal({title:"Datos Grabados con Exito!", type:"success", showConfirmButton:false, text:"DATOS GRABADOS", timer:'2000'}, 
      function () 
    {
      location.href = "menu.php?id=3"; 
    });
    }
</script> 
	<title></title>
</head>
<body>
<?php
require_once('db/conexion.php');

$nombre 		= $_SESSION['usuario'];
$usuario 		= strtoupper($nombre);

$nombres		= strtoupper($_POST['nombres']);
$apellidos		= strtoupper($_POST['apellidos']);
$cui 			= $_POST['cui'];
$pasaporte 		= $_POST['pasaporte'];
$sexo 			= strtoupper($_POST['sexo']);
$direccion		= strtoupper($_POST['direccion']);
$zona			= $_POST['zona'];
$pais			= $_POST['pais'];
$departamento	= $_POST['departamento'];
$municipio		= $_POST['municipio'];
$telefono		= $_POST['telefono'];
$ape_cadasa 	= strtoupper($_POST['apellido_casada']);
$email 			= strtolower($_POST['email']);
$nit 			= strtoupper($_POST['nit']);

/**** DATOS COMPLEMENTARIOS ****/
$nombre_empresa	= strtoupper($_POST['nombre_empresa']);
$cargo_empresa	= strtoupper($_POST['cargo_empresa']);
$profesion		= strtoupper($_POST['profesion']);
$nombre_refe		= strtoupper($_POST['nombre_ref']);
$relacion_ref	= strtoupper($_POST['relacion_ref']);
$telefono_ref	= $_POST['telefono_ref'];
$celular_ref	= $_POST['celular_ref'];
$observaciones	= strtoupper($_POST['observaciones']);


$sql = mysqli_query($conn, "SELECT MAX(ID_CONTACTO)+1 CONTEO
							FROM tb_contacto");
while($row = $sql->fetch_array(MYSQLI_ASSOC)){
    $id_contacto = $row['CONTEO'];

    if($id_contacto == 0){
    	$id_contacto = 1;
    }else{
    	$id_contacto = $row['CONTEO'];
    }

}

$insert = mysqli_query($conn, "INSERT INTO tb_contacto (
			ID_CONTACTO,
			ID_USUARIO,
			NOMBRES,
			APELLIDOS,
			APELLIDO_CASADA,
			SEXO,
			DIRECCION,
			ZONA,
			DEPARTAMENTO,
			MUNICIPIO,
			PAIS,
			CUI,
			PASAPORTE,
			FECHA_CREA,
			TELEFONO,
			EMAIL,
			NIT,
			NOMBRE_EMPRESA,
			CARGO_EMPRESA,
			PROFESION,
			NOMBRE_REFERENCIA,
			RELACION_REFERENCIA,
			TELEFONO_REFERENCIA,
			CELULAR_REFERENCIA,
			OBSERVACIONES)
			VALUES(
			'".$id_contacto."',
			'".$usuario."',
			'".$nombres."',
			'".$apellidos."',
			'".$ape_cadasa."',
			'".$sexo."',
			'".$direccion."',
			'".$zona."',
			'".$departamento."',
			'".$municipio."',
			'".$pais."',
			'".$cui."',
			'".$pasaporte."',
			CURRENT_TIMESTAMP,
			'".$telefono."',
			'".$email."',
			'".$nit."',
			'".$nombre_empresa."',
			'".$cargo_empresa."',
			'".$profesion."',
			'".$nombre_refe."',
			'".$relacion_ref."',
			'".$telefono_ref."',
			'".$celular_ref."',
			'".$observaciones."')");

if($insert == TRUE){
	echo "<script>Ingresado();</script>";
}else{
	echo "<script>Error();</script>";
}

$conn->close();			

?>
</body>
</html>