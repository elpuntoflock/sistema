
<?php
$usuario = strtoupper($_SESSION['usuario']);
require_once 'db/conexion.php';

$proveedor          = $_POST['proveedor'];
$tipo_documento     = $_POST['tipo_documento'];
$tipo_pago          = $_POST['tipo_pago'];
$nombre             = strtoupper($_POST['nombre']);
$serie              = strtoupper($_POST['serie']);
$factura            = $_POST['factura'];
$dias               = $_POST['dias'];
$fecha              = $_POST['fecha'];
$date            	= date("Y/m/d", strtotime($fecha));
$observaciones      = strtoupper($_POST['descripcion']);
$monto              = $_POST['monto'];


if($tipo_pago == 2){
    $estatus = 'C';
}else{
    $estatus = 'P';
}


$Dfactura = mysqli_query($conn, "SELECT MAX(id_factura)+1 CONTEO
                                    FROM tb_factura_proveedor");

while($row = $Dfactura->fetch_array(MYSQLI_ASSOC)){
    $id_factura = $row['CONTEO'];

    if($id_factura == 0){
    	$id_factura = 1;
    }else{
    	$id_factura = $row['CONTEO'];
    }

}


if($tipo_documento == 'R'){
    $recibo = $id_factura;
}else{
    $recibo = 0;
}

$datos = mysqli_query($conn, "SELECT CONCAT(DIRECCION,' ',ZONA)LUGAR, NIT
                                FROM tb_proveedor
                                WHERE id_proveedor = '".$proveedor."'");

while($rest = $datos->fetch_array(MYSQLI_ASSOC)){
    $direccion = $rest['LUGAR'];
    $nit       = $rest['NIT'];
}                             

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="lib_alert/css/sweetalert.css">
    <script type="text/javascript" src="lib_alert/js/sweetalert-dev.js"></script>

<script>
function Ingresado()
    {
      swal({title:"Factura Proveedor grabada con Exito...!!!", type:"success", showConfirmButton:false, text:"DATOS GRABADOS", timer:'2000'}, 
      function () 
    {
      location.href = "menu.php?id=37"; 
    });
    }
    function Error()
    {
      swal({title:"Ha ocurrido error al insertar el Registro...!!!", type:"danger", showConfirmButton:false, text:"VERIFICAR DATOS", timer:'2000'}, 
      function () 
    {
      location.href = "menu.php?id=37"; 
    });
    }    
</script> 
	<title></title>
</head>
<body>


<?php

$sql = mysqli_query($conn, "INSERT INTO tb_factura_proveedor (ID_FACTURA, 
                                                              ID_USUARIO, 
                                                              tb__ID_USUARIO, 
                                                              ID_PROVEEDOR, 
                                                              SERIE, 
                                                              FACTURA, 
                                                              FECHA_EMISION, 
                                                              NIT, 
                                                              RECIBO,
                                                              NOMBRE, 
                                                              DIRECCION, 
                                                              TOTAL, 
                                                              CREDITO, 
                                                              ESTATUS, 
                                                              OBSERVACIONES, 
                                                              FECHA_GRABA, 
                                                              TIPO) VALUES 
                                                              ('".$id_factura."', 
                                                              '".$usuario."', 
                                                              '".$usuario."', 
                                                              '".$proveedor."', 
                                                              '".$serie."', 
                                                              '".$factura."', 
                                                              '".$date."',
                                                              '".$nit."',
                                                              '".$recibo."', 
                                                              '".$nombre."', 
                                                              '".$direccion."', 
                                                              '".$monto."', 
                                                              '".$dias."', 
                                                              '".$estatus."', 
                                                              '".$observaciones."',
                                                              CURRENT_TIMESTAMP, 
                                                              '".$tipo_documento."')");



if($sql == TRUE){
    echo "<script>Ingresado();</script>";
}else{
    echo "<script>Error();</script>";
}

?>