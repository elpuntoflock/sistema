<?php
require_once('lib/pdf/mpdf.php');
/*Conexion a la Base de Datos*/
require_once('db/conexion.php');

$num_caso = $_REQUEST['tmp'];
$fecha = date('d/m/Y');


/*Encabezado del Reporte*/
$encabezado = mysqli_query($conn, "SELECT DATE_FORMAT(FECHA,'%d/%m/%Y')FEC, MONTO, MONEDA
                  									FROM tb_cargo_abono
                  									WHERE TIPO = 6
                  									  AND ID_CASO = '".$num_caso."'");

while($rowA1 = $encabezado->fetch_array(MYSQLI_ASSOC)){
	$monto = number_format($rowA1['MONTO'],2,'.',',');
  $fecha = $rowA1['FEC'];
  $moneda1 = $rowA1['MONEDA'];
}

$sql1 = mysqli_query($conn, "SELECT SALDO, MONEDA
                            FROM tb_corriente
                            WHERE ID_CASO = '".$num_caso."'");

while($rowAA = $sql1->fetch_array(MYSQLI_ASSOC)){
  $saldo = $rowAA['SALDO'];
  $moneda_fin = $rowAA['MONEDA'];
}

if (isset($saldo) <= 0){
	$saldo = "0.00";
}else{
	$saldo = number_format($saldo,2,'.',',');
}

/*Cuerpo del Reporte*/
$detalle = mysqli_query($conn, "SELECT DATE_FORMAT(FECHA,'%d/%m/%Y')FEC, DESCRIPCION, MONTO, MONEDA, TIPO
                                  FROM tb_cargo_abono
                                  WHERE TIPO <> 6
                                    AND ID_CASO = '".$num_caso."'
                                  ORDER BY FECHA ASC");

    while ($row = mysqli_fetch_array($detalle)){
      $tipo = $row[4];
      $moneda = number_format($row[2],2,'.',',');

      if(($tipo == 2) or ($tipo == 4) or ($tipo == 5)){
        $valor = "<td style='text-align: right; color: blue; font-weight: bold;'>$row[3] $moneda</td>";
      }elseif (($tipo == 1) or ($tipo == 3)) {
        $valor = "<td style='text-align: right; color: red; font-weight: bold;' >$row[3] $moneda</td>";
      }

    $loop = $loop .'
    
    <tr>
    <td>'.$row[0].'</td>
    <td>'.$row[1].'</td>
    '.$valor.'
    </tr>
    ';
    
    }  
    
    
    $datos = mysqli_query($conn, "SELECT a.CAUSA, a.ID_CONTACTO, CONCAT(b.NOMBRES,' ',b.APELLIDOS)NOMBRES, c.MONTO, c.MONEDA
                                    from tb_caso a,
                                        tb_contacto b,
                                        tb_cargo_abono c
                                    where a.id_contacto = b.id_contacto
                                      and a.id_caso 	= c.id_caso
                                      and c.tipo = 6
                                      and a.id_caso = '".$num_caso."'");

while($resdat = $datos->fetch_array(MYSQLI_ASSOC)){

  $causa        = $resdat['CAUSA'];
  $contacto     = $resdat['ID_CONTACTO'];
  $nombre       = $resdat['NOMBRES'];
  $mon_inicial  = $resdat['MONTO'];
  $val_inicial  = number_format($mon_inicial,2,'.',',');
  $moneda       = $resdat['MONEDA'];

}   


    $html = "<header class='clearfix'>

    <h1>REPORTE POR CAUSA</h1>
    <br>
    <br>
    <div style='text-align: right;'>Fecha de Impresi&oacute;n: $fecha</div>
    <br>
    <br>
    <div id='logo'>
        <img src='img/logo/Law.png' style='width: 150px;'>
    </div>
    <br>
    <br>
    <br>
    <div>
      <ul style='font-weight: bold;'>
        <li>CAUSA:<span> $causa</span></li>
        <li>Cliente:<span> $contacto $nombre</span></li>
        <li>Monto Inicial:<span> $moneda $val_inicial</span></li>
      </ul>
    </div>
    
  </header>
   <main>
            
   <table>
      <thead>
        <tr style='background-color: #005691;'>
          <th class='service' style='color: #fff;'>FECHA</th>
          <th class='desc' style='text-align: center; color: #fff;'>DESCRIPCION</th>
          <th style='color: #fff;'>MONTO</th>
          </tr>
      </thead>
      <tbody>
        <tr>
          <td class='service' style='font-weight: bold; color: #000 !important;'>$fecha</td>
          <td class='desc' style='text-align: center; font-weight: bold; color: #000 !important;'>CARGO INICIAL DE CASO:</td>
          <td class='unit' style='font-weight: bold; color: #000 !important;'>$moneda1 $monto</td>
        </tr>
      </tbody>
    </table>

    <br>
    <br>
          
    <table>
      <thead>
        <tr style='background-color: #005691; font-size: 1em;'>
          <th class='service' style='color: #fff;'>FECHA</th>
          <th style='color: #fff;'>DETALLE</th>
          <th style='width: 150px; color: #fff;'>MONTO</th>
        </tr>
      </thead>
      <tbody>
          $loop;
          <tr style='background-color: #005691;'>
          <td style='color: #fff; font-weight: bold;'>FECHA: $fecha</td>
          <td style='color: #fff; font-weight: bold;'>SALDO TOTAL:</td>
          <td style='color: #fff; font-weight: bold; text-align: right;'>$moneda_fin&nbsp;&nbsp;$saldo</td>          
          </tr>
      </tbody>
    </table>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <div style='text-align:center;'>Firma:___________________________________</div>
    <div style='text-align:center;'>Lic. Victor P&eacute;rez</div>
  </main>";

  $mpdf = new mPDF('c','A4');
  $css = file_get_contents('lib/reportes/css/style.css');
  $mpdf->writeHTML($css,1);
  $mpdf->WriteHTML(utf8_encode($html));
  //$mpdf->writeHTML($html);
  $mpdf->Output('cargo abono.pdf','I');

?>