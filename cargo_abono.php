<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="alerta/css/sweetalert.css">
<script type="text/javascript" src="alerta/js/sweetalert-dev.js"></script>

<script>
	function ErrorAcceso()
		{
		swal({title:"Su usuario no tiene privilegios para esta pantalla..!", type:"error", showConfirmButton:false, text:"COMUNIQUESE CON EL ADMINISTRADOR", timer:'900'}, 
		function () 
		{
		location.href = "menu.php?id=22"; 
		});
		}
</script>
<?php
$usuario  = $_SESSION['usuario'];
$usuario  = strtoupper($usuario);

require_once('db/conexion.php');

$sql_permiso = mysqli_query($conn, "SELECT COUNT(*)CUENTA
										FROM tb_acceso_item
										WHERE id_usuario = '".$usuario."'
										  AND ITEM = ".$_REQUEST['id']."");
								
	while($valida = $sql_permiso->fetch_array(MYSQLI_ASSOC)){

		$resultado = $valida['CUENTA'];
	}

	if($resultado == 1){
		
	}else{
		echo "<script>ErrorAcceso();</script>";
	}

$sql = mysqli_query($conn, "SELECT A.ID_CASO, A.DESCRIPCION, A.CAUSA
                            FROM tb_caso A,
                                 tb_acceso B
                            WHERE A.ID_CASO = B.ID_CASO
                              AND B.ID_USUARIO = '".$usuario."'");

if(isset($_POST['caso']))
    {
      $caso = $_POST['caso'];  
    }
    else{
      $caso = '';        
    }


$detalle = mysqli_query($conn, "SELECT DATE_FORMAT(FECHA,'%d/%m/%Y')FEC, DESCRIPCION, MONTO, TIPO, MONEDA
                                  FROM tb_cargo_abono
                                  WHERE TIPO <> 6
                                    AND ID_CASO = '".$caso."'
                                  ORDER BY FECHA ASC");

$sql1 = mysqli_query($conn, "SELECT SALDO
              								FROM tb_corriente
              							  WHERE ID_CASO = '".$caso."'");

while($rowAA = $sql1->fetch_array(MYSQLI_ASSOC)){
	$saldo = $rowAA['SALDO'];
}

if (isset($saldo) <= 0){
	$saldo = "0.00";
}else{
	$saldo = number_format($saldo,2,'.',',');
}

$encabezado = mysqli_query($conn, "SELECT DATE_FORMAT(FECHA,'%d/%m/%Y')FEC, MONTO, MONEDA
                  									FROM tb_cargo_abono
                  									WHERE TIPO = 6
                  									  AND ID_CASO = '".$caso."'");

while($rowA1 = $encabezado->fetch_array(MYSQLI_ASSOC)){
	$monto = number_format($rowA1['MONTO'],2,'.',',');
  $fecha = $rowA1['FEC'];
  $moneda = $rowA1['MONEDA'];
}

if(isset($monto) == null){
  $moneda = " ";
}else{
  $moneda = $moneda;
}


if (isset($monto) <= 0){
	$monto = "0.00";
}else{
	$monto = $monto;
}

if (isset($fecha) == null){
	$fecha = "";
}else{
	$fecha = $fecha;
}

?>

  <script type="text/javascript">

function selectItemByValue(elmnt, value){

///alert('elmnt: '+elmnt.name +' val: '+ value);

for(var i=0; i < elmnt.options.length; i++)
  {
    if(elmnt.options[i].value == value)
      elmnt.selectedIndex = i;
  }
}

</script>

<div class="">
    <div class="row">
        <div class="col-md-12">
            <div class="wrapper-logo-secondary">
                <img src="img/logo/Law.png" alt="Logotipo Firma Law">
            </div>
        </div>
    </div>
</div>

<div class="">

      <div class="row">
          <div class="top-line">
              <div class="col-md-4" data-line="movil"><div class="line" style="margin-top: 25px;"></div></div>
              <div class="col-md-4 titulo-seccion"><p>CARGO / ABONO</p></div>
              <div class="col-md-4"><div class="line" style="margin-top: 25px;"></div></div>
          </div>
      </div>

<div class="col-md-12 bajar"> 
  <button type="button" class="boton3" data-toggle="modal" data-target="#myModal">REALIZAR CARGO / ABONO</button>
</div>

  <div class="modal fade" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
      <div class="modal-header">
          <h4 class="modal-title">ABONOS DE CLIENTES</h4>
          <button type="button" class="close" data-dismiss="modal">X</button>
        </div>
        
        <div class="modal-body">
          <form action="menu.php?id=6" method="post">
            <div>
              <label>SELECCIONAR NUMERO DE CAUSA</label>
              <select name="id_caso" class="form-control abono" id="id_caso">
                <option value="-1">SELECCIONAR CAUSA</option>
            <?php
            while ($row = mysqli_fetch_array($sql))
            {
              echo '<option value="' . $row['ID_CASO']. '">' . utf8_encode($row['CAUSA']) . '</option>' . "\n";
            }
            ?>                  
              </select>
            </div>
            <div>
              <label>DESCRIPCI&Oacute;N</label>
              <input type="text" name="desc_abono" class="form-control abono upper" placeholder="Ingresar Descripci&oacute;n">
            </div>
            <div>
              <label>MONTO</label>
              <input type="text"  name="monto" class="form-control abono upper" placeholder="Ingresar el Monto Abonar" >
            </div>
            <div>
              <label>TIPO DE ABONO</label>
              <select name="tipo" class="form-control abono">
                <option value="6">CARGO INICIAL</option>
                <option value="1">CARGO NORMAL</option>
                <option value="2">ABONO NORMAL</option>
                <option value="3">CHEQUE RECHAZADO</option>
                <option value="4">NOTA DE CREDITO</option>
                <option value="5">NOTA DE DEBITO</option>
              </select>
            </div>
            <div>
              <label>TIPO DE MONEDA</label>
              <select name="moneda" class="form-control upper">
                <option value="Q">QUETZALES</option>
                <option value="$">DOLARES</option>
              </select>
            </div>
            <div>
              <label>OBSERVACIONES</label>
              <input type="text" name="obs_abono" class="form-control abono upper" placeholder="Ingresar Observaci&oacute;n">
            </div>
            <div class="boton-formulario bajar">
              <button type="submit" class="boton3">GRABAR</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

    <form action="menu.php?id=4" method="post">  
  <div class="row">
        <div class="top-line" style="margin-top: 25px !important; margin-bottom: 30px;">
            <div class="col-md-4" data-line="movil"><div class="line" style="margin-top: 25px !important;"></div></div>
            <div class="col-md-4 titulo-seccion" style="margin-top: 15px !important;"><p>BUSQUEDA CASOS</p></div>
            <div class="col-md-4"><div class="line" style="margin-top: 25px !important;"></div></div>
        </div>     
  </div>
  <div class=" bajar">
    <div class="row">
      <div class="col-md-6">
        <label>SELECCIONAR CAUSA</label>
        <select name="caso" class="form-control upper" id='caso' placeholder="SELECCIONAR CASO">
          <option value="-1">SELECCIONAR CAUSA</option>
            <?php
            $sqlX = mysqli_query($conn, "SELECT A.ID_CASO, A.DESCRIPCION, A.CAUSA
                                          FROM tb_caso A,
                                             tb_acceso B
                                          WHERE A.ID_CASO = B.ID_CASO
                                            AND B.ID_USUARIO = '".$usuario."'");   
                                                     
            while ($rowx = mysqli_fetch_array($sqlX))
            {
              echo '<option value="' . $rowx['ID_CASO']. '">' . utf8_encode($rowx['CAUSA']) . '</option>' . "\n";
            }
            ?>           
        </select>
          <script language="javascript">
							var numberMI = document.getElementById("caso");
							selectItemByValue(numberMI,<?= "'".$caso."'"?>);
					</script>	
      </div>
      <div class="col-md-1" style="margin-top: 36px;">
            <div class="boton-formulario">
              <button type="submit" class="boton3">BUSCAR</button>
            </div>        
      </div>
    </div>
  </div>


      <div class=" bajar">
        <div class="row">
        <div class="top-line" style="margin-top: 25px !important; margin-bottom: 30px;">
            <div class="col-md-4" data-line="movil"><div class="line" style="margin-top: 25px !important;"></div></div>
            <div class="col-md-4 titulo-seccion" style="margin-top: 15px !important;"><p>DETALLE</p></div>
            <div class="col-md-4"><div class="line" style="margin-top: 25px !important;"></div></div>
        </div>

      <div class="col-md-12">
        <div class="centrar">
          <button type="button" class="boton6"><a href="rep_cuenta_cor_pdf.php?tmp=<?php echo $caso; ?>" target="_blank">Reporte PDF</a></button>   
        </div>
      </div>         
       
    <div class="col-md-12 table-responsive bajar">

    	<table class="display nowrap table table-striped table-bordered bajar" style="width:100%;">
    		<thead>
    			<tr>
    				<td>FECHA</td>
    				<td>DESCRIPCI&Oacute;N</td>
    				<td>MONTO</td>
    			</tr>
    		</thead>
    		<tbody>
          	<tr style="background-color: #005691;">
        		<td style="color: #fff; font-weight: bold;"><?php echo $fecha; ?></td>
        		<td style="color: #fff; font-weight: bold;">CARGO INICIAL DE CASO:</td>
        		<td style="color: #fff; font-weight: bold;"><?php echo $moneda; ?>&nbsp;&nbsp;<?php echo $monto; ?></td>
        	</tr>     			
    		</tbody>
    	</table>

      <table id="example" class="display nowrap table table-striped table-bordered" style="width:100%;">
          <thead>
              <tr>
                  <th class="centrar">FECHA</th>
                  <th class="centrar">DETALLE</th>
                  <th class="centrar">MONTO</th>
              </tr>
          </thead>
          <tbody>
          <?php
      		while ($row = mysqli_fetch_array($detalle)){
            
        			$tipo 	= $row[3];
              $plata 	= number_format($row[2],2,'.',',');
              $monedaX = $row[4];

	        echo "<tr>";
	          echo "<td>$row[0]</td>";
            echo "<td style='text-align: left;'>$row[1]</td>";
            if(($tipo == 2) or ($tipo == 4) or ($tipo == 5)){
              echo "<td style='text-align: right; color: blue; font-weight: bold;'>$monedaX $plata</td>";
            }elseif (($tipo == 1) or ($tipo == 3)) {
              echo "<td style='text-align: right; color: red; font-weight: bold;' >$monedaX $plata</td>";
            }            
	          
	        echo "</tr>";
	        } 
      	?>       
        <tr style="background-color: #005691;">
        	<td style="color: #fff; font-weight: bold;">FECHA: <?php echo date('d/m/Y') ?></td>
        	<td style="color: #fff; font-weight: bold;">SALDO TOTAL:</td>
        	<td style="color: #fff; font-weight: bold; text-align: right;"><?php echo $moneda.'&nbsp;&nbsp;'.$saldo; ?></td>
        </tr>  
          </tbody>
      </table>
    </div>
        </div>
    </div>      
    </form>
  

<div class="">
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
          <p>Some text in the modal.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
</div>
</div>