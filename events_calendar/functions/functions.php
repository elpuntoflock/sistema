<script type="text/javascript">

function selectItemByValue(elmnt, value){

//alert('elmnt: '+elmnt.name +' val: '+ value);

for(var i=0; i < elmnt.options.length; i++)
  {
	if(elmnt.options[i].value == value)
	  elmnt.selectedIndex = i;
  }
}

</script>
<?php
require("config.php");


/*==========================================================================================================*/
/*===================================== ANTI SQL INJECTION Function ========================================*/
/*==========================================================================================================*/

function antiSQLInjection($texto){
	// Words for search
	$check[1] = chr(34); // simbol "
	$check[2] = chr(39); // simbol '
	$check[3] = chr(92); // simbol /
	$check[4] = chr(96); // simbol `
	$check[5] = "drop table";
	$check[6] = "update";
	$check[7] = "alter table";
	$check[8] = "drop database";
	$check[9] = "drop";
	$check[10] = "select";
	$check[11] = "delete";
	$check[12] = "insert";
	$check[13] = "alter";
	$check[14] = "destroy";
	$check[15] = "table";
	$check[16] = "database";
	$check[17] = "union";
	$check[18] = "TABLE_NAME";
	$check[19] = "1=1";
	$check[20] = 'or 1';
	$check[21] = 'exec';
	$check[22] = 'INFORMATION_SCHEMA';
	$check[23] = 'like';
	$check[24] = 'COLUMNS';
	$check[25] = 'into';
	$check[26] = 'VALUES';

	// Cria se as vari�veis $y e $x para controle no WHILE que far� a busca e substitui��o
	$y = 1;
	$x = sizeof($check);
	// Faz-se o WHILE, procurando alguma das palavras especificadas acima, caso encontre alguma delas, este script substituir� por um espa�o em branco " ".
	while($y <= $x){
		   $target = strpos($texto,$check[$y]);
			if($target !== false){
				$texto = str_replace($check[$y], "", $texto);
			}
		$y++;
	}
	// Retorna a vari�vel limpa sem perigos de SQL Injection
	return $texto;
}


/*==========================================================================================================*/
/*========================================= EVENT Functions ================================================*/
/*==========================================================================================================*/

// Write javascript with events listing without the need of getting it from external file
function listEvents()
{
	global $conection;
	$sql = mysqli_query($conection, "SELECT * FROM events a, tb_caso b
										where a.id_caso = b.id_caso");
    $row = mysqli_num_rows($sql); //changed

	$image = $row['image'];
	
	if($row == ''){
		echo "
			<script>		
				$(document).ready(function() {
					$('#events').fullCalendar({
					});	
				});			
			</script>
		";
		
	}
	if($row != '') {
		echo " 
		<script>		
		$(document).ready(function() {
			$('#events').fullCalendar({
				lang: 'es',
				defaultDate: '".date("Y-m-d")."',
				editable: true,
				eventLimit: true,
				displayEventTime: true,	
				header: {
					left: 'prev,next today',
					center: 'title',
					right: 'month,agendaWeek,agendaDay,listMonth'
				},
												
				eventClick:  function(event, jsEvent, view) {				
					$('#modalTitle').html(event.title);
					var imgName=event.image;
					document.getElementById('imageDiv').innerHTML = '<img src='+imgName+' onerror=".'this.style.display="none"'." class=".'img-responsive'." alt=".''." >';
					$('#modalBody').html(event.description);
					$('#causa').html(event.causa);
					$('#detalle').html(event.descripcion);
					$('#startTime').html(moment(event.start).format('DD-MM-YYYY H:mm:ss'));
					$('#endTime').html(moment(event.end).format('DD-MM-YYYY H:mm:ss'));
					$('#eventUrl').attr('href',event.url);
					$('#fullCalModal').modal();
					 return false;
				},
				
				editable: true,
				   eventDrop: function(event, delta) {
				   var start = $.fullCalendar.moment(event.start).format();
				   var end = $.fullCalendar.moment(event.end).format();
				   $.ajax({
				   url: 'events_update.php',
				   data: 'description='+ event.description +'&title='+ event.title +'&start='+ start +'&end='+ end +'&url='+ event.url +'&color='+ event.color +'&id='+ event.id ,
				   type: 'POST',
				   success: function(json) {
					swal('Good job!', 'Event Updated!', 'success');
						 setTimeout(function () {
							location.reload()
						}, 1000);
					}
				   });
				   },
							
				events: [
					";
					while ($row = mysqli_fetch_array($sql)) {
				echo "
					{
						id: '".$row['id']."',
						title: '".utf8_encode($row['title'])."',
						image: 'assets/uploads/".$row['image']."',
						description: '<p>".utf8_encode($row['description'])."</p>',					
						start: '".$row['start']."',
						end: '".$row['end']."',
						url: '".$row['url']."',
						color: '".$row['color']."',
						descripcion: '".utf8_encode($row['DESCRIPCION'])."',
						causa: '".utf8_encode($row['CAUSA'])."',
						allDay: false
					},"; 	
			} ;
			echo "
				],	

			
			});	
		});			
	</script>
	";
	}	
}

// Display events information inside a modal box
function modalEvents()
{

	echo "	
	
	<div id='fullCalModal' class='modal fade'>
		<div class='modal-dialog'>
			<div class='modal-content'>
				<div class='modal-header'>
					<!--button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>&times;</span> <span class='sr-only'>CERRAR</span></button-->
					<h4><i class='fa fa-calendar' aria-hidden='true'></i> DETALLE GENERAL DE LA TAREA</h4>
				</div>
				
				<div class='modal-body'>
					<div class='table-responsive'>
					<div class='col-md-12'>	
					<div id='imageDiv'> </div>
					<h4 style='color: #005691;'><i class='far fa-hand-point-right'></i> DESCRIPCI&Oacute;N DE TAREA:</h4>
					 <p id='modalBody' style='margin-top: -8px;'></p>
				</div>					
						<div class='col-md-12'>	
							<!--div id='imageDiv'> </div-->
							<h4 style='color: #005691;'><i class='fas fa-user-clock'></i> FECHA INICIO: </h4>
							 <p id='startTime' style='margin-top: -10px;'></p>
						</div>			
						<div class='col-md-12'>	
							<div id='imageDiv'> </div>
							<h4 style='color: #005691;'><i class='fas fa-user-clock'></i> FECHA FINAL:</h4>
							 <p id='endTime' style='margin-top: -10px;'></p>
						</div>
						<div class='col-md-12' style='margin-top: -10px;'>	
							<h4 style='color: #005691;'><i class='fa fa-globe'></i> CAUSA:</h4>
							 <p id='causa' style='margin-top: -10px;'></p>
						</div>	
						<div class='col-md-12'>	
							<h4 style='color: #005691;'><i class='fas fa-info-circle'></i> DESCRIPCI&Oacute;N DE CAUSA:</h4>
							 <p id='detalle' style='margin-top: -10px;'></p>
						</div>																					
					</div>
				</div>
				<div class='modal-footer' style='text-align: center;'>
					<button type='button' class='boton_close' data-dismiss='modal'>CERRAR</button>		
				</div>
			</div>
		</div>
	</div> 
";
}

// Display all events
function listAllEventsDelete()
{
	
	global $conection;
	$sql = mysqli_query($conection, "select * from events ORDER BY start ASC");
    $row = mysqli_fetch_assoc($sql);
		
		echo "<table class='table table-striped table-bordered table-hover' id='dataTables-example'>";
		echo "  <thead>
                <tr>	
                  <th style='text-align: center;'>DESCRIPCI&Oacute;N</th>
				  <th style='text-align: center;'>FECHA INICIO</th>
				  <th style='text-align: center;'>FECHA FINAL</th>
				  <th style='text-align: center;'>ELIMINAR</th>
                </tr>
              </thead>";
			  echo "<tr><td>";		
			echo $row['description'];
			echo "</td><td>"; 
			echo $row['start'];
			echo "</td><td>";		
			echo $row['end'];
			echo "</td><td class='r'>
			<a href='javascript:EliminaEvento(". $row['id'] . ")'class='btn btn-danger btn-sm' role='button'><i class='fa fa-fw fa-trash'></i> DELETE</a></td>";
		while ($row = mysqli_fetch_array($sql)) {
			// Print out the contents of each row into a table
			echo "<tr><td>";		
			echo $row['title'];
			echo "</td><td>"; 
			echo $row['start'];
			echo "</td><td>";		
			echo $row['end'];
			echo "</td><td class='r'>
			<a href='javascript:EliminaEvento(". $row['id'] . ")'class='btn btn-danger btn-sm' role='button'><i class='fa fa-fw fa-trash'></i> DELETE</a></td>";
			echo "</tr>"; 	
		} 

		echo "</table>";
	
}

// Display all events
function listAllEventsEdit()
{
	global $conection;
	$sql = mysqli_query($conection, "select * from events ORDER BY start ASC");
    $row = mysqli_fetch_assoc($sql);
		
		echo "<table class='table table-striped table-bordered table-hover' id='dataTables-example'>";
		echo "  <thead>
                <tr>	
                  <th style='text-align: center;'>DESCRIPCI&Oacute;N</th>
				  <!--th>LINK</th-->
				  <th style='text-align: center;'>FECHA INICIO</th>
				  <th style='text-align: center;'>FECHA FINAL</th>
				  <th style='text-align: center;'>EDITAR</th>
                </tr>
              </thead>";
			  echo "<tr><td style='text-align: left;'>";		
			echo $row['description'];
			echo "</td>"; 
			//echo $row['url'];
			echo "<td>"; 
			echo $row['start'];
			echo "</td><td>";		
			echo $row['end'];
			echo "</td><td class='r'>
			<a href='menu.php?id=31&rc=". $row['id'] .  "' class='btn btn-primary btn-sm' role='button'><i class='fa fa-fw fa-edit'></i> EDITAR</a></td>";
		while ($row = mysqli_fetch_array($sql)) {
			// Print out the contents of each row into a table
			echo "<tr><td style='text-align: left;'>";		
			echo $row['description'];
			echo "</td><td>"; 
			echo $row['start'];
			echo "</td><td>";		
			echo $row['end'];
			echo "</td><td class='r'>
			<a href='menu.php?id=31&rc=". $row['id'] . "' class='btn btn-primary btn-sm' role='button'><i class='fa fa-fw fa-edit'></i> EDITAR</a></td>";
			echo "</tr>"; 	
		} 

		echo "</table>";
	
}

// Display all Types (sort of category for Events)
function listAllTypes()
{
	global $conection;
	$sql = mysqli_query($conection, "select * from type ORDER BY id ASC");
    $row = mysqli_fetch_assoc($sql);
		
		echo "<table class='table table-striped table-bordered table-hover' id='dataTables-example'>";
		echo "  <thead>
                <tr>
					<th>CORRELATIVO</th>				
					<th>DESCRIPC&Oacute;N</th>
					<th>ELIMINAR</th>
                </tr>
              </thead>";
			echo "<tr><td>";		
			echo $row['id'];
			echo "</td>";
			echo "<td>";		
			echo $row['title'];
			echo "</td>";						
			echo "<td class='r'>
			<a href='javascript:EliminaTipo(". $row['id'] .")'class='btn btn-danger btn-sm' role='button'><i class='fa fa-fw fa-trash'></i> DELETE</a></td>";
			echo "</tr>"; 
		while ($row = mysqli_fetch_array($sql)) {
			// Print out the contents of each row into a table
			echo "<tr><td>";		
			echo $row['id'];
			echo "</td>";
			echo "<td>";		
			echo $row['title'];
			echo "</td>"; 			
			echo "<td class='r'>			
			<a href='javascript:EliminaTipo(". $row['id'] . ")'class='btn btn-danger btn-sm' role='button'><i class='fa fa-fw fa-trash'></i> DELETE</a></td>";
			echo "</tr>"; 	
		} 

		echo "</table>";
	
}


// Edit Themes Information
function editEvent($id)
{
	global $conection;
	$sql = mysqli_query($conection, "select * from events WHERE id='".$id."'");
    $row = mysqli_fetch_assoc($sql);
	$image = $row['image'];

	
	

    echo "
				<fieldset>	
				
				<div class='form-group'>
					<label class='col-md-3 control-label' for='description'>SELECCIONAR TIPO</label>
					<div class='col-md-6'>
						<select name='title' class='form-control input-md'>
							<option>".$row['title']."</option>";
						
						$query = mysqli_query($conection, "select * from type ORDER BY id DESC");
						while ($rowX = mysqli_fetch_assoc($query)) {
											  
							echo "<option value='".$rowX['title']."'>".$rowX['title']."</option>";
												
						  }
					echo	"</select>
					</div>
				</div>				
				

					<!-- Text input-->
					<div class='form-group' style='display: none;'>
						<label class='col-md-3 control-label' for='color'>COLOR</label>
						<div class='col-md-4'>
							<div id='cp1' class='input-group colorpicker-component'>
								<input id='cp1' type='text' class='form-control' name='color' value='".$row['color']."' required/>
								<span class='input-group-addon'><i></i></span>
							</div>
						</div>
					</div>

					<div class='form-group'>
						<label class='col-md-3 control-label' for='start'>FECHA INICIAL</label>
						<div class='input-group date form_date col-md-3' data-date='' data-date-format='yyyy-mm-dd hh:ii' data-link-field='start' data-link-format='yyyy-mm-dd hh:ii'>
						<span class='input-group-addon'><span class='glyphicon glyphicon-calendar'></span></span><input class='form-control' size='16' type='text' value='".$row['start']."' readonly>
						</div>
						<input id='start' name='start' type='hidden' value='".$row['start']."' required>

					</div>

					<div class='form-group'>
						<label class='col-md-3 control-label' for='end'>FECHA FINAL</label>
						<div class='input-group date form_date col-md-3' data-date='' data-date-format='yyyy-mm-dd hh:ii' data-link-field='end' data-link-format='yyyy-mm-dd hh:ii'>
							<span class='input-group-addon'><span class='glyphicon glyphicon-calendar'></span></span><input class='form-control' size='16' type='text' value='".$row['end']."' readonly>
						</div>
						<input id='end' name='end' type='hidden' value='".$row['end']."' required>

					</div>
					
					<!-- Text input-->
					<div class='form-group' style='display: none;'>
						<label class='col-md-3 control-label' for='url'>Link</label>
						<div class='col-md-6'>
							 <textarea rows ='1' cols='10' id='url' name='url' type='text' class='form-control input-md upper'>".$row['url']."</textarea>

						</div>
					</div>

					<!-- Text input-->
					<div class='form-group'>
						<label class='col-md-3 control-label' for='description'>DESCRIPCI&Oacute;N</label>
						<div class='col-md-6'>
							<input class='form-control upper' name='description' id='description' value='".$row['description']."'>
						</div>
					</div>

					<div class='form-group'>
						<label class='col-md-3 control-label' for='observaciones'>OBSERVACIONES</label>
						<div class='col-md-6'>
							<input class='form-control' name='observaciones' id='observaciones' value='".$row['observaciones']."'>
						</div>
					</div>					
	
				";

}


// Update Themes Information
function updateEvent($id,$title,$description,$observaciones,$start,$end,$url,$color)
{
	global $conection;
	$query = mysqli_query($conection,"UPDATE events SET title = '".$title."', description = '".$description."', observaciones = '".$observaciones."',start = '".$start."', end = '".$end."', url = '".$url."', color = '".$color."' WHERE id = '".$id."'");
	if (!$query) {
		echo ("No data was inserted!: " . mysqli_error());
		return false;
	} else {
			echo "<script type='text/javascript'>swal('Datos Modificados con Exito..!!!', 'Tarea Modificada...!!!', 'success');</script>";
			echo '<meta http-equiv="refresh" content="1; menu.php?id=8">';
			die();
			}				
			return true;
}

