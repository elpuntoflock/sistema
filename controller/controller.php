<?php
$nombre = $_SESSION['usuario'];
require_once 'db/conexion.php';

/********************** DATOS PARA DASHBOARD *****************/
$contacto = mysqli_query($conn,"SELECT COUNT(*)CONTEO
                                FROM tb_contacto");

while($row = $contacto->fetch_array(MYSQLI_ASSOC)){
   $num_contacto = $row['CONTEO'];
}

$casos = mysqli_query($conn,"SELECT COUNT(*)TOTAL
                                FROM tb_caso");

while($rest = $casos->fetch_array(MYSQLI_ASSOC)){
    $num_casos = $rest['TOTAL'];
 }
  
/*************************************************************/ 
$sql = mysqli_query($conn, "SELECT ID_CONTACTO, TRIM(CONCAT_WS(' ', NOMBRES,' ',APELLIDOS,' ',NOMBRE_EMPRESA))NOMBRE, CUI, TELEFONO, DIRECCION, ZONA, EMAIL, APELLIDO_CASADA, 
								   NIT, NOMBRE_EMPRESA, CARGO_EMPRESA, NOMBRES, APELLIDOS, APELLIDO_CASADA, SEXO, NOMBRE_REFERENCIA, TELEFONO_REFERENCIA, CELULAR_REFERENCIA,
								   OBSERVACIONES, DIRECCION, EMAIL, NOMBRE_REFERENCIA, TELEFONO_REFERENCIA, CELULAR_REFERENCIA,
								   OBSERVACIONES
                              FROM tb_contacto
                              ORDER BY ID_CONTACTO DESC");
                              
/******************QUE TRAIGA CONTACTOS CON CASOS PARA FACTURACION Y CON SALDO *******************************************/ 
$sql1 = mysqli_query($conn, "SELECT distinct a.ID_CONTACTO, TRIM(CONCAT_WS(' ', NOMBRES,' ',APELLIDOS,' ',NOMBRE_EMPRESA))NOMBRE 
							FROM tb_contacto a, tb_caso b, tb_corriente c 
							WHERE (a.id_contacto = b.id_contacto) and b.estatus in ('A','I')
							AND (c.ID_CASO = b.ID_CASO)
							AND c.SALDO > 0
							ORDER BY 1 DESC");                              
                        

/*********************** TRAE LA DATA DE LOS CASOS ***************************/

$busqueda	= mysqli_query($conn, "SELECT CASE 
									WHEN A.CAUSA = '' THEN 'SIN CAUSA'
									ELSE A.CAUSA
									END AS DETALLE_CAUSA, A.ID_CONTACTO,  TRIM(CONCAT_WS(' ', B.NOMBRES,' ',B.APELLIDOS,' ',B.NOMBRE_EMPRESA))NOMBRES, A.JUZGADO, 
									A.ID_CASO, A.DESCRIPCION, DATE_FORMAT(A.FECHA_INI,'%d/%m/%Y'), DATE_FORMAT(A.FECHA_FIN,'%d/%m/%Y'), A.OBSERVACIONES, A.NUMERO_MP,
									FISCALIA, JUZGADO, JUEZ, OFICIAL, DIRECCION_JUZGADO, ZONA_JUZGADO, TELEFONO_JUZGADO, A.CASO_ORIGEN, A.ESTATUS,
                                    A.ABOGA_RESPONSABLE
									FROM	tb_caso A,
											tb_contacto B,
											tb_acceso C
									WHERE A.ID_CONTACTO = B.ID_CONTACTO
									AND A.ID_CASO 		= C.ID_CASO
									AND C.ID_USUARIO  	= '".$nombre."'");

/****************************** TRAE INFORMACIÓN DE LOS EVENTOS *******************/

$events = mysqli_query($conn, "SELECT A.ID_CASO, 
									A.DESCRIPCION, A.JUZGADO, CASE 
											WHEN A.CAUSA = '' THEN 'SIN CAUSA'
											ELSE A.CAUSA
											END AS DETALLE_CAUSA
								FROM tb_caso A,
									tb_acceso B
								WHERE A.ID_CASO = B.ID_CASO
								AND B.ID_USUARIO = '".$nombre."'");

/**************************** CAUSA PARA CARGO ABONO  **********************/
$cta = mysqli_query($conn, "SELECT A.ID_CASO, A.DESCRIPCION, CASE 
									WHEN A.CAUSA = '' THEN 'SIN CAUSA'
									ELSE A.CAUSA
									END AS DETALLE_CAUSA
                            FROM tb_caso A,
                                 tb_acceso B
							WHERE A.ID_CASO = B.ID_CASO"); 

//Solicitaron que al tener acceso a la cuenta corriente puedan tener acceso a todos los casos//

/**************************************************************************************//*,' ',NOMBRE_PROVEEDOR,' ',APELLIDO_PROVEEDOR*/

$proveedor = mysqli_query($conn, "SELECT TRIM(CONCAT(NOMBRE_EMPRESA))NOMBRE, 
										TRIM(CONCAT(NOMBRE_CONTACTO,' ',APELLIDO_CONTACTO))CONTACTO,
                                    CARGO_CONTACTO, TELEFONO_CONTACTO, CELULAR_CONTACTO, 
                                    CASE WHEN TIPO = 'P' THEN 'PROVEEDOR'
                                         WHEN TIPO = 'C' THEN 'COLABORADOR'
                                    ELSE TIPO END AS TIPOS,
                                    CASE WHEN STATUS = 'A' THEN 'ACTIVO'
                                         WHEN STATUS = 'B' THEN 'BAJA'
                                         WHEN STATUS = 'I' THEN 'INACTIVO'
                                    ELSE STATUS END AS ESTADO, ID_PROVEEDOR, NOMBRE_PROVEEDOR, APELLIDO_PROVEEDOR, DIRECCION, ZONA,
									NIT, CUI, NOMBRE_CONTACTO, APELLIDO_CONTACTO, TIPO, STATUS
							 FROM tb_proveedor");

/******************************************* FACTURAS ************************************************
$facturas = mysqli_query($conn, "SELECT CONCAT_WS(A.SERIE,'-',A.FACTURA)FAC, A.TOTAL, A.FECHA_EMISION, A.ESTATUS, A.TCHEQUE, A.TTC, A.TEFECTIVO, A.ID_FACTURA, A.FECHA_ANULA, B.ID_CASO, A.OBSERVACIONES
                                FROM tb_factura_cliente A,
                                    tb_detalle_factura B
                                WHERE A.ID_FACTURA = B.ID_FACTURA");

/******************************************* FACTURAS ************************************************/
$facturas = mysqli_query($conn, "SELECT CONCAT_WS(A.SERIE,'-',A.FACTURA)FAC, A.TOTAL, A.FECHA_EMISION, A.ESTATUS, A.TCHEQUE, A.TTC, A.TEFECTIVO, A.ID_FACTURA, A.FECHA_ANULA,  A.OBSERVACIONES
                                FROM tb_factura_cliente A");
 
/**************************** BANCOS *************************************** */
$banco = mysqli_query($conn, "SELECT ID_BANCO, BANCO, FECHA_CREA, USUARIO_GRABA
                             FROM tb_banco");

/******************************** CUENTAS BANCARIAS *******************************/
$cuentas = mysqli_query($conn, "SELECT A.IDCUENTA, B.BANCO, A.CUENTA, A.NUM_CUENTA, A.MONEDA
                                FROM tb_cuenta_banco A,
                                     tb_banco B
								WHERE A.ID_BANCO = B.ID_BANCO");
								
$sql_banco = mysqli_query($conn, "SELECT ID_BANCO, BANCO
									FROM tb_banco"); 
									
/********************************************************************/
$perfil = mysqli_query($conn, "SELECT NOMBRES, NOMBRE2, APELLIDO1, APELLIDO2, EMAIL, CELULAR
                             from tb_usuario
                             where id_usuario = '".$usuario."'");

    while($row = $perfil->fetch_array(MYSQLI_ASSOC)){

    $pnombre    = $row['NOMBRES'];
    $snombre    = $row['NOMBRE2'];
    $papellido  = $row['APELLIDO1'];
    $sapellido  = $row['APELLIDO2'];
    $email      = $row['EMAIL'];
    $celular    = $row['CELULAR'];
    
    }

/*********************************** PROVEEDOR ********************************/
$proveedores = mysqli_query($conn,"SELECT DISTINCT ID_PROVEEDOR, TRIM(CONCAT_WS(' ', NOMBRE_PROVEEDOR,APELLIDO_PROVEEDOR,'-',NOMBRE_EMPRESA)NOMBRES
                                FROM tb_proveedor
                                ORDER BY 2");

/************************************* VISUALIZA LAS FACTURAS DE LOS PROVEEDORES **************************************/                                
$rep_facturas = mysqli_query($conn, "SELECT TRIM(CONCAT_WS(' ', a.NOMBRE_PROVEEDOR, a.APELLIDO_PROVEEDOR , a.NOMBRE_EMPRESA))NOMBRE, b.SERIE, b.FACTURA,
                                     DATE_FORMAT(b.FECHA_EMISION, '%d/%m/%Y'), b.OBSERVACIONES, b.TOTAL, b.ESTATUS
                                        FROM tb_proveedor a,
                                            tb_factura_proveedor b
                                        WHERE a.ID_PROVEEDOR = b.ID_PROVEEDOR");

/******************************************************** DATOS PARA CHEQUES ********************************************/  
$cuenta = mysqli_query($conn, "SELECT IDCUENTA, CUENTA, NUM_CUENTA, MONEDA
                                FROM tb_cuenta_banco
                                WHERE ESTATUS = 'A'");

$fac_provee = mysqli_query($conn, "SELECT ID_FACTURA, OBSERVACIONES, NOMBRE, TRIM(CONCAT_WS(SERIE,' - ',FACTURA))DETALLE
                                    FROM tb_factura_proveedor");     
                                    
                                    
/************************* FECHAS YA EXISTENTES EN BASE DE EVENTOS *****************************/



?>