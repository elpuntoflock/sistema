<?php
header('Content-type: application/vnd.ms-excel');
header("Content-Disposition: attachment; filename=Reporte General.xls");
header("Pragma: cache");
header("Cache-Control: private");
header("Expires: 0");
ini_set("session.cache_limiter", "");	


require_once('db/conexion.php');

if($_REQUEST['tmp'] == 'C'){

	$vNombreEmpresa = " ";
	$vNombreSistema = "REPORTE EXCEL CONTACTOS";
	$vNombreReporte = "ABOGA";


	$sql = mysqli_query($conn, "SELECT NOMBRES, APELLIDOS, CUI, TELEFONO, DIRECCION, EMAIL
	FROM tb_contacto");

?>

<h3></h3>

<table cellspacing="0" cellpadding="0">

<?php
echo "<tr height=20>";
echo "<th colspan=7 align=center style=color:#215989;><font face='Arial, Helvetica, sans-serif';><b>$vNombreEmpresa</b></font></th>";
echo "</tr>";
echo "<tr height=20>";
echo "<th colspan=7 align=center style=color:#215989;><font face='Arial, Helvetica, sans-serif'; size='+1';><b>Reporte General de Contactos</b></font></th>"; 
echo "</tr>";
echo "<tr height=20>";
//echo "<th colspan=11 align=center style=color:#215989;><font face='Arial, Helvetica, sans-serif'; size='+1';><b>Reporte General de Contactos</b></font></th>"; 
echo "</tr>";
echo "<br>";
echo "<br>";
echo "<br>";
echo "<br>";

?>

 <tr align="center">

 <th style='border:solid; border-width:1px; font-size:15px'>NOMBRES</th>
 <th style='border:solid; border-width:1px; font-size:15px'>APELLIDOS</th>
 <th style='border:solid; border-width:1px; font-size:15px'>CUI</th>
 <th style='border:solid; border-width:1px; font-size:15px'>TELEFONO</th>
 <th style='border:solid; border-width:1px; font-size:15px'>DIRECCI&Oacute;N</th>
 <th style='border:solid; border-width:1px; font-size:15px'>EMAIL</th>
 

 </tr>

<?php
  while ($row = mysqli_fetch_array($sql)){
  	echo "<tr>";
  	echo "<td align=left style='border:dotted; border-width:1px' align=top>$row[0]</td>";
  	echo "<td align=left style='border:dotted; border-width:1px' align=top>$row[1]</td>";
	echo "<td align=left style='border:dotted; border-width:1px; mso-number-format:\"0\"' valign=top>$row[2]</td>";  
	echo "<td align=left style='border:dotted; border-width:1px' align=top>$row[3]</td>";
	echo "<td align=left style='border:dotted; border-width:1px' align=top>$row[4]</td>";
	echo "<td align=left style='border:dotted; border-width:1px' align=top>$row[5]</td>";
  	echo "</tr>";
  }


?>
</table>

<?php

}elseif($_REQUEST['tmp'] == 'A'){

	$vNombreEmpresa = " ";
	$vNombreSistema = "REPORTE EXCEL CASOS";
	$vNombreReporte = "ABOGA";


	$busqueda	= mysqli_query($conn, "SELECT CONCAT(B.NOMBRES,' ',B.APELLIDOS)NOMBRES, A.DESCRIPCION, DATE_FORMAT(A.FECHA_INI,'%d/%m/%Y'), DATE_FORMAT(A.FECHA_FIN,'%d/%m/%Y'), 
										A.ID_CONTACTO, A.ID_CASO, A.OBSERVACIONES
										FROM 	tb_caso A,
												tb_contacto B
										WHERE A.ID_CONTACTO = B.ID_CONTACTO");

?>

<h3></h3>

<table cellspacing="0" cellpadding="0">

<?php
echo "<tr height=30>";
echo "<th colspan=7 align=center style=color:#215989;><font face='Arial, Helvetica, sans-serif';><b>$vNombreEmpresa</b></font></th>";
echo "</tr>";
echo "<tr height=20>";
echo "<th colspan=7 align=center style=color:#215989;><font face='Arial, Helvetica, sans-serif'; size='+1';><b>Reporte General de Casos</b></font></th>"; 
echo "</tr>";
echo "<tr height=20>";
//echo "<th colspan=11 align=center style=color:#215989;><font face='Arial, Helvetica, sans-serif'; size='+1';><b>Reporte General de Casos</b></font></th>"; 
echo "</tr>";
echo "<br>";
echo "<br>";

?>

 <tr align="center">

 <th style='border:solid; border-width:1px; font-size:15px'>CONTACTO</th>
 <th style='border:solid; border-width:1px; font-size:15px'>DESCRIPCI&Oacute;N</th>
 <th style='border:solid; border-width:1px; font-size:15px'>FECHA INICIO</th>
 <th style='border:solid; border-width:1px; font-size:15px'>FECHA FINAL</th>
 
 </tr>

<?php
  while ($rowX = mysqli_fetch_array($busqueda)){
	  $descripcion = strtoupper($rowX[1]);
  	echo "<tr>";
  	echo "<td align=left style='border:dotted; border-width:1px' align=top>$rowX[0]</td>";
  	echo "<td align=left style='border:dotted; border-width:1px' align=top>$descripcion</td>";
	echo "<td align=left style='border:dotted; border-width:1px' align=top>$rowX[2]</td>";
	echo "<td align=left style='border:dotted; border-width:1px' align=top>$rowX[3]</td>";
  	echo "</tr>";
  }


?>
</table>
<?php
}
?>
	