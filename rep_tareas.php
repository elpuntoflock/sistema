<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="alerta/css/sweetalert.css">
<script type="text/javascript" src="alerta/js/sweetalert-dev.js"></script>

<!--LIBRERIAS-->
	<link href="events_calendar/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">	
    <script src="events_calendar/js/jquery.js"></script>	
	<link href="events_calendar/css/bootstrap-colorpicker.css" rel="stylesheet">
	<style>

/* Important part */
.modal-dialog{
    overflow-y: initial !important
}
.modal-body{
    height: 700px;
    overflow-y: auto;
}	
	</style>
<!--FINALIZAN LIBRERIAS-->

<script>
	function ErrorAcceso()
		{
		swal({title:"Su usuario no tiene privilegios para esta pantalla..!", type:"error", showConfirmButton:false, text:"COMUNIQUESE CON EL ADMINISTRADOR", timer:'900'}, 
		function () 
		{
		location.href = "menu.php?id=22"; 
		});
		}
</script>

<?php
$usuario 	= $_SESSION['usuario'];
$usuario 	= strtoupper($usuario);

require_once('db/conexion.php');

$sql = mysqli_query($conn, "SELECT A.ID_CASO, A.CAUSA, A.DESCRIPCION
							FROM tb_caso A,
								 tb_acceso B
							WHERE A.ID_CASO = B.ID_CASO
							  AND B.ID_USUARIO = '".$usuario."'");

if(isset($_POST['caso']))
    {
      $caso = $_POST['caso'];  
    }
    else{
      $caso = '';        
    }

$tareas = mysqli_query($conn, "SELECT A.ID, A.DESCRIPTION, A.START FECHA, A.END FECHA1
								FROM events A,
									tb_acceso B
								WHERE A.ID_CASO = B.ID_CASO
								AND A.ID_CASO = '".$caso."'
								AND B.ID_USUARIO = '".$usuario."'
								ORDER BY FECHA ASC");

$sql_permiso = mysqli_query($conn, "SELECT COUNT(*)CUENTA
										FROM tb_acceso_item
										WHERE id_usuario = '".$usuario."'
										  AND ITEM = ".$_REQUEST['id']."");
								
	while($valida = $sql_permiso->fetch_array(MYSQLI_ASSOC)){

		ECHO $resultado = $valida['CUENTA'];
	}

	if($resultado == 1){
		
	}else{
		echo "<script>ErrorAcceso();</script>";
	}

?>
  <script type="text/javascript">

    function selectItemByValue(elmnt, value){

    ///alert('elmnt: '+elmnt.name +' val: '+ value);

    for(var i=0; i < elmnt.options.length; i++)
      {
        if(elmnt.options[i].value == value)
          elmnt.selectedIndex = i;
      }
    }

  </script>

<div class="">
    <div class="row">
        <div class="col-md-12">
            <div class="wrapper-logo-secondary">
                <img src="img/logo/Law.png" alt="Logotipo Firma Law">
            </div>
        </div>
    </div>
</div>
<!--input type="text" value="<?php echo $resultado; ?>"-->
<div class="">
	<div class="row">
		<div class="top-line">
	        <div class="col-md-4" data-line="movil"><div class="line" style="margin-top: 25px !important;"></div></div>
	        <div class="col-md-4 titulo-seccion"><p>REPORTE TAREAS</p></div>
	        <div class="col-md-4"><div class="line" style="margin-top: 25px !important;"></div></div>
	    </div>
	</div>
</div>
<form action="menu.php?id=16" method="post">
<div class="bajar">
	<div class="row">
		<div>
            <div class="col-md-6">
            <label>Seleccionar Causa</label>
            <select name="caso" class="form-control" id="caso">
                <option>SELECCIONAR CAUSA</option>
                <?php
                while ($row = mysqli_fetch_array($sql))
                {
                  echo '<option value="' . $row['ID_CASO']. '">' . $row['CAUSA'] .' - '. $row['DESCRIPCION'].'</option>' . "\n";
                }
                ?>
            </select>
            <script language="javascript">
                var numberMI = document.getElementById("caso");
                selectItemByValue(numberMI,<?= "'".$caso."'"?>);
            </script>
            </div>
            <div class="col-md-1" style="margin-top: 36px;">
            <div class="boton-formulario">
              <button type="submit" class="boton3">BUSCAR</button>
            </div>
            </div>
        					
		</div>
	</div>
</div>	
</form>

<form>
	<div class="">
		<div class="row">

		    <div class="col-md-12 table-responsive" style="margin-top: 75px;">
		      <table id="example" class="display nowrap table table-striped table-bordered" style="width:100%;">
		          <thead>
		              <tr>
		                  <!--th class="centrar">ID TAREA</th-->
		                  <th class="centrar">DESCRIPCI&Oacute;N DEL CASO</th>
		                  <th class="centrar">FECHA / HORA INICIO</th>
						  <th class="centrar">FECHA / HORA FINAL</th>
		              </tr>
		          </thead>
		          <tbody>
		          <?php
		      		while ($row = mysqli_fetch_array($tareas)){
			          echo "<tr>";
			          //echo "<td>$row[0]</td>";
			          echo "<td style='text-align: left;'>$row[1]</td>";
					  echo "<td style='text-align: center;'>$row[2]</td>";
					  echo "<td style='text-align: center;'>$row[3]</td>";
			        echo "</tr>";
			        } 
		      	?>       
		          </tbody>
		      </table>
		    </div>
			<div>
			<button type="button" class="boton6"><a href="reporte.php?user=<?php echo $usuario; ?>&caso=<?php echo $caso; ?>" target="_blank">Reporte PDF</a></button>
			<!--button type="button" class="hover"><a href="genera_rep_excel.php?tmp=A">Reporte Excel</a></button-->
		</div>
		<div class='bajar'>
			<button type='button' class='boton3' data-toggle="modal" data-target="#ModalTarea">CREAR TAREA</button>
		</div>
		</div>
	</div>
</form>

<!--MODAL TAREA-->
<div class="modal fade" id="ModalTarea" role="dialog">
    <div class="modal-dialog">
    
      	<div class="modal-content">
			<div class="modal-header">
			
			<h4 class="modal-title" id="myModalLabel"> <i class="fa fa-calendar" aria-hidden="true"></i> INGRESO DE TAREAS</h4>
			</div>
			<div class="modal-body">
				<form action="menu.php?id=36" method="post">
					<div>
						<label>SELECCIONAR TIPO</label>
						<select name='title' class="form-control input-md">
							
							<?php 
							require_once('db/conexion.php');
							$query = mysqli_query($conn, "select * from type ORDER BY id DESC");
							
								echo "<option value='No type Selected' required>SELECCIONAR TIPO</option>";
								
							while ($row = mysqli_fetch_assoc($query)) {
									
								echo "
								
								<option value='".$row['title']."'>".$row['title']."</option>
								
								";
													
								}
						
							?>
						</select>
					</div>

					<div>
						<label>SELECCIONAR CAUSA</label>
						<select name="id_caso" class="form-control abono">
							<option value="-1">SELECCIONAR CASO</option>
								<?php

								$sql = mysqli_query($conn, "SELECT A.ID_CASO, A.CAUSA
															FROM tb_caso A,
																	tb_acceso B
														WHERE A.ID_CASO = B.ID_CASO
															AND B.ID_USUARIO = '".$usuario."'");


								while ($row = mysqli_fetch_array($sql))
								{
								echo '<option value="' . $row['ID_CASO']. '">' . $row['CAUSA'] . '</option>' . "\n";
								}
								?>                  
						</select>
					</div>

					<div>
						<label>COLOR</label>
						<div id="cp1" class="input-group colorpicker-component">
							<input id="cp1" type="text" class="form-control" name="color" value="#5367ce" required/>
							<span class="input-group-addon"><i></i></span>
						</div>
					</div>

					<div>
						<label>FECHA INICIO</label>
						<div class="input-group date form_date" data-date="" data-date-format="yyyy-mm-dd hh:ii" data-link-field="start" data-link-format="yyyy-mm-dd hh:ii">
							<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span><input class="form-control" size="16" type="text" value="" readonly>
						</div>
						<input id="start" name="start" type="hidden" value="" required>
					</div>

					<div>
						<label>FECHA FINAL</label>
						<div class="input-group date form_date" data-date="" data-date-format="yyyy-mm-dd hh:ii" data-link-field="end" data-link-format="yyyy-mm-dd hh:ii">
							<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span><input class="form-control" size="16" type="text" value="" readonly>
						</div>
						<input id="end" name="end" type="hidden" value="">

					</div>
				
					<div>
						<label>DESCRIPCI&Oacute;N</label>
						<input type="text" class="form-control upper" name="description" id="description">
					</div>

					<div>
						<label>OBSERVACIONES</label>
						<input type="text" class="form-control upper" name="observa" id="observa">
					</div>

					<!--div>
						<label for="">NOTIFICACI&Oacute;N</label>
						<input type="text" name="notificacion" class="form-control centrar upper" placeholder="NOTIFICACI&Oacute;N" require="">
					</div-->


					<div style="margin-top: 15px;">
							<label for="">NOTIFICAR EMAIL No. 1</label>
							<select name="email" id="" class="form-control centrar">
								<option value="">NOTIFICAR A EMAIL No. 1</option>
								<?php

									//require_once('db/conexion.php');

									$email1 = mysqli_query($conn, "SELECT ID_CONTACTO, EMAIL
																								FROM tb_contacto
																								WHERE EMAIL IS NOT NULL");
									while ($rowX = mysqli_fetch_array($email1))
									{
										echo '<option value="' . $rowX['EMAIL']. '">' . $rowX['EMAIL'] . '</option>' . "\n";
									}
									?> 							
							</select>

					<div style="text-align: left;" class="bajar">
						<a data-toggle='collapse' href='#demo' data-toggle="collapse"><img src="img/mas.png" style="width: 5%;"> Agregar Nuevo Correo</a>
					</div>

					</div>


					<div id="demo" class="collapse bajar">
						<label for="">NOTIFICAR EMAIL No. 2</label>
						<select name="email2" id="" class="form-control">
								<option value="">NOTIFICAR A EMAIL No. 2</option>
								<?php

								require_once('db/conexion.php');

								$email2 = mysqli_query($conn, "SELECT ID_CONTACTO, EMAIL
																							FROM tb_contacto
																							WHERE EMAIL IS NOT NULL");
								while ($rowX1 = mysqli_fetch_array($email2))
								{
									echo '<option value="' . $rowX1['EMAIL']. '">' . $rowX1['EMAIL'] . '</option>' . "\n";
								}
								?> 								
						</select>

						<div style="text-align: left;" class="bajar">
							<a data-toggle='collapse' href='#email8' data-toggle="collapse"><img src="img/mas.png" style="width: 5%;"> Agregar Nuevo Correo</a>
						</div>

					</div>

					<div id="email8" class="collapse bajar">
							<label for="">NOTIFICAR EMAIL No. 3</label>
							<select name="email3" class="form-control">
									<option value="">NOTIFICAR A EMAIL No. 3</option>
									<?php

									require_once('db/conexion.php');

									$emailX = mysqli_query($conn, "SELECT ID_CONTACTO, EMAIL
																								FROM tb_contacto
																								WHERE EMAIL IS NOT NULL");
									while ($rowX2 = mysqli_fetch_array($emailX))
									{
										echo '<option value="' . $rowX2['EMAIL']. '">' . $rowX2['EMAIL'] . '</option>' . "\n";
									}
									?> 									
							</select>
					</div>

					<!--div>
					<label for="">FECHA AVISO</label>
						<input type="text" name="fec_aviso" id="fec_notifica" class="form-control centrar upper" placeholder="Ingresar Fecha de Notificaci&oacute;n" require="">
					</div-->	

					<div>
						<label for="">DIAS PREVIOS AL AVISO:</label>
						<select name="dias" class="form-control">
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
								<option value="6">6</option>
								<option value="7">7</option>
								<option value="8">8</option>
								<option value="9">9</option>
								<option value="10">10</option>
						</select>
					</div>				
				
					<div class="modal-footer">
						<button type="submit" class="boton3">GRABAR</button>
						<button type="button" class="boton_close" data-dismiss="modal">CERRAR</button>
					</div>			
				</form>
			</div>

      	</div>
      
    </div>
  </div>
<!--FIN MODAL TAREA-->
<!-- DateTimePicker JavaScript -->
<script type="text/javascript" src="events_calendar/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<!-- Datetime picker initialization -->
<script type="text/javascript">
	$('.form_date').datetimepicker({
		language:  'en',
		weekStart: 1,
		todayBtn:  0,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		forceParse: 0
	});
</script>	
<!-- ColorPicker JavaScript -->
<script src="events_calendar/js/bootstrap-colorpicker.js"></script>
<script>
	$(function() {
		$('#cp1').colorpicker();
	});

</script>