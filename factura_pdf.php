<?php
require_once 'db/conexion.php';
require 'fpdf/fpdf.php';
require 'num.letras.php';

$globalx = 0;
$globaly = 0;
$l = 0;

$factura = $_REQUEST['num_factura'];


/********  QUERY DE ENCABEZADO  *******/
$sql = mysqli_query($conn, "SELECT FECHA_EMISION, TOTAL, NOMBRE, DIRECCION, NIT
                            FROM tb_factura_cliente
                            WHERE ID_FACTURA = '".$factura."'");

    while($row = $sql->fetch_array(MYSQLI_ASSOC)){

        //$row['FECHA_EMISION'];
        $date   = date_create($row['FECHA_EMISION']);
        $dia    = date_format($date,'d');
        $mes    = date_format($date,'m');
        $anio   = date_format($date,'Y');

        if($mes == 1){
            $mes = 'Enero';
        }elseif($mes == 2){
            $mes = 'Febrero';
        }elseif($mes == 3){
            $mes = 'Marzo';
        }elseif($mes == 4){
            $mes = 'Abril';
        }elseif($mes == 5){
            $mes = 'Mayo';
        }elseif($mes == 6){
            $mes = 'Junio';
        }elseif($mes == 7){
            $mes = 'Julio';
        }elseif($mes == 8){
            $mes = 'Agosto';
        }elseif($mes == 9){
            $mes = 'Septiembre';
        }elseif($mes == 10){
            $mes = 'Octubre';
        }elseif($mes == 11){
            $mes = 'Noviembre';
        }elseif($mes == 12){
            $mes = 'Diciembre';
        }

        //$total      = $row['TOTAL'];
        $total      = number_format($row['TOTAL'],2,'.',',');
        $nombre     = $row['NOMBRE'];
        $direccion  = $row['DIRECCION'];
        $nit        = $row['NIT'];
        $letras     = numtoletras($row['TOTAL']);

    }
    /******************************/
    $detalle = mysqli_query($conn, "SELECT DESCRIPCION, TOTAL
                                    FROM tb_detalle_factura
                                    WHERE ID_FACTURA = '".$factura."'");
    /***************************/

    $facxy = array (
        "fecha_dia" => array ($globalx + 135, $globaly + 27),
        "fecha_mes" => array ($globalx + 154, $globaly + 27),
        "fecha_anio" => array ($globalx + 180, $globaly + 27),
        "nombre" => array ($globalx + 30, $globaly + 38),
        "direccion" => array ($globalx + 30, $globaly + 45),
        "nit" => array ($globalx + 164, $globaly + 45),
        "total" => array ($globalx + 170, $globaly + 110),
        "letras" => array ($globalx + 27, $globaly + 108),
        "linea1" => array ($globalx + 13, $globaly + 61),
        "linea2" => array ($globalx + 13, $globaly + 67),
        "linea3" => array ($globalx + 13, $globaly + 73),
        "linea4" => array ($globalx + 13, $globaly + 79),
        "linea5" => array ($globalx + 13, $globaly + 85),
        "linea6" => array ($globalx + 13, $globaly + 91),
        "linea7" => array ($globalx + 13, $globaly + 97),
        "valor1" => array ($globalx + 170, $globaly + 61),
        "valor2" => array ($globalx + 170, $globaly + 67),
        "valor3" => array ($globalx + 170, $globaly + 73),
        "valor4" => array ($globalx + 170, $globaly + 79),
        "valor5" => array ($globalx + 170, $globaly + 85),
        "valor6" => array ($globalx + 170, $globaly + 91),
        "valor7" => array ($globalx + 170, $globaly + 97)
        );

$pdf = new FPDF();

$pdf->AddPage();
// DATOS FECHA
$pdf->SetFont('Arial','',11);
$pdf->setxy($facxy["fecha_dia"][0],$facxy["fecha_dia"][1]);
$pdf->Cell(17,7, $dia,$l,0,'C');
$pdf->setxy($facxy["fecha_mes"][0],$facxy["fecha_mes"][1]);
$pdf->Cell(23,7, $mes,$l,0,'C');
$pdf->setxy($facxy["fecha_anio"][0],$facxy["fecha_anio"][1]);
$pdf->Cell(17,7, $anio,$l,0,'C');

// NOMBRE DIRECCION NIT
$pdf->SetFont('Arial','',12);
$pdf->setxy($facxy["nombre"][0],$facxy["nombre"][1]);
$pdf->Cell(165,5, $nombre,$l,0);
$pdf->SetFont('Arial','',11);
$pdf->setxy($facxy["direccion"][0],$facxy["direccion"][1]);
$pdf->Cell(120,5,  $direccion,$l,1,1);
$pdf->SetFont('Arial','',12);
$pdf->setxy($facxy["nit"][0],$facxy["nit"][1]);
$pdf->Cell(32,5,  $nit,$l,1,1);

//DETALLE 7 LINEAS CON ARRAY
$pdf->SetFont('Arial','',10.5);

$var =0;
    while ($rest = mysqli_fetch_array($detalle))
    {
        $var ++;
        $descripcion = $rest[0];
        $monto       = number_format($rest[1],2,'.',',');

        $pdf->setxy($facxy["linea$var"][0],$facxy["linea$var"][1]);
        $pdf->Cell(150,5, $descripcion,$l,0);
        $pdf->setxy($facxy["valor$var"][0],$facxy["valor$var"][1]);
        $pdf->Cell(25,5, $monto ,$l,0,'R');

    }

// LETRAS TOTAL
$pdf->SetFont('Arial','',9);
$pdf->setxy($facxy["letras"][0],$facxy["letras"][1]);
$pdf->MultiCell(120,5, $letras,$l,'J',0,2);

$pdf->SetFont('Arial','',12);
$pdf->setxy($facxy["total"][0],$facxy["total"][1]);
$pdf->Cell(25,6, $total,$l,1,'R');

$pdf->Output();

?>