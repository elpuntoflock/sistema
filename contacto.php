<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="alerta/css/sweetalert.css">
<script type="text/javascript" src="alerta/js/sweetalert-dev.js"></script>

<script>
	function ErrorAcceso()
		{
		swal({title:"Su usuario no tiene privilegios para esta pantalla..!", type:"error", showConfirmButton:false, text:"COMUNIQUESE CON EL ADMINISTRADOR", timer:'900'}, 
		function () 
		{
		location.href = "menu.php?id=22"; 
		});
		}
</script>
<?php
$nombre = $_SESSION['usuario'];
require_once('db/conexion.php');

$sql = mysqli_query($conn, "SELECT NOMBRES, APELLIDOS, CUI, TELEFONO, DIRECCION, ZONA, EMAIL, ID_CONTACTO, APELLIDO_CASADA
							FROM tb_contacto");

$sql_permiso = mysqli_query($conn, "SELECT COUNT(*)CUENTA
										FROM tb_acceso_item
										WHERE id_usuario = '".$nombre."'
										  AND ITEM = '".$_REQUEST['id']."'");
								
	while($valida = $sql_permiso->fetch_array(MYSQLI_ASSOC)){

		ECHO $resultado = $valida['CUENTA'];
	}

	if($resultado == 1){
		
	}else{
		echo "<script>ErrorAcceso();</script>";
	}						

?>

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>

<style>
/* Important part */
.modal-dialog{
    overflow-y: initial !important
}
.cuerpo{
    height: 800px;
    overflow-y: auto;
}
</style>

<div class="">
    <div class="row">
        <div class="col-md-12">
            <div class="wrapper-logo-secondary">
                <img src="img/logo/Law.png" alt="Logotipo Firma Law">
            </div>
        </div>
    </div>
</div>

	<div class="wrapper-return">
		<button type="button" class="boton4"><a href="menu.php?id=22">Regresar</a></button>
	</div>

    <div class=" bajar">
        <div class="row">
        <div class="top-line" style="margin-top: 25px !important; margin-bottom: 30px;">
            <div class="col-md-4" data-line="movil"><div class="line" style="margin-top: 25px !important;"></div></div>
            <div class="col-md-4 titulo-seccion" style="margin-top: 15px !important;"><p>CONTACTOS</p></div>
            <div class="col-md-4"><div class="line" style="margin-top: 25px !important;"></div></div>
        </div>        
    <div class="col-md-12 table-responsive bajar">
	    <table id="example" class="display nowrap table table-striped table-bordered" style="width:100%;">
	        <thead>
	            <tr>
					<th class="centrar">CODIGO</th>
	                <th class="centrar">NOMBRES</th>
	                <th class="centrar">APELLIDOS</th>
	                <th class="centrar">CUI</th>
	                <th class="centrar">TELEFONO</th>
					<th class="centrar">EDITAR CONTACTO</th>
	            </tr>
	        </thead>
	        <tbody>
	       	<?php
			while ($row = mysqli_fetch_array($sql)){
				echo "<tr>";
					$id_contacto 	= $row[7];
					$nombres 		= $row[0];
					$apellidos		= $row[1]; 
					$telefono 		= $row[3];
					$direccion		= $row[4];
					$zona 			= $row[5];
					$ap_casada		= $row[8];
					$cui			= $row[2];
					$email 			= strtolower($row[6]);

					echo "<td>$id_contacto</td>";
					echo "<td style='text-align: left;'>$row[0]</td>";
					echo "<td style='text-align: left;'>$row[1]</td>";
					echo "<td>$row[2]</td>";
					echo "<td>$row[3]</td>";
					echo "<td><a href='#' data-toggle='modal' data-target='#myModal' 
					data-id_contact	= '$id_contacto'
					data-nombres 	= '$nombres'
					data-apellidos 	= '$apellidos'
					data-direccion	= '$direccion'
					data-zona		= '$zona'
					data-telefono	= '$telefono'
					data-ap_casada 	= '$ap_casada'
					data-cui	 	= '$cui'
					data-email 		= '$email'><img class='img-edit' src='img/edit.png'></a></td>";
				echo "</tr>";
				} 
			?>           
	        </tbody>
	    </table>

    </div>
		</div>
		<div>
			<button type="button" class="boton6"><a href="rep_contatos.php" target="_blank">Reporte PDF</a></button>
			<button type="button" class="boton6"><a href="genera_rep_excel.php?tmp=C" target="_blank">Reporte Excel</a></button>
		</div>
    </div>	



<script type="text/javascript">
		  /*Funcion de Validar solo numeros*/
    function valida(e){
      tecla = (document.all) ? e.keyCode : e.which;

      if (tecla==8){
          return true;
      }
      patron =/[0-9]/;
      tecla_final = String.fromCharCode(tecla);
      return patron.test(tecla_final);
  	}
</script>

	<div class=" bajar">
		<div class="row">
			<div class="top-line" style="margin-top: 25px !important; margin-bottom: 30px;">
				<div class="col-md-4" data-line="movil"><div class="line" style="margin-top: 25px !important;"></div></div>
				<div class="col-md-4 titulo-seccion" style="margin-top: 15px !important;"><p>NUEVOS INGRESOS</p></div>
				<div class="col-md-4"><div class="line" style="margin-top: 25px !important;"></div></div>
			</div> 

		<div class="col-md-12 bajar">
			<div class="boton-formulario">
			<center>
				<button type="button" class="boton3" data-toggle="modal" data-target="#ModalCrea">CREAR NUEVO CONTACTO</button>
			</center>
			</div>			
		</div> 			

		</div>
	</div>

  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <!--button type="button" class="close" data-dismiss="modal">&times;</button-->
          <h4 class="modal-title">MODIFICACI&Oacute;N CONTACTOS</h4>
        </div>
		
		<div class="modal-body">

		<form action="edit_contacto.php" method="post">
			<div class="ocultar">
				<label>ID CONTACTO</label>
				<input type="text" name="id" class="form-control centrar" readonly="">
			</div>
			<div>
	  			<label>CUI</label>
	  			<input type="text" name="dpi" class="form-control upper" placeholder="Cui">
		  </div>			
			<div>
	  			<label>NOMBRES</label>
	  			<input type="text" name="nom_contacto" class="form-control upper" placeholder="Nombres">
		  </div>
		  <div>
	  			<label>APELLIDOS</label>
				<input type="text" name="ape_contacto" class="form-control upper" placeholder="Apellidos">
		  </div>
		  <div>
				<label for="">APELLIDO DE CASADA</label>
				<input type="text" name="apellido_casada" class="form-control upper" placeholder="Apellido Casada">
				</div>		  
		  <div>
	  			<label>DIRECCI&Oacute;N</label>
				<input type="text" name="address" class="form-control upper" placeholder="Direcci&oacute;n">
		  </div>
		  <div>
	  			<label>ZONA</label>
				  <select name="zona_contacto" class="form-control upper">
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
					<option value="7">7</option>
					<option value="8">8</option>
					<option value="9">9</option>
					<option value="10">10</option>
					<option value="11">11</option>
					<option value="12">12</option>
					<option value="13">13</option>
					<option value="14">14</option>
					<option value="15">15</option>
					<option value="16">16</option>
					<option value="17">17</option>
					<option value="18">18</option>
					<option value="19">19</option>
					<option value="21">21</option>
					<option value="24">24</option>
					<option value="25">25</option>
				</select>
		  </div>
		  <div>
	  			<label>TELEFONO</label>
	  			<input type="text" name="tel_contacto" class="form-control upper" placeholder="Telefono">
		  </div>
		  <div>
	  			<label>EMAIL</label>
				<input type="text" name="email_contacto" class="form-control" placeholder="Correo Electronico">
		  </div>			
        </div>
		
        <div class="modal-footer">
			<button type="submit" class="boton3">GRABAR</button>
        	<button type="button" class="boton_close" data-dismiss="modal">CERRAR</button>
        </div>	
		</form>
		</div>	
		
   
    </div>
  </div>
  <script>
	$('#myModal').on('show.bs.modal', function(e)
	{
		var id 			= $(e.relatedTarget).data('id_contact');
		var nombres 	= $(e.relatedTarget).data('nombres');
		var apellidos 	= $(e.relatedTarget).data('apellidos');
		var direccion 	= $(e.relatedTarget).data('direccion');
		var zona	 	= $(e.relatedTarget).data('zona');
		var telefono 	= $(e.relatedTarget).data('telefono');
		var email	 	= $(e.relatedTarget).data('email');
		var casada	 	= $(e.relatedTarget).data('ap_casada');
		var cui		 	= $(e.relatedTarget).data('cui');

		$(e.currentTarget).find('input[name="id"]').val(id);
		$(e.currentTarget).find('input[name="nom_contacto"]').val(nombres);
		$(e.currentTarget).find('input[name="ape_contacto"]').val(apellidos);
		$(e.currentTarget).find('input[name="address"]').val(direccion);
		$(e.currentTarget).find('select[name="zona_contacto"]').val(zona);
		$(e.currentTarget).find('input[name="tel_contacto"]').val(telefono);
		$(e.currentTarget).find('input[name="email_contacto"]').val(email);
		$(e.currentTarget).find('input[name="apellido_casada"]').val(casada);
		$(e.currentTarget).find('input[name="dpi"]').val(cui);
	});
  </script>

  <div class="">
	<div class="modal fade" id="ModalCrea" role="dialog">
		<div class="modal-dialog">
		
		<div class="modal-content">
			<div class="modal-header">
			<h4 class="modal-title">CREACI&Oacute;N DE CONTACTOS</h4>
			</div>
			<div class="modal-body cuerpo">
			<form action="inserta_cliente.php" method="post">
				<div>
					<label for="">NOMBRES</label>
					<input type="text" name="nombres" class="form-control upper" placeholder="Ingresar Nombres" autofocus="" required="">				
				</div>
				<div>
					<label for="">APELLIDOS</label>
					<input type="text" name="apellidos" class="form-control upper" placeholder="Ingresar Apellidos" required="">
				</div>	
				<div>
					<label for="">APELLIDO DE CASADA</label>
					<input type="text" name="apellido_casada" class="form-control upper" placeholder="Apellido Casada">
				</div>	
				<div>
					<label for="">CUI</label>
					<input type="text" name="cui" class="form-control centrar upper" placeholder="Ingresar Cui" onkeypress="return valida(event)" maxlength="13" minlength="13" required="">
				</div>	
				<div>
					<label for="">PASAPORTE</label>
					<input type="text" name="pasaporte" class="form-control centrar upper" placeholder="Ingresar Pasaporte">
				</div>
				<div>
					<label for="">TELEFONO</label>
					<input type="text" name="telefono" class="form-control centrar upper" placeholder="Ingresar Telefono" onkeypress="return valida(event)" maxlength="8" minlength="8" required="">
				</div>
				<div>
					<label>DIRECCI&Oacute;N</label>
					<input type="text" name="direccion" class="form-control upper" placeholder="Direcci&oacute;n" required="">
				</div>
				<div>
					<label for="">ZONA</label>
					<select name="zona" class="form-control" required="">
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
					<option value="7">7</option>
					<option value="8">8</option>
					<option value="9">9</option>
					<option value="10">10</option>
					<option value="11">11</option>
					<option value="12">12</option>
					<option value="13">13</option>
					<option value="14">14</option>
					<option value="15">15</option>
					<option value="16">16</option>
					<option value="17">17</option>
					<option value="18">18</option>
					<option value="19">19</option>
					<option value="21">21</option>
					<option value="24">24</option>
					<option value="25">25</option>
				</select>					
				</div>
				<div>
					<label for="">SEXO</label>
					<select name="sexo" class="form-control">
					<option value="M">MASCULINO</option>
					<option value="F">FEMENINO</option>
				</select>
				</div>
				<div>
					<label for="">CORREO ELECTRONICO</label>
					<input type="text" name="email" class="form-control" placeholder="Ingresar Correo Electronico" style="text-transform: lowercase;">			
				</div>
				<div>
					<label>PAIS</label>
					<select name="pais" class="form-control" id="pais" required="">
						<option></option>
					</select>				
				</div>
				<div>
					<label>DEPARTAMENTO</label>
					<select name="departamento" class="form-control" id="departamento" required="">
						<option>SELECCIONAR DEPARTAMENTO</option>
					</select>
				</div>
				<div>
					<label>MUNICIPIO</label>
					<select name="municipio" class="form-control" id="municipio" required="">
						<option>SELECCIONAR MUNICIPIO</option>
					</select>
				</div>

				<div class="boton-formulario bajar">
					<center>
						<button type="submit" class="boton3">GRABAR</button>
						<button type="button" class="boton_close" data-dismiss="modal">CERRAR</button>
					</center>
				</div>					

			</form>
			</div>
		</div>
		
		</div>
	</div>
</div>


<script>
	function validacion(){
		var nombre = document.getElementById('nombres').value;

		if($nombre == " "){
			alert('El campo Nombres no puede estar vacio....');
			return false;
		}else{

		}

	}
</script>